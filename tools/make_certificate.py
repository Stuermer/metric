#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Create client server certificates
"""

import argparse
import socket

from metric_control import utils

DEFAULT_DOMAINS = [socket.gethostname(), "localhost", "mxred"]


def main():
    parser = argparse.ArgumentParser(
        description='Create self-signed certificates for client and server')

    parser.add_argument('-k', '--keyfile', type=str, default='metric.key',
                        help="Name of the keyfile")
    parser.add_argument('-c', '--certfile', type=str, default='metric.crt',
                        help="Name of the certfile")
    parser.add_argument('-d', '--domains', type=str, default=DEFAULT_DOMAINS, nargs='*')
    parser.add_argument('-i', '--ips', type=str, default=[], nargs='*')
    args = parser.parse_args()

    utils.generate_ssl_certs(args.keyfile, args.certfile, args.domains, args.ips,
                             validity_in_s=3600 * 24 * 365 * 25)


if __name__ == "__main__":
    main()
