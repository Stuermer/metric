import numpy as np
import pytest
from scipy.interpolate import griddata

from metric_control.clients import mccdaq_client
from metric_control.generated.common_pb2 import DeviceStatus

READY = DeviceStatus.Value("READY")


@pytest.fixture(scope='module')
def client():
    client = mccdaq_client.MCCDAQClient()
    yield client
    client.__del__()


@pytest.mark.dependency()
def test_init(client):
    client.initialize()


@pytest.mark.dependency(depends=['test_init'])
def test_get_position(client):
    ad = client.get_position()
    assert 0 <= ad.position_1 <= 360
    assert 0 <= ad.position_2 <= 360


@pytest.mark.dependency(depends=['test_init', 'test_get_position'])
def test_set_position(client):
    client.set_position(20, 10)
    ad = client.get_position()
    assert ad.position_1 == 20
    assert ad.position_2 == 10


@pytest.mark.dependency(depends=['test_init', 'test_get_position'])
def test_correct_atmosphere(client):
    zenithdist = 15.
    atemp = 0.
    rotator = 0.

    t1_data = np.genfromtxt("etc/adc_data/ADC_VALUES_ZEMAX_INTELLIDRIVES_253K.CSV")
    t2_data = np.genfromtxt("etc/adc_data/ADC_VALUES_ZEMAX_INTELLIDRIVES_263K.CSV")
    t3_data = np.genfromtxt("etc/adc_data/ADC_VALUES_ZEMAX_INTELLIDRIVES_273K.CSV")
    t4_data = np.genfromtxt("etc/adc_data/ADC_VALUES_ZEMAX_INTELLIDRIVES_283K.CSV")

    r1_angle = np.concatenate((t1_data[:, 1], t2_data[:, 1], t3_data[:, 1], t4_data[:, 1]))
    r2_angle = np.concatenate((t1_data[:, 2], t2_data[:, 2], t3_data[:, 2], t4_data[:, 2]))
    r_zd = np.concatenate((t1_data[:, 0], t2_data[:, 0], t3_data[:, 0], t4_data[:, 0]))
    r_temp = np.concatenate((np.repeat(-20., len(t1_data)), np.repeat(-10., len(t2_data)),
                             np.repeat(0., len(t3_data)), np.repeat(10., len(t4_data))))

    a1 = griddata(np.array((r_zd, r_temp)).T, r1_angle, (zenithdist, atemp), 'linear')
    a2 = griddata(np.array((r_zd, r_temp)).T, r2_angle, (zenithdist, atemp), 'linear')

    client.correct_atmosphere(zenithdist, rotator, atemp)
    ad = client.get_position()

    assert a1 - 1. <= ad.position_1 <= a1 + 1
    assert a2 - 1 <= ad.position_2 <= a2 + 1


@pytest.mark.dependency(depends=['test_init', 'test_get_position', 'test_set_position'])
def test_home(client):
    client.home()
    ad = client.get_position()
    assert ad.position_1 == 0
    assert ad.position_2 == 0
