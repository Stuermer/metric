"""
This file implements a base class for clients. It provides the
glue code between gRPC and python asyncio
"""

import asyncio
import functools
import getpass
import inspect
import logging
import re
import threading
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
import sys

import grpc
from qtpy.QtCore import QObject, Signal, QThread, Slot, QStateMachine

from metric_control.gui.state import QStateNamed
from . import server, utils
from .generated.common_pb2 import LockingRequest, Status
from .generated.common_pb2_grpc import LockingStub
from google.protobuf.empty_pb2 import Empty

SERVER_SLEEP = 60

logger = logging.getLogger("client")
logging.basicConfig(level=logging.DEBUG)


class RemoteError(Exception):
    pass


def make_interactive_client(client, clientname):
    """
    Creates an instance of a client and prints all available functions and its documentation.

    The purpose of this function is to be used when creating an interactive client

    Args:
        client (Client): gRPC client
        clientname (str): name of the client (for interactive usage)

    Returns:
        Client: gRPC client
    """
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(threadName)10s %(name)18s: %(message)s",
        stream=sys.stderr,
    )
    # this is needed to disable weired debug infomation when using tab auto-complete in ipython session
    logging.getLogger("parso.cache").disabled = True
    logging.getLogger("parso.cache.pickle").disabled = True
    logging.getLogger("parso.python.diff").setLevel("INFO")

    c = client(enforce=True)
    print(
        f"*****************************************\n"
        f"Client name is {clientname}.\n"
        f"*****************************************\n"
    )

    print(f"The available methods are: \n")
    methods = []
    for att in c.__class__.__dict__:
        if not att.startswith("_"):
            print(f"{clientname}." + att, getattr(c, att).__doc__)
            print("-----------------------------")
            methods.append(att)
    print(
        f"For help call {clientname}.{{function}}? , e.g. {clientname}.{methods[1]}?\n"
        f"*****************************************\n"
        f" Don't forget to call {clientname}.initialize(). \n"
        f"*****************************************\n"
    )
    return c


def _wrap_grpc_call_impl(f, gf, name):
    """Callback to be called by an gRPC future.

    It translates the gRPC future into an python asyncio future.

    Note: Functions returning a Status object are handled specially, if
    Status.status is not OK an exception is raised.

    Arguments:
        f: asyncio.Future
        gf: gRPC.Future
        name: Name of the remote function called, used for debug output
    """
    if f.cancelled():
        ClientBase.logger.info(
            "Future cancelled, dropping result from calling %s", name
        )
        return
    try:
        result = gf.result()
        if isinstance(result, Status) and result.status != result.OK:
            raise RemoteError("Error calling {}: {}".format(name, result))
        f.set_result(gf.result())
    except Exception as e:
        f.set_exception(e)


def wrap_grpc_call(gprc_func, future, loop, name=""):
    """
    Wraps a GRPC result in a future that can be yielded by asyncio
    Usage::
    async def my_fn(req, timeout=None):
        result = await wrap_grcp_call(stub.function_name.future(req, timeout))

    Inspired by: https://github.com/grpc/grpc/issues/6046
    """
    callback = functools.partial(_wrap_grpc_call_impl, future, gprc_func, name)

    def inner_callback(x):
        try:
            loop.call_soon_threadsafe(callback)
        except RuntimeError as exc:
            logger.error("%s", exc)
            if not loop.is_closed():
                raise

    gprc_func.add_done_callback(inner_callback)
    return future


def finish_grpc_stream(future, outer_future, rendezvous):
    """
    Finishes the call of an gRPC stream
    """
    if future.cancelled():  # Stream was aborted
        logger.info("Cancelling stream...")
        rendezvous.cancel()
        outer_future.cancel()
    if not outer_future.cancelled():
        outer_future.set_result(None)


class HeartBeatThread(threading.Thread):
    def __init__(self, heartbeat):
        super().__init__()
        logger.info("Start heartbeat thread")
        self.heartbeat = heartbeat
        self.started = threading.Event()
        self.start()

        # Wait for the heartbeat
        if self.started.wait(timeout=1.0):
            logger.info("Obtained lock")
        else:
            logger.error("Starting heartbeat failed")
            self.heartbeat.cancel()
            self.join()
            raise RuntimeError("Could not obtain lock: No heartbeat")

    def cancel(self):
        if self.is_alive():
            self.heartbeat.cancel()
        self.join()

    def run(self):
        try:
            for beat in self.heartbeat:
                # logger.trace("BEAT")
                self.started.set()
        except grpc.RpcError as exc:
            if exc.code() is not grpc.StatusCode.CANCELLED:
                logger.error("%s", exc)
                # raise
            else:
                logger.info("Heartbeat exited")


class ReceiveStreamThread(QThread):
    """ Runs a stream in a thread"""

    data_received = Signal(object)
    error = Signal(object)
    canceled = Signal()
    finished = Signal()

    def __init__(self, grpc_func, request, name, parent=None):
        super().__init__(parent)
        self.name = name
        logger.debug("Calling stream '%s' scheduled", name)
        self.stream = grpc_func(request)

    @Slot()
    def cancel(self):
        self.stream.cancel()

    def run(self):
        logger = logging.getLogger("client.{}".format(self.name))
        try:
            for item in self.stream:
                # logger.debug("Calling %s -> received %s", self.name, item)
                self.data_received.emit(item)
            else:
                self.finished.emit()
        except grpc.RpcError as exc:
            if not (isinstance(exc, grpc.Future) and exc.cancelled()):
                self.error.emit(exc)
            logger.debug("Calling %s cancelled", self.name)
            self.canceled.emit()


class ClientBase(QObject):
    """Client stores commands and executes

    We keep track of active server streaming calls so we can cancel them in
    case of an error
    """

    shutdown = Signal()
    sig_error = Signal(object)
    sig_busy = Signal()
    sig_ready = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print("ClientBase")
        self.queue = None
        self.loop = None
        self.heartbeats = set()

        self.ready_state = QStateNamed(name="READY")
        self.busy_state = QStateNamed(name="BUSY")
        self.error_state = QStateNamed(name="ERROR")
        self.unknown_state = QStateNamed(name="UNKNOWN")

        self.unknown_state.addTransition(self.sig_ready, self.ready_state)
        self.ready_state.addTransition(self.sig_busy, self.busy_state)
        self.busy_state.addTransition(self.sig_ready, self.ready_state)

        self.unknown_state.entered.connect(lambda: print("INUNKNOWNSTATE"))
        self.ready_state.entered.connect(lambda: print("PRINT READY READY"))

        self.ready_state.addTransition(self.sig_error, self.unknown_state)
        self.busy_state.addTransition(self.sig_error, self.unknown_state)

        self.machine = QStateMachine()
        self.machine.addState(self.ready_state)
        self.machine.addState(self.busy_state)
        self.machine.addState(self.error_state)
        self.machine.addState(self.unknown_state)
        self.machine.setInitialState(self.unknown_state)

        self.machine.start()

    def __del__(self):
        self.shutdown.emit()
        for heartbeat in self.heartbeats:
            heartbeat.cancel()

    def create_stub(self, Stub, config=None, enforce=False, section=None):
        """Opens a secure channel and request a lock on the Service

        Implements the locking protocol for servers:
        1. Call the LockingServicer of the Service to obtain a lock
        2. Return a channel with the locking token
        """

        if config is None:
            config = server.load_config()
        if section is None:
            section, _ = re.subn(r"Stub$", "", Stub.__name__)

        host = config.get_string(section, "host")
        port = config.get_string(section, "port")
        address = "{}:{}".format(host, port)
        cert = config.get_file_content(section, "cert_file", mode="rb")

        logger.info("Connecting to %s", address)
        # TODO verify cert file using openssl?
        credentials = grpc.ssl_channel_credentials(root_certificates=cert)
        channel = grpc.secure_channel(address, credentials)

        stub = LockingStub(channel)

        request = LockingRequest(
            software_version=utils.get_software_version(),
            client_user=getpass.getuser(),
            client_agent=type(self).__name__,
            force_unlock=enforce,
        )

        # Check on first remote call for connection errors:
        # See Issue #3
        try:
            response = stub.request_locking_token(request)
        except grpc.RpcError as exc:
            if exc.code() == grpc.StatusCode.UNAVAILABLE:
                logger.fatal("Error connecting to server: %s", exc.details())
                err = """{}
Please check IP, port and previous log messages for SSL related errors. You can
increase the grpc log output by setting:
export GRPC_TRACE=transport_security
export GRPC_VERBOSITY=debug""".format(
                    exc.details()
                )
                raise RuntimeError(err)
            else:
                raise

        if not response.token:
            logger.error(response.error)
            raise RuntimeError(
                "Could not obtain locking token: {}".format(response.error)
            )
        elif response.error:
            logger.warning("Obtained unclean locking token: %s", response.error)
        else:
            logger.info("Obtained locking token")

        # Start heartbeat RPC
        logger.info("Starting heartbeat")
        rpc = stub.heartbeat(response)
        logger.info("Started heartbeat RPC")
        self.heartbeats.add(HeartBeatThread(rpc))

        call_credentials = grpc.metadata_call_credentials(
            server.LockingTokenCallCredentials(response.token)
        )
        credentials = grpc.composite_channel_credentials(credentials, call_credentials)

        # Allow large message, e.g. for FITS images
        options = (("grpc.max_receive_message_length", -1),)
        return Stub(grpc.secure_channel(address, credentials, options))

    def release_lock(self):
        "Release a locked Server, fails are only logged"
        for heartbeat in self.heartbeats:
            heartbeat.cancel()
        self.heartbeats.clear()

    def queue_call(self, f, request, callback=None):
        """
        Execute function directly or queue gRPC method call, when used within
        a ParallelExecution context manager.

        Arguments:
            f: fRPC method
            request: request to use
            callback: a asyncio coroutine, that is called each time a
                      stream (grpc.UnaryStreamMultiCallable) returns a value,
                      works only with the ParallelExecution context manager.
        """
        # Hacky: Extract function name for better error messages
        try:
            frame = inspect.currentframe().f_back
            info = inspect.getframeinfo(frame)
            m = re.search(r"\(self\.([\w.]+)\s*(,|\))", info.code_context[0])
            if m is None:
                m = re.search(r"\s*self\.([\w.]+)\s*(,|\))", info.code_context[0])
            name = m.group(1)
        except Exception as exc:
            logger.warning(
                "Couldn't resolve method name: %s\n\t(code line: %s)",
                exc,
                info.code_context[0],
            )
            name = "unknown"

        if self.loop is None:
            logger.debug("Call %s synchronously", name)
            if isinstance(f, grpc.UnaryUnaryMultiCallable):
                return f(request)
            elif isinstance(f, grpc.UnaryStreamMultiCallable):
                stream = ReceiveStreamThread(f, request, name, parent=self)
                stream.error.connect(self.sig_error.emit)
                self.shutdown.connect(stream.cancel)
                stream.start()
                return stream
            else:
                raise RuntimeError()
        else:
            # We need to create a future ahead and pass it to the coroutine,
            # because we don't schedule the call right away, but only after
            # execute_commands is called
            future = self.loop.create_future()
            if isinstance(f, grpc.UnaryUnaryMultiCallable):
                if callback is not None:
                    raise ValueError(
                        "A callback is not supported for unary calls:"
                        " use add_done_callback on the future"
                    )

                cr = self._make_unary_unary_call(f, request, future, self.loop, name)
            elif isinstance(f, grpc.UnaryStreamMultiCallable):
                cr = self._make_unary_stream_call(
                    f, request, future, callback, self.loop, name
                )
            else:
                raise RuntimeError()

            self.queue[f._channel].append((cr, future, name, []))
            return future

    def set_loop(self, loop, queue):
        """Setting the loop causes calls be queue in the loop. This allows
        for the parallel execution of commands of various clients:

        with ParallelExecution(client1, client2):
             client1.do_stuff()
             client2.do_stuff()

        Note: This functions is intended to be called only by ParallelExecution
        """
        self.loop = loop
        self.queue = queue

    def close_loop(self):
        """
        Note: This functions is intended to be called only by ParallelExecution
        """
        self.loop = None
        self.queue = None

    @staticmethod
    async def _make_unary_unary_call(grpc_func, request, future, loop, name):
        if future.cancelled():
            logger.debug("Calling '%s' cancelled", name)
            return

        logger.debug("Calling '%s' scheduled", name)
        return await wrap_grpc_call(
            grpc_func.future(request), future, loop=loop, name=name
        )

    @classmethod
    async def _make_unary_stream_call(
        cls, grpc_func, request, future, callback, loop, name
    ):
        if future.cancelled():
            logger.debug("Calling '%s' cancelled", name)
            return

        # Start the RPC
        rendezvous = grpc_func(request)

        # Now launch a thread to iterate over the RPC stream
        f = functools.partial(
            cls._receive_unary_stream, rendezvous, name, callback, [], loop
        )
        executor = ThreadPoolExecutor(1, thread_name_prefix=name)  # Might get blocked?
        stream_task = loop.run_in_executor(executor, f)

        # Propagate that the task is finished to the given future
        stream_task.add_done_callback(
            functools.partial(
                finish_grpc_stream, outer_future=future, rendezvous=rendezvous
            )
        )

        logger.debug("Calling stream '%s' scheduled", name)
        return stream_task

    @classmethod
    def _receive_unary_stream(cls, grpc_rendezvous, name, callback, subtasks, loop):
        """
        This method receives a stream from an gRPC method call and passes each
        streamed value into a callback. The callback is handed to the the given
        asyncio event loop.

        Returns the scheduled callbacks, so that the main function can wait on
        them.

        Note: This method is intended to be called by loop.run_in_executor
        callback: an coroutine to be called
        """
        logger = logging.getLogger("client.{}".format(name))
        logger.debug("Receiving stream %s -> %s", name, callback)
        try:
            for item in grpc_rendezvous:
                subtasks.append(asyncio.run_coroutine_threadsafe(callback(item), loop))
                logger.debug(
                    "Calling %s -> %s called callback(%s)", name, callback, item
                )
        except grpc.RpcError as exc:
            if not (isinstance(exc, grpc.Future) and exc.cancelled()):
                raise
            logger.debug("Calling %s -> %s cancelled", name, callback)


class ParallelExecution(object):
    """

    Note: timeout is currently ignored
    """

    def __init__(self, *clients, timeout=None, debug=False):
        self.queue = defaultdict(list)
        self.loop = asyncio.new_event_loop()
        self.loop.set_debug(debug)
        self.timeout = timeout
        self.clients = clients

    def __enter__(self):
        for client in self.clients:
            client.set_loop(self.loop, self.queue)

    def __exit__(self, exc_type, exc_value, traceback):
        # When exception occurred in the with statement, we don't
        # attempt to execute the queue
        if exc_type:
            logger.error("Dropping command queue due to %s(%s)", exc_type, exc_value)
            return None
        try:
            self.execute_commands()
        finally:
            for client in self.clients:
                client.close_loop()

            for channel, calls in self.queue.items():
                for cr, future, name, msgs in calls:
                    if future.cancelled() or future.exception():
                        logger.warning(
                            "Couldn't finish calling %s: %s", name, ", ".join(msgs)
                        )

    def execute_commands(self):
        """Execute queued commands
        In case of an error active unary calls are finished, active stream
        cancelled and pending calls skipped.
        """

        task = asyncio.ensure_future(
            self._run_tasks(self.queue, self.loop), loop=self.loop
        )
        try:
            logger.info("Starting remote commands")
            self.loop.run_until_complete(task)
            logger.info("Finished remote commands without error")
        except Exception as exc:
            logger.error("Error executing commands, see backtrace for details")
            raise
        finally:
            self.loop.close()  # Detect unprocessed tasks

    @classmethod
    async def _run_tasks(cls, queue, loop):
        """
        """
        # Launch a task for each grpc channel
        jobs = []
        for channel, calls in queue.items():
            job = cls._process_calls(calls, loop=loop)
            jobs.append(asyncio.ensure_future(job, loop=loop))

        done, pending = await asyncio.wait(
            jobs, loop=loop, return_when=asyncio.FIRST_EXCEPTION
        )

        logger.info("%i/%i jobs executed", len(done), len(jobs))

        # Check if any task has thrown and cancel any outstanding futures
        if any(f.exception() is not None for f in done):
            logger.warning("Exceptions detected, abort and cleaning up...")

            # Cancel futures and close uncalled coroutines
            for channel, calls in queue.items():
                for cr, future, name, msgs in calls:
                    if not future.done():
                        future.cancel()
                        logger.info("Cancelling future %s %s", name, future)
                        msgs.append("Future cancelled due to exception.")

            if pending:
                await asyncio.wait(pending, loop=loop)

        # This raises the first exception in case there was an error
        for job in jobs:
            if job.exception():
                raise job.exception()

    @staticmethod
    async def _process_calls(calls, loop):
        """Processes the given calls sequentially

        The coroutine processes the calls in the given order and awaits their
        completion. When a remote call returns a stream, it is executed in a
        separated thread and won't block the execution of the next thread.
        """

        streams = []
        for cr, future, name, msgs in calls:
            if future.cancelled():
                logger.debug("Calling '%s' cancelled", name)
                msgs.append("Was cancelled.")
                continue
            try:
                result = await cr
                if asyncio.isfuture(result):
                    streams.append((result, msgs))
            except Exception as exc:
                logger.error("Abort execution of '%s', caught exception %s", name, exc)
                msgs.append(str(exc))
                logger.info("Aborting %i active streams", len(streams))
                for stream, stream_msgs in streams:
                    stream.cancel()
                    stream_msgs.append("Abort due to exception")
                raise

        if streams:
            streams = [s for s, _ in streams]
            logger.debug("Waiting for %i streams", len(streams))
            for stream in streams:
                logger.debug("  -> %s", streams)
            await asyncio.wait(streams, loop=loop)  # Propagates cancellation
