"""
This file implements a base classes and utility code for servers.
"""
import argparse
import datetime
import functools
import importlib
import json
import logging
import os
import re
import secrets
import sys
import threading
import time
from collections import namedtuple
from concurrent.futures import ThreadPoolExecutor
from logging.config import fileConfig
from pathlib import Path

import grpc
import psutil
import redis
from autologging import logged

from metric_control.config import ConfigType
from . import utils
from .generated import common_pb2_grpc
from .generated.common_pb2 import LockingToken, HeartBeat, Status

SERVER_SLEEP = 60

auth_logger = logging.getLogger("root")

log_file_path = str(Path(__file__).resolve().parents[1]) + "/etc/loggers.ini"

logger = logging.getLogger("server")

# Represents an active connection
ActiveClient = namedtuple("ActiveClient", ["token", "ip", "user", "agent"])


class LockingTokenCallCredentials(grpc.AuthMetadataPlugin):
    """Metadata wrapper for raw access token credentials.

    See: grpc._auth
    """

    METADATA_LOCK_FIELD = "x-locking-token"

    def __init__(self, access_token):
        self._access_token = access_token

    def __call__(self, context, callback):
        metadata = ((self.METADATA_LOCK_FIELD, self._access_token),)
        callback(metadata, None)

    @classmethod
    def verify_token(cls, context, token):
        for key, value in context.invocation_metadata():
            if key == cls.METADATA_LOCK_FIELD:
                if secrets.compare_digest(value, token):
                    auth_logger.debug("Valid token from: %s", context.peer())
                    return True
                else:
                    auth_logger.debug("Invalid token from: %s", context.peer())
                    return False

        auth_logger.debug("No token in request from: %s", context.peer())
        return False

    @staticmethod
    def create_token():
        return secrets.token_hex()


def make_server(serviceinstance, servername):
    """
    Generalized function to create server instance

    Args:
        serviceinstance (service): service instance (e.g. ADC)
        servername (str): name used in description

    Returns:

    """
    # create logs folder if not existing
    Path(__file__).resolve().parents[1].joinpath("logs").mkdir(parents=True, exist_ok=True)

    parser = argparse.ArgumentParser(description=f"{servername} control.")
    parser.add_argument(
        "-c",
        "--config",
        type=ConfigType,
        required=False,
        default="etc/metric_config.ini",
        help="location of the Metric config file",
    )
    parser.add_argument(
        "-s",
        "--simulation",
        default=False,
        action="store_true",
        help="start server as pure simulator (no hardware access)",
    )
    parser.add_argument(
        "--section", type=str, default=None, help="Section name in config file"
    )

    args = parser.parse_args()

    if args.config == "etc/metric_config.ini" and args.simulation:
        args.config = "etc/metric_config_simulation.ini"

    service = serviceinstance(
        args.config, simulation=args.simulation, section=args.section
    )
    serve(service, config=args.config)


class LockingServicer(common_pb2_grpc.LockingServicer):
    """
    Locking service, that grant unique access for a single client

    This class provides the check_token decorator, which allows access only with
    an valid locking token. It further tracks all active connections and cancels
    them, when the token is invalidated.

    Note: This is not intended to provide secure authentication! Just to avoid
    accidental concurrent access.

    Note: Private methods (starting with _) do not lock data structures against
          concurrent access, the caller has this responsibility
    """

    HEARTBEAT_SLEEP = 0.2

    def __init__(self):
        # token_request holds an requested token, this becomes active
        # as soon as the heartbeat RPC is running
        self.active_token_requests = {}
        self.request_token_lock = threading.RLock()
        self.heartbeat_lock = threading.Lock()

        # active holds an established token, concurrent access
        # is guarded by active_lock
        self.active = None
        self.active_rpcs = set()
        self.active_lock = threading.RLock()
        self.software_version = bytes(utils.get_software_version())

    @staticmethod
    def check_token(f):
        """Decorator, that wraps an RPC call to verify its token

        This raises an exception, that can progagate to the gRPC layer to signal
        invalid locking. We register an callback to automatically clean the
        registry.
        """

        @functools.wraps(f)
        def wrapped(self, request, context):
            with self.locking.active_lock:
                if not self.locking._has_valid_token(context):
                    msg = "Invalid locking token"
                    auth_logger.debug(msg)
                    context.set_code(grpc.StatusCode.PERMISSION_DENIED)
                    context.set_details(msg)
                    raise Exception(msg)
                self.locking._register_rpc(context)  # Checks token and raises
            return f(self, request, context)

        return wrapped

    def request_locking_token(self, request, context):
        """
        Request unique access to the server. The loc

        Note: This allows for a race condition if a second client calls
        lock_service, before the heartbeat is active
        """

        with self.request_token_lock:
            client = dict(
                agent=request.client_agent, ip=context.peer(), user=request.client_user
            )
            agent = "'{agent}' from user: {user}@{ip}".format(**client)

            # Check software versions
            ok, msg = self._check_software_version(request.software_version)
            if not ok:
                auth_logger.info("%s from %s", msg, agent)
                return LockingToken(error=msg)

            if request.force_unlock:
                self.stop_heartbeat()

            with self.active_lock:
                if self.active is not None:
                    msg = (
                        "Couldn't lock service, because it is already locked "
                        " by {}".format(agent)
                    )
                    auth_logger.info(msg)
                    return LockingToken(error=msg)

                # TODO Somebody could spam us with token requests -> DOS
                # but this is unlikely
                token = LockingTokenCallCredentials.create_token()
                response = ActiveClient(token=token, **client)
                self.active_token_requests[token] = response
                auth_logger.info("Granted locking token to %s", agent)
                return LockingToken(token=token, error=msg)

    def heartbeat(self, request, context):
        with self.active_lock, self.request_token_lock:
            auth_logger.info("Requested heartbeat")
            active = self.active_token_requests.get(request.token, None)
            if active is None:
                if self.active is not None:
                    msg = "Already locked by '{agent}' from user: {user}@{ip}".format(
                        **self.active._asdict()
                    )
                else:
                    msg = "Invalid token from {}".format(context.peer())
                context.set_code(grpc.StatusCode.PERMISSION_DENIED)
                context.set_details(msg)
                raise Exception(msg)

            if not self.heartbeat_lock.acquire(blocking=False):
                msg = "Already locked by '{agent}' from user: {user}@{ip}".format(
                    **self.active._asdict()
                )
                context.set_code(grpc.StatusCode.PERMISSION_DENIED)
                context.set_details(msg)
                raise Exception(msg)

            # Now the caller has the complete lock and we invalidate all
            # other requests
            self.active = active
            self.active_token_requests.clear()

        try:
            agent = "'{agent}' from user: {user}@{ip}".format(**active._asdict())
            auth_logger.info("Starting heartbeat for %s %s", agent, self.heartbeat_lock)

            while True:
                # auth_logger.trace("BEAT for %s %s", agent, self.heartbeat_lock)
                yield HeartBeat()
                time.sleep(self.HEARTBEAT_SLEEP)
                with self.active_lock:
                    if self.active is None or not context.is_active():
                        break
        finally:
            auth_logger.info("Closing heartbeat for %s", agent)
            with self.active_lock:
                self._cancel_active_rpcs()
                self.active = None
                self.heartbeat_lock.release()
            auth_logger.info("Closed heartbeat for %s", agent)

    def release_service(self, request, context):
        with self.request_token_lock:
            with self.active_lock:
                if self.active is None:
                    auth_logger.info("Ignoring release, service not locked.")
                    return Status(
                        status=Status.NOT_LOCKED, error_message="Service not locked"
                    )
                else:
                    agent = "'{agent}' from user: {user}@{ip}".format(
                        **self.active._asdict()
                    )

                valid = self._has_valid_token(context)
                if not valid:
                    auth_logger.info("Ignoring release, locked by %s", agent)
                    return Status(
                        status=Status.NOT_LOCKED,
                        error_message="Service locked by {}".format(agent),
                    )
                else:
                    auth_logger.info("Released locking from %s", agent)
                    self.active = None

            # Wait for the heartbeat RPC to finish and cancel active RPCs
            with self.heartbeat_lock:
                with self.active_lock:
                    self._cancel_active_rpcs()
            return ServerBase.OK

    def stop_heartbeat(self):
        "Stop the heartbeat RPC"
        # This stops the heartbeat RPC
        with self.request_token_lock:
            with self.active_lock:
                agent = ""
                if self.active is not None:
                    agent = "from '{agent}' from user: {user}@{ip}".format(
                        **self.active._asdict()
                    )
                auth_logger.info("Released locking from %s", agent)
                self.active = None

            # Wait for the heartbeat RPC to finish and cancel active RPCs
            with self.heartbeat_lock:
                with self.active_lock:
                    self._cancel_active_rpcs()

    def test_unary_unary(self, request, context):
        return request

    def test_unary_stream(self, request, context):
        for ii in range(request.number_of_replies):
            yield request

    def _has_valid_token(self, context):
        return self.active is not None and LockingTokenCallCredentials.verify_token(
            context, self.active.token
        )

    def _register_rpc(self, context):
        """register an RPC call"""
        self.active_rpcs.add(context)

        def remove_context():
            with self.active_lock:
                try:
                    self.active_rpcs.remove(context)
                except KeyError:
                    pass

        context.add_callback(remove_context)
        return True

    def _cancel_active_rpcs(self):
        """Cancel all currently active request

        The caller ensures, that self.lock is locked

        Note: I am not sure what gRPC really does here...
        """
        for context in self.active_rpcs:
            context.cancel()
        # Replace the set, because the done_callbacks will still reference
        # the old one
        self.active_rpcs = set()

    def _check_software_version(self, version):
        software_version = self.software_version
        dirty = False
        if version.endswith(b"-dirty"):
            version = version[:-6]
            dirty = True
        if software_version.endswith(b"-dirty"):
            software_version = software_version[:-6]
            dirty = True

        if version != software_version:
            msg = "Client version '{}', doesn't matches server version '{}'"
            return True, msg.format(version, software_version)
        elif dirty:  # Good, but dirty
            msg = "Version check is not safe: Uncommited code in repo"
            return True, msg
        else:  # Good
            return True, ""


class FakeLockingServicer(object):
    def __init__(self):
        self.active_lock = threading.Lock()

    def _has_valid_token(self, context):
        return True

    def _register_rpc(self, context):
        pass


# @traced(logger)
@logged(logger)
class ServerBase(object):
    """Base class for RPC servers"""

    def __init_subclass__(cls):
        base = [c for c in cls.__bases__ if c is not ServerBase][0]
        for name, obj in cls.__dict__.items():
            if name.startswith("__") or name not in base.__dict__:
                continue
            setattr(cls, name, LockingServicer.check_token(obj))

    def __init__(self, simulation=False, section=None, redis_server="localhost"):
        if section is None:
            self.section = type(self).__name__
        else:
            self.section = section

        fileConfig(
            log_file_path,
            disable_existing_loggers=False,
            defaults={"logfilename": "./logs/" + self.section + ".log"},
        )

        self.redis = redis.StrictRedis(host=redis_server, port=6379, db=0)
        self.old_values = (
            {}
        )  # cached parameter (state) values to write only changes to databas
        self.simulation = simulation

    OK = Status(status=Status.OK)

    def error(self, msg):
        logger.exception("ERROR: " + str(msg))
        return Status(status=Status.ERROR, error_message=str(msg))

    def add_locking_servicer(self, servicer):
        assert isinstance(servicer, LockingServicer)
        self.locking = servicer

    def add_fake_locking_servicer(self):
        """Add a fake locking servicer to be used in scripts"""
        self.locking = FakeLockingServicer()

    def make_log_dict(self):
        res = {}
        timestamp = datetime.datetime.utcnow().isoformat()
        res.update({key: json.dumps(getattr(self, key)) for key in self.logged_values})
        res.update({"time": json.dumps(timestamp)})
        return res

    def make_log_dict_local(self):
        dic = {key: getattr(self, key) for key in self.logged_values}
        print(dic)
        return dic

    def read_database(self, name):
        try:
            result = self.redis.hgetall(name)
            result = {
                k.decode("ascii"): json.loads(v.decode("ascii"))
                for k, v in result.items()
            }
            return result
        except Exception as e:
            logging.error(e)
            return None

    def write_to_databases(self):
        try:
            self.redis.hmset(self.redis_base_string, self.make_log_dict())
        except:
            logger.error("Could not write to Redis DB")
            # raise RuntimeError("Could not write to Redis DB")

    def cleanup(self):
        """
        Cleanup function in case server crashes or gets interrupted by Keyboard command.
        Overwrite this function for subclasses in case cleanup (e.g. telnet connection) is neccessary.
        """
        pass

    def restart_server(self):
        """Restarts the server program, with file objects and descriptors
           cleanup
        """

        try:
            p = psutil.Process(os.getpid())
            for handler in p.open_files() + p.connections():
                os.close(handler.fd)

        except Exception as e:
            logging.error(e)

        try:
            self.cleanup()
        except Exception as e:
            logging.error(e)

        python = sys.executable
        os.execl(python, python, *sys.argv)


logger = logging.getLogger("servicer")


def add_service_to_executor(service, port, credentials, executor):
    """Add the given service to the executor"""

    # Guess the name of our class: Check all base classes except this one
    # and use the first
    class_ = type(service)
    base = [t for t in class_.__bases__ if t is not ServerBase][0]
    name = base.__name__
    module = importlib.import_module(base.__module__)
    add_to_server = getattr(module, "add_{}_to_server".format(name))

    # Check that all methods are implemented
    methods = [getattr(service, f).__name__ for f in dir(base) if f[0] != "_"]
    not_implemented = [f for f in methods if f not in class_.__dict__]
    if not_implemented:
        raise NotImplementedError(
            "Not all RPC methods are implemented, missing: {}".format(
                ", ".join(not_implemented)
            )
        )

    # Create and add locking support
    locking_servicer = LockingServicer()
    service.add_locking_servicer(locking_servicer)

    # Create and start server
    server = grpc.server(executor)
    add_to_server(service, server)
    common_pb2_grpc.add_LockingServicer_to_server(locking_servicer, server)
    ip = "[::]:{}".format(int(port))
    if credentials:
        server.add_secure_port(ip, credentials)
    else:
        server.add_insecure_port(ip)
    server.start()
    return server


def load_config(args=None):
    from .config import ConfigType

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        type=ConfigType,
        default="etc/metric_config_simulation.ini",
        help="location of the Metric config file",
    )
    # if args is None:
    #     args = sys.argv
    parsed_args, unknown = parser.parse_known_args(args=args)
    return parsed_args.config


def serve_many(services, config):
    """Startup server.

    TODO
    Note: If we use max_workers=1 to avoid parallel execution of task, this
          could lead to deadlocks, see:
          https://docs.python.org/3/library/concurrent.futures.html#threadpoolexecutor
    """

    if config is None:
        config = load_config()

    section = "Server"

    cert_chain = config.get_file_content(section, "cert_file", mode="rb")
    private_key = config.get_file_content(section, "key_file", mode="rb")
    max_workers = config.get_integer(section, "max_workers")

    credentials = grpc.ssl_server_credentials([(private_key, cert_chain),])

    if max_workers <= 0:
        max_workers = None

    with ThreadPoolExecutor(max_workers=max_workers) as executor:

        servers = []
        for service in services:
            if service.section is not None:
                section = service.section
            else:
                section, _ = re.subn(r"Server$", "", type(service).__name__)
            port = config.get_integer(section, "port")
            logger.info("Starting %s at port %i", section, port)
            if service.simulation:
                logger.info("************* SIMULATION MODE ********************")
            servers.append(
                add_service_to_executor(service, port, credentials, executor)
            )

        try:
            while True:
                time.sleep(SERVER_SLEEP)
        except KeyboardInterrupt:
            print("exiting...")
            for server in servers:
                server.stop(1.0)
            for service in services:
                service.cleanup()


def serve(service, config=None):
    """Startup and run server"""
    serve_many([service], config=config)
