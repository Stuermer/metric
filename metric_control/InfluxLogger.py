import json
import threading

import redis
from influxdb import InfluxDBClient

# InfluxDB connections settings
host = 'localhost'
port = 8086
user = 'root'
password = 'root'
dbname = 'metric'

myclient = InfluxDBClient(host, port, user, password, dbname)


#     try:
#         myclient.drop_database(dbname)
#     except:
#         pass
#     myclient.create_database(dbname)
# except:
#     print("Check database setup ! ")


class Listener(threading.Thread):
    def __init__(self, r, channels, influxclient=None):
        threading.Thread.__init__(self)
        self.redis = r
        self.pubsub = self.redis.pubsub()
        self.pubsub.psubscribe(channels)
        self.client = influxclient
        print('Now listening to ', channels)

    def work(self, item):
        if item['channel'].decode("ascii").endswith(":hset"):
            hset = item['data'].decode("ascii")
            if hset.startswith("metric."):
                result = self.redis.hgetall(hset)
                result = {k.decode("ascii"): json.loads(v.decode("ascii")) for k, v in result.items()}
                # print(result)
                t = result.pop("time")
                json_body = [
                    {
                        "measurement": hset,
                        "time": t,
                        "fields": {k: v for k, v in result.items()}
                    }
                ]
                print(json_body)
                self.client.write_points(json_body)

    def run(self):
        while True:
            item = self.pubsub.get_message()
            if item:
                self.work(item)
                # print(item)
            else:
                time.sleep(0.005)


if __name__ == "__main__":
    import time

    r = redis.StrictRedis()
    l = Listener(r, '__keyevent@0__:*', myclient)
    l.start()
