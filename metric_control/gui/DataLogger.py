import copy
import json
import threading
from datetime import datetime

import PyQt5.QtCore
import PyQt5.QtGui
import numpy as np
import pandas as pd
import redis
import tables
from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal, QTimer
from influxdb import DataFrameClient
from influxdb import InfluxDBClient


class Listener(threading.Thread):
    def __init__(self, r, channels, influxclient=None):
        threading.Thread.__init__(self)
        self.redis = r
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)
        self.client = influxclient
        print('Now listening to ', channels)

        # def _write(self, time, data, seriesname, tags):
    def work(self, item):
        # d = json.loads(item['data'])
        if isinstance(item['data'], bytes):
            d = json.loads(item['data'].decode("ascii"))
            # print d
            if 'influx_seriesname' in d:
                self.client._write(d['timestamp'], d['data'], d['influx_seriesname'], d['influx_tags'])
            else:
                # _write_annotation(self, time, title, text, tags):
                self.client._write_annotation(d['timestamp'], d['title'], d['text'], d['influx_tags'])

    def run(self):
        for item in self.pubsub.listen():
            if item['data'] == "KILL":
                self.pubsub.unsubscribe()
                print(self, "unsubscribed and finished")
                break
            else:
                self.work(item)

class DataLogger(QObject):
    """
    Parent DataLogger class. Provides some common features of data logging.
    """
    # signals
    sig_save_raw_active = pyqtSignal(bool)  # False when all saveNSamples are saved

    def __init__(self):
        """
        Constructor of DataLogger.

        :return:
        :rtype:
        """
        super(DataLogger, self).__init__()
        self.saveNSamples = 0
        self.save_raw_active = False

        self.save_active = True
        self.redis = redis.Redis()



        self.timer_ok = True
        self.timer = QTimer()
        self.timer.timeout.connect(self._set_timer_ok)

    @pyqtSlot(datetime, dict)
    @pyqtSlot(str, dict)
    def write_raw(self, timestamp, data):
        """
        Data slot for the DataLogger.
        Connect your raw data signals here. By default it will not save these data, unless you call save_n_raw_samples
         or set_save_raw_active.

        :param timestamp: ISO-8601 timestamp
        :type timestamp: str
        :param data: data dictionary of the raw data
        :type data: dict
        :return:
        :rtype:
        """
        if isinstance(timestamp,str):
            timestamp = datetime.fromisoformat(timestamp)

        if self.save_raw_active and self.timer_ok:
            if self.saveNSamples > 0:
                self.saveNSamples -= 1
            d = np.array(list(data.values())).T
            self._write_snapshot(timestamp.isoformat(), 'raw_data', d)
            print('snapshot saved ', timestamp.isoformat())
            if self.timer_ok and self.timer.isActive():
                self.timer_ok = False
        if self.saveNSamples == 0:
            self.save_raw_active = False
            self.sig_save_raw_active.emit(False)

    @pyqtSlot(str, dict, str, dict)
    def write(self, time, data, name, tags):
        """
        Writes data/measurements to the DataLogger.

        Can be started / stopped by calling set_save_active

        :param time: ISO-8601 timestamp
        :type time: str
        :param data: data dictionary.
        :type data: dict
        :param name: name of the measurement
        :type name: str
        :param tags: Additional tags to identify the measurement. See subclasses.
        :type tags: dict
        :return:
        :rtype:
        """
        if self.save_active:
            self._write(time, data, name, tags)

    @pyqtSlot(int, int)
    def save_n_raw_samples(self, n, save_every_N_seconds=0):
        """
        Save n raw data snapshots to the DataLogger with an average time of save_every_N_seconds in between the datasets

        :param n: Total number of datasets to save. If <0 all coming data will be saved (until save_raw_active is set
        to false)
        :type n: int
        :param save_every_N_seconds: average time between snapshots [s]
        :type save_every_N_seconds: float
        :return:
        :rtype:
        """
        print(f"Save {n} Sample(s) to File {self.filename if self.filename else '' }")
        self.saveNSamples = n
        self._set_timer(save_every_N_seconds)
        # self.save_raw_active = True

    @pyqtSlot(int)
    def _set_timer(self, save_every_N_seconds):
        """
        Sets the timer for raw data snapshots
        :param save_every_N_seconds: average time between snapshots [s]
        :type save_every_N_seconds: float
        :return:
        :rtype:
        """
        if save_every_N_seconds > 0:
            freq = 1000. * save_every_N_seconds
            self.timer.setInterval(freq)
            self.timer.start()
        else:
            self.timer_ok = True
            self.timer.stop()

    @pyqtSlot(bool)
    def set_save_raw_active(self, active):
        """
        Activate / Deactivate saving raw data snapshots

        :param active:  Activate / Deactivate saving raw data snapshots
        :type active: bool
        :return:
        :rtype:
        """
        self.save_raw_active = active

    def _set_timer_ok(self):
        self.timer_ok = True


class InfluxLogger(DataLogger):
    """
    This class provides an interface to influxdb to store time series data such as positions, fit results etc.
    """

    def __init__(self, host='localhost', port=8086, user='root', password='root', dbname='laserlock', debug=False):
        """
        Constructor of the Influx Logger.

        :param host: Host IP address.
        :type host: str
        :param port: Host port
        :type port: int
        :param user: influxdb username
        :type user: str
        :param password: influxdb password
        :type password: str
        :param dbname: influxdb databasename
        :type dbname: str
        :param debug: debug mode switch
        :type debug: bool
        :return:
        :rtype:
        """
        super(InfluxLogger, self).__init__()
        self.host = host
        self.port = port
        self.user = user
        self.password = 'root'
        self.dbname = dbname

        if not debug:
            self.client = InfluxDBClient(host, port, user, password, dbname)
            self.pandasclient = DataFrameClient(host, port, user, password, dbname, timeout=5)
            self.listener1 = Listener(redis.Redis(), ['influx_log'], self)
            self.listener2 = Listener(redis.Redis(), ['influx_annotations'], self)
            self.listener1.start()
            self.listener2.start()
        else:
            self.listener1 = Listener(redis.Redis(), ['influx_log'], self)
            self.listener2 = Listener(redis.Redis(), ['influx_annotations'], self)
            self.listener1.start()
            self.listener2.start()

        self.debug = debug

    @pyqtSlot(str, str, str, str)
    def _write_annotation(self, time, title, text, tags):
        """
        This function writes annotations to influx db that can be used as an overlay on timeseries plot in grafana.
        See 'Grafana - Annotations' for example usage.

        :param time: ISO-8601 formatted datetime of the annotation/event
        :type time: str
        :param title: Title of the event. E.g. 'Software upgrade'
        :type title: str
        :param text: Content of the annotation. E.g. 'Upgraded QtStabilizeIt to version v1.5'
        :type text: str
        :param tags: Tag of the annotation. These tags can be used to filter annotations/events in grafana. Can be used
        e.g. to specify the event type. E.g. 'Software_upgrade'
        :type tags: str
        :return:
        :rtype:
        """

        time = str(time)
        title = str(title)
        text = str(text)
        tags = str(tags)
        try:
            template = {
                "measurement": "events",
                "tags": {'event_tag': tags},
                "time": str(time),
                "fields": {'text': text,
                           'title': title}
            }
            if not self.debug:
                self.client.write_points([template])
            else:
                print(template)
        except Exception as e:
            print(e)

    @pyqtSlot(str, dict, str, dict)
    def _write(self, time, data, seriesname, tags):
        """
        Writes arbitrary measurements to influxdb. This function should be used to write fit results etc. to influxdb.
        .. |caution| The data given to influxdb is a dictionary. All keys of the dictionary will be used as tag VALUES
        for the tag KEY 'parameter_name'. This means do not pass a tag 'parameter_name' via the argument tags, because
        it will be overwritten when filling in the data


        :param time: ISO-8601 timestamp for the measurement
        :type time: str
        :param data: measurements to write. E.g. {'center': 123, 'fwhm': 10.1}
        :type data: dict
        :param seriesname: influxdb series name E.g. 'rubidium'
        :type seriesname: str
        :param tags: tags for the measurement. E.g. {'rb-group': 'Rb87f1'}
        :type tags: dict
        :return:
        :rtype:
        """
        # drop time from results if there...
        data.pop("time", None)
        data.pop('timestamp', None)
        # change timestamp key to 'time' in order to make it compatbile with influxdb
        template = {
            "measurement": str(seriesname),
            "tags": tags,
            "time": str(time),
            "fields": {}
        }

        # Create a list of template objects. One for each datapoint that should be written
        d = []
        for dkey, dvalue in data.items():
            d.append(copy.deepcopy(template))
            d[-1]['tags']['parameter_name'] = dkey  # possibly overwrites tags, if tag 'parameter_name' already exists!!
            d[-1]['fields']['value'] = dvalue

        try:
            if not self.debug:
                self.client.write_points(d, time_precision='u')
            else:
                print(d)
        except Exception as err:
            print(err)

    @pyqtSlot(str, str, dict)
    def _write_snapshot(self, timestamp, seriesname, data):
        """
        Writes a data snapshot to influxdb. Used to produce X-Y plots in grafana which is not supported otherwise.
        .. caution:: All datasets in data must have the same lenght!

        It fakes a x-y plot by creating a timeaxis from data['X']. This data is translated into a timeshift to a fixed
         offset in time (here 10/29/1984) to pretend beeing a timeseries. This snapshot can be plotted in grafana and
          gets updated/overwritten when new data comes in.

        If data['X'] exists it will use these values for the timeseries.
        If data['X'] doesn't exist, it will create an evenly spaced time axis with length equal to the first dataset
        in the dictionary

        .. todo:: right now the series only gets deleted when data['X'] exists. However, this is could cause problems.
        In case the number of sampling points changes for data['Y'], the x-axis will look different, and the old values
         don't get overwritten, but instead, the new datapoints are insterted in between...

        .. todo:: timestamp not used so far. Could maybe passed to influxdb as a tag and displayed in grafana.

        :param timestamp: ISO-8601 timestamp of the snapshot.
        :type timestamp: str
        :param seriesname: name of the influxdb series. E.g. 'spectrum_snapshots'
        :type seriesname: str
        :param data: data as a dictionary. E.g. {'X': [1,2,3], 'y1': [4,6,7], 'y2': [6,5,7]}
        :type data: dict
        :return:
        :rtype:
        """
        if not self.debug:
            n = len(data[list(data.keys())[0]])
            if 'X' in list(data.keys()):
                #self.pandasclient.delete_series('laserlock', str(seriesname))
                try:
                    self.client.query('DROP MEASUREMENT '+'"{}"'.format(str(seriesname)), database='laserlock')
                except:
                    print('No database ', str(seriesname))
                    pass
                offsets = data['X']
                idx = pd.to_timedelta(offsets, unit='s') + pd.datetime(1984, 10, 29)
                data.pop('X', None)
            else:
                idx = pd.bdate_range(start="29/10/1984", periods=n, freq='1S')

            df = pd.DataFrame(data, index=idx)
            self.pandasclient.write_points(df, str(seriesname), database='laserlock', time_precision='u')
        else:
            print('Write snapshot to influx', data)


class HDFLogger(DataLogger):
    """
    This class provides an interface to the write to HDF files.
    """

    # table format of timestamp tables for raw data
    class timestampData(tables.IsDescription):
        timestamp = tables.StringCol(27)

    def __init__(self, filename, mode='a', complevel=None, complib=None):
        """
        Creator of the HDFLogger. Use this class to log data to HDF files.

        There are two ways of using HDFLogger.
        You can either directly write data to the HDF File by calling _write_snapshot or _write, depending on the data
        structure.

        Alternatively you can use write_raw from the Parent class DataLogger, whichs allows you to store e.g. every N
        seconds.

        Example:
        --------

        dl = HDFLogger()
        your_data_source.data_signal.connect(dl.write)
        dl.save_n_raw_samples(10, 5)  # will save 10 data packages with an average time of 5 seconds between them



        :param filename: path to hdf file. Will be created if it doesn't exist
        :type filename: str
        :param mode: file write mode. Can be 'w' or 'a'. 'w' will delete all old data, 'a' will append data
        :type mode: str
        :param complevel: compression level from 0-9
        :type complevel: int
        :param complib: compression library. Possible values "zlib", "lzo", "bzip2" or "blosc"
        :type complib: str
        :return:
        :rtype:
        """
        super(HDFLogger, self).__init__()
        self.filename = filename
        self.mode = mode
        self.complevel = complevel
        self.complib = complib
        
        self.tables = {}

    @pyqtSlot(str)
    def change_file(self, filename, mode='a', complevel=None, complib=None):
        self.filename = filename
        self.mode = mode
        self.complevel=complevel
        self.complib=complib

    def _write_snapshot(self, timestamp, table_name, data):
        """
        Writes a snapshot to the HDF file as a EArray.
        The timestamp will be saved in a seperate table at tablename_time

        If tablename does not exist, it will be created.
        .. caution:: It is important for performance, that data is appended as rows and not as columns. When data
        has shape (N,M), make sure N>M !

        :param timestamp: ISO-8601 timestamp
        :type timestamp: str
        :param table_name: name of the table, where to append the data. Will be created if it doesn't exist as an hdf group.
        :type table_name: str
        :param data: raw_data array.  E.g. array([[ 0.16363736,  0.39793544,  0.10025356,  0.85649649],
       [ 0.81776367,  0.11136659,  0.116602  ,  0.88636642]]).T
        :type data: np.ndarray
        :return:
        :rtype:
        """
        with tables.open_file(self.filename, self.mode, complevel=self.complevel, complib=self.complib) as hdf:
            atom = tables.Atom.from_dtype(data.dtype)
            try:
                arr = hdf.get_node('/', table_name, 'EArray')
                arr.append(data)
                arr.flush()
    
                time_table = hdf.get_node('/', table_name + '_time')
                ts = time_table.row
                ts['timestamp'] = timestamp
                ts.append()
                time_table.flush()
            except:
                arr = hdf.create_earray('/', table_name, atom, shape=(0, len(data[0])), expectedrows=100000,
                                             createparents=True)
                arr.append(data)
                arr.flush()
    
                time_table = hdf.create_table('/', table_name + '_time', self.timestampData)
                ts = time_table.row
                ts['timestamp'] = timestamp
                ts.append()
                time_table.flush()

    def _get_dtype(self, data):
        """Given a dict, generate a nested numpy dtype"""
        fields = []
        for (key, value) in list(data.items()):
            if isinstance(key, str):
                key = key.encode('utf-8')
            # make strings go to the next 64 character boundary
            # pytables requires an 8 character boundary
            if isinstance(value, str):
                value += ' ' * (64 - (len(value) % 64))
                # pytables does not support unicode
                if isinstance(value, str):
                    value = value.encode('utf-8')
            elif isinstance(value, str):
                value += ' ' * (64 - (len(value) % 64))

            if isinstance(value, dict):
                fields.append((key, self._get_dtype(value)))

            else:
                value = np.array(value)
                fields.append((key, '%s%s' % (value.shape, value.dtype)))
        return np.dtype(fields)

    def _recurse_row(self, row, base, data):
        for (key, value) in list(data.items()):
            new = base + key
            if isinstance(value, dict):
                self._recurse_row(row, new + '/', value)
            else:
                row[new] = value

    def _add_row(self, tbl, data):
        """Add a new row to a table based on the contents of a dict.
        """
        row = tbl.row
        for (key, value) in list(data.items()):
            if isinstance(value, dict):
                self._recurse_row(row, key + '/', value)
            else:
                row[key] = value
        row.append()
        tbl.flush()

    def _append_column(self, table, group, name, column):

        """Returns a copy of `table` with an empty `column` appended named `name`."""

        description = table.description._v_colObjects.copy()
        description[name] = column
        copy = tables.Table(group, table.name + "_copy", description)

        # Copy the user attributes
        table.attrs._f_copy(copy)

        # Fill the rows of new table with default values
        for i in range(table.nrows):
            copy.row.append()
        # Flush the rows to disk
        copy.flush()

        # Copy the columns of source table to destination
        for col in description:
            getattr(copy.cols, col)[:] = getattr(table.cols, col)[:]

        # store original name
        # name = table.name

        # Remove the original table
        table.remove()

        # copy.name = name

        return copy

    @pyqtSlot(str, dict, str, dict)
    def _write(self, time, data, table_name, tags):
        """
        Writes arbitrary measurements to the HDF. This function should be used to write fit results etc. to the HDF file.

        .. caution:: tags will be converted to HDF columns. Make sure you don't use a key in the tags dict that exists
        in the data dict. It will be overwritten !!!

        :param time: ISO-8601 timestamp for the measurement
        :type time: str
        :param data: measurements to write. E.g. {'center': 123, 'fwhm': 10.1}
        :type data: dict
        :param table_name: HDF table name E.g. 'rubidium'
        :type table_name: str
        :param tags: tags for the measurement. E.g. {'rb-group': 'Rb87f1'}
        :type tags: dict
        :return:
        :rtype:
        """
        with tables.open_file(self.filename, self.mode, complevel=self.complevel, complib=self.complib) as hdf:
            data['time'] = str(time)  # dateutil.parser.parse(str(time))
            # data['time'] = dateutil.parser.parse(str(time))
            table_name = str(table_name)
            data.update(tags)
            if table_name not in list(self.tables.keys()):
                print(table_name, data)
                dtype = self._get_dtype(data)
                self.tables[table_name] = hdf.create_table('/', table_name, dtype)
            try:
                self._add_row(self.tables[table_name], data)
                hdf.flush()
            except KeyError as e:
                old_columns = self.tables[table_name].description._v_colObjects.copy()
                dtype = self._get_dtype(data)
                try:
                    self.tables[table_name + 'TMP'] = hdf.create_table('/', table_name + 'TMP', dtype)
                except tables.exceptions.NodeError:
                    self.tables[table_name+'TMP'].remove()
                    hdf.flush()
                    self.tables[table_name + 'TMP'] = hdf.create_table('/', table_name + 'TMP', dtype)

                for n in range(self.tables[table_name].nrows):
                    self.tables[table_name + 'TMP'].row.append()

                self.tables[table_name + 'TMP'].flush()

                for col in old_columns:
                    getattr(self.tables[table_name + 'TMP'].cols, col)[:] = getattr(self.tables[table_name].cols, col)[:]

                self.tables[table_name].remove()
                hdf.flush()

                self.tables[table_name] = self.tables.pop(table_name + 'TMP')
                self.tables[table_name].move('/', table_name)

                self._add_row(self.tables[table_name], data)


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)

    dl = InfluxLogger(debug=False)
    # dv = DataViewer('data/newdata.h5')
    # dl = HDFLogger('data/output_test.h5', 'w')
    #
    # dv.sig_plot_raw.connect(dl.write_raw)
    # dl.save_n_raw_samples(10)
    # # dv.sig_plot_raw.connect(dl.write)
    #
    # dv.gui.show()
    # dv.openFile()
    # dv.gui.tB_play.clicked.emit(True)

    sys.exit(app.exec_())
