import numpy as np
import pyqtgraph
from pyqtgraph import PlotWidget, intColor
from qtpy.QtCore import Slot
from qtpy.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QGroupBox,
    QCheckBox,
    QHBoxLayout,
)


class DataViewerGui(QWidget):
    def __init__(self, number_of_signals_on_left_axis=1, *args, **kwargs, ):
        """
        Gui class for DataViewer

        This class provides a gui for the DataViewer class.

        :param remote: Should the plot be done in a remote process ?
        :type remote: bool
        """
        super().__init__(*args, **kwargs)
        self.number_of_signals_on_left_axis = number_of_signals_on_left_axis
        layout = QVBoxLayout()
        self.plot1 = PlotWidget()
        self.legend = None
        self.gb = QGroupBox()
        self.gb.setTitle("Signals:")
        self.gb_layout = QHBoxLayout()
        self.gb.setLayout(self.gb_layout)
        self.checkboxes = {}
        layout.addWidget(self.gb)
        layout.addWidget(self.plot1)

        self.curves = None
        self.plotItems = None
        self.plotDataItems = None
        self.viewBoxes = None
        self.firstkey = None
        self.setLayout(layout)
        self.axes = None

    def setup_plots(self, data_dict):
        """
        Configures Plot
        Args:
            data_dict (dict): dict with data. string->np.array dictionary.

        Returns:
            None
        """

        # setup oscilloscope plots
        ## Handle view resizing
        def updateViews():
            ## view has resized; update auxiliary views to match
            for kk in self.viewBoxes.keys():
                self.viewBoxes[kk].setGeometry(self.plotItems[self.firstkey].vb.sceneBoundingRect())
            # p3.setGeometry(self.plotItems[firstkey].vb.sceneBoundingRect())

            ## need to re-update linked axes since this was called
            ## incorrectly while views had different shapes.
            ## (probably this should be handled in ViewBox.resizeEvent)
                self.viewBoxes[kk].linkedViewChanged(self.plotItems[self.firstkey].vb, self.viewBoxes[kk].XAxis)



        self.curves = {}
        self.plotItems = {}
        self.plotDataItems = {}
        self.viewBoxes = {}
        self.axes = {}
        for i, k in enumerate(data_dict.keys()):
            self.checkboxes[k] = QCheckBox(k, checked=True)
            self.gb_layout.addWidget(self.checkboxes[k])
            if i < self.number_of_signals_on_left_axis:
                self.plotItems[k] = self.plot1.plotItem
                self.plotItems[k].vb.sigResized.connect(updateViews)
                self.firstkey = k
                self.axes[k] = self.plotItems[k].getAxis('left')
                self.axes[k].setPen(intColor(i))
            elif i < self.number_of_signals_on_left_axis + 1:
                self.viewBoxes[k] = pyqtgraph.ViewBox()
                p1 = self.plotItems[self.firstkey]
                p1.showAxis('right')
                p1.scene().addItem(self.viewBoxes[k])
                self.axes[k] = p1.getAxis('right')
                self.axes[k].setPen(intColor(i))
                p1.getAxis('right').linkToView(self.viewBoxes[k])
                self.viewBoxes[k].setXLink(p1)
                p1.getAxis('right').setLabel(k, color=intColor(i))
            else:

                self.viewBoxes[k] = pyqtgraph.ViewBox()
                p3 = self.viewBoxes[k]
                self.axes[k] = pyqtgraph.AxisItem('right', pen=intColor(i))
                p1 = self.plotItems[self.firstkey]
                p1.layout.addItem(self.axes[k],2,2*i)
                p1.scene().addItem(p3)
                self.axes[k].linkToView(p3)
                p3.setXLink(p1)
                self.axes[k].setLabel(k, color=intColor(i))
            if i < self.number_of_signals_on_left_axis:
                self.curves[k] = self.plot1.plot(pen=intColor(i), name=k)
            else:

                self.curves[k] =pyqtgraph.PlotCurveItem(pen=intColor(i), name=k)
                self.viewBoxes[k].addItem(self.curves[k])

            self.checkboxes[k].toggled.connect(self.curves[k].setVisible)

        updateViews()
        self.plot1.showGrid(x=True, y=True, alpha=0.3)
        self.legend = self.plot1.addLegend()
        for i, k in enumerate(data_dict.keys()):
            self.legend.addItem(self.curves[k], k)
            self.checkboxes[k].toggled.connect(self.legend.items[i][0].setVisible)
            self.checkboxes[k].toggled.connect(self.legend.items[i][1].setVisible)
            self.checkboxes[k].toggled.connect(self.axes[k].setVisible)

    @Slot(str, dict)
    def plot(self, timestamp=None, data_dict=None):
        """
        Plots the data.

        Args:
            timestamp (str): timestamp. Is ignored. Just for compatibility.
            data_dict (dict): dict with data. string->np.array dictionary.

        Returns:
            None
        """
        if self.curves is None:
            self.setup_plots(data_dict)

        for k, v in data_dict.items():
            if isinstance(v, tuple):
                self.curves[k].setData(v[0],
                                       np.ascontiguousarray(v[1]), _callSync="off"
                                       )  # speed up by using _callSync=off
            else:
                self.curves[k].setData(
                    np.ascontiguousarray(v), _callSync="off"
                )  # speed up by using _callSync=off
