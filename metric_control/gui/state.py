from qtpy.QtCore import QState


class QStateNamed(QState):
    def __init__(self, *args, **kwargs):
        name = kwargs.pop("name", "")
        super().__init__(*args, **kwargs)
        self.setObjectName(name)
