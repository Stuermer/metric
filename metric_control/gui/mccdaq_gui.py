import datetime
import json
import os
import pathlib
import sys

import lmfit
import numpy as np
from scipy.interpolate import UnivariateSpline, LSQUnivariateSpline
from scipy.signal import argrelextrema

from metric_control.generated import mccdaq_pb2
from metric_control.gui.DataViewer_gui import DataViewerGui
from metric_control.gui.etalon import CFP
from metric_control.gui.fitter import FitterWorker, QTextEditLoggingHandler
from metric_control.gui.state import QStateNamed
from metric_control.servers.jacobians import (
    GaussianModelnn,
    LorentzianModelnn,
    PseudoVoigtnn2,
)

os.environ["QT_API"] = "PyQt5"
from qtpy.QtWidgets import (
    QWidget,
    QApplication,
    QPushButton,
    QVBoxLayout,
    QLabel,
    QHBoxLayout,
    QMenuBar,
    QAction,
    QActionGroup,
    QFileDialog,
    QComboBox,
    QTextEdit,
)
from qtpy import QtGui
from qtpy.QtGui import QFont
from qtpy.QtCore import Signal, Slot, QStateMachine, QThread, QTimer, QObject

from metric_control.clients.mccdaq_client import MCCDAQClient
from metric_control.gui.DataLogger import HDFLogger

import logging
import redis
import pickle as cPickle
from lmfit.models import (
    GaussianModel,
    LinearModel,
)

logging.basicConfig()


class Redisworker(QObject):
    sig_start_stop = Signal(bool)
    sig_new_data = Signal(str, dict)

    def __init__(self):
        super().__init__()
        self.redis = redis.StrictRedis("localhost")
        self.ready = False
        self.sig_start_stop.connect(self.set_stream)

    @Slot(bool)
    def set_stream(self, streaming):
        self.ready = streaming
        if self.ready:
            self.wait_for_data()

    def wait_for_data(self):
        while self.ready:
            spectrum_read = self.redis.brpop("spectra", timeout=1)
            if spectrum_read is not None:
                spectrum = cPickle.loads(spectrum_read[1])
                time = spectrum.pop("time", None)
                self.sig_new_data.emit(time, spectrum)


class RbFitterWorker2(FitterWorker):
    def __init__(self, path=None):
        super().__init__()
        if path is None:
            path = (
                    str(pathlib.Path(__file__).resolve().parent)
                    + "/rubidium_references.json"
            )
        with open(path, "r") as fp:
            self.references = json.load(fp)

        self.selected_groups = []
        for g in self.references:
            if isinstance(self.references[g], dict):
                if self.references[g]["selected"]:
                    self.selected_groups.append(g)

        print(self.selected_groups)

        self.model = None
        self.background_model = None
        self.background_pars = None
        self.pars = None
        self.px_to_mhz = None
        self.new_model = True
        self.x = None
        self.create_models()

    @Slot(np.ndarray)
    def set_px_to_mhz(self, coefficients):
        self.px_to_mhz = np.poly1d(coefficients)

    def create_models(self):
        # self.background_model = LinearModel(prefix=f"linear_")
        # for g in self.selected_groups:
        #     self.background_model += GaussianModelnn(prefix=f"{g}_")
        # self.background_pars = self.background_model.make_params()
        # self.pars = self.background_pars
        # self.model = self.background_model
        print(self.background_pars)

    def save_pars_to_json(self, pars=None, path="test.json"):
        if pars is None:
            pars = self.pars

        references = self.references.copy()
        for g in self.selected_groups:
            # write global intensity to file
            # references[g]["gaussian_amplitude"] = pars[f"{g}_amplitude"].value
            references["hfl_sigma"] = self.pars[f"hfl_sigma"].value
            references["hfl_sigma_g"] = self.pars[f"hfl_sigma_g"].value
            references["hfl_fraction"] = self.pars["hfl_fraction"].value

            # write amplitudes of hyperfine lines
            for hfl in references[g]["Iratio"]:
                references[g]["Iratio"][hfl] = self.pars[f"{g}_{hfl}_amplitude"].value

        with open(path, "w") as json_file:
            json.dump(references, json_file, indent=4)

    def fit(self, data):
        if not (self.busy or self.px_to_mhz is None):
            self.sig_is_busy.emit(True)

            self.busy = True
            if self.x is None or len(data) != len(self.x):
                self.x = np.arange(len(data))
            if self.new_model:
                # create models:
                # self.model = LinearModel(prefix="linear_")
                for g in self.selected_groups:
                    for hfl in self.references[g]["freq"]:
                        if self.model is None:
                            self.model = LorentzianModelnn(prefix=f"{g}_{hfl}_")
                        else:
                            self.model += LorentzianModelnn(prefix=f"{g}_{hfl}_")

                self.pars = self.model.make_params()
                expression = ""
                for i, c in enumerate(self.px_to_mhz.coefficients):
                    self.pars.add(f"poly_{i}", value=c, vary=False)
                    expression += f"poly_{len(self.px_to_mhz.coefficients) - 1 - i}*x**{i}+"

                expression = expression[:-1]
                poly_expression = lambda x: expression.replace("x", x)

                # set initial values
                fit_range_min_per_group = []
                fit_range_max_per_group = []
                for g in self.selected_groups:
                    x_center = (
                            self.references[g]["linecenter"]
                            + 1.0
                            / self.px_to_mhz(self.references[g]["linecenter"])
                            * self.references[g]["gaussian_offset"]
                    )

                    fit_range_min_per_group.append(
                        int(
                            x_center
                            - 3.5
                            * 1.0
                            / self.px_to_mhz(x_center)
                            * self.references[g]["gaussian_sigma"]
                        )
                    )
                    fit_range_max_per_group.append(
                        int(
                            x_center
                            + 3.5
                            * 1.0
                            / self.px_to_mhz(x_center)
                            * self.references[g]["gaussian_sigma"]
                        )
                    )

                self.pars.add("hfl_sigma", self.references["hfl_sigma"], vary=True)
                self.pars.add("hfl_sigma_g", self.references["hfl_sigma_g"],vary=False)
                self.pars.add("hfl_fraction", self.references["hfl_fraction"],vary=False)

                # rb groups hyper fine lines
                for g in self.selected_groups:
                    # self.pars.add(f"{g}_iratio", value=self.references[g]["globIntensity"],vary=False)
                    # self.pars.add(f"{g}_gaussian_sigma", value=self.references[g]["gaussian_sigma"])
                    x_center = self.references[g]["linecenter"]

                    for hfl in self.references[g]["freq"]:
                        x_hyperfine = (
                                x_center
                                + 1.0
                                / self.px_to_mhz(x_center)
                                * self.references[g]["freq"][hfl]
                                - 1.0
                                / self.px_to_mhz(x_center)
                                * list(self.references[g]["reference_line"].values())[0]
                        )

                        self.pars[f"{g}_{hfl}_sigma"].set(
                            expr="1./("
                                 + poly_expression(f"{g}_{hfl}_center")
                                 + ")*hfl_sigma"
                        )
                        # self.pars[f"{g}_{hfl}_sigma_g"].set(
                        #     expr="1./("+poly_expression(f"{g}_{hfl}_center")+")*hfl_sigma_g"
                        # )
                        #
                        # self.pars[f"{g}_{hfl}_fraction"].set(
                        #     value=self.references["hfl_fraction"], expr=f"hfl_fraction"
                        # )

                        self.pars[f"{g}_{hfl}_amplitude"].set(
                            value=data[int(x_hyperfine)]
                            # value=self.references[g]["Iratio"][hfl], vary=True
                        )
                        # if no crossover line
                        if len(hfl) == 4:
                            self.pars[f"{g}_{hfl}_center"].set(
                                value=x_hyperfine, vary=True,
                            )

                        # crossover line
                        else:
                            lines = hfl.split("c")
                            l1 = lines[1][:2]
                            l2 = lines[1][2:]
                            self.pars[f"{g}_{hfl}_center"].set(
                                value=x_hyperfine,
                                expr=f"({g}_{lines[0]}{l1}_center + {g}_{lines[0]}{l2}_center) / 2",
                            )

                self.fit_range = (
                    np.max([0] + [np.min(fit_range_min_per_group)]),
                    np.min([len(data) - 1] + [np.max(fit_range_max_per_group)]),
                )
            fit_range = self.fit_range

            # spline knots
            average_scaling = self.px_to_mhz(
                (self.x[fit_range[0]] + self.x[fit_range[1]]) / 2.0
            )
            t = np.linspace(
                self.x[fit_range[0]], self.x[fit_range[1]], int(30 * average_scaling)
            )

            indx = np.ones_like(self.x[fit_range[0]: fit_range[1]], dtype=bool)

            indx_knots = np.ones_like(t, dtype=bool)
            for g in self.selected_groups:
                for hfl in self.references[g]["freq"]:
                    xmin = int(
                        self.pars[f"{g}_{hfl}_center"].value - 10 * average_scaling
                    )
                    xmax = int(
                        self.pars[f"{g}_{hfl}_center"].value + 10 * average_scaling
                    )
                    indx[
                        np.logical_and(
                            self.x[fit_range[0]: fit_range[1]] > xmin,
                            self.x[fit_range[0]: fit_range[1]] < xmax,
                        )
                    ] = False
                    indx_knots[np.logical_and(t > xmin, t < xmax)] = False

            spl = LSQUnivariateSpline(
                self.x[fit_range[0]: fit_range[1]][indx],
                data[fit_range[0]: fit_range[1]][indx],
                t=t[indx_knots][1:-1],
                k=3,
            )
            #
            # # spline subtracted data
            # data_spl = data[fit_range[0]: fit_range[1]] - spl(self.x[fit_range[0]: fit_range[1]])
            # # find minimum data point and check whether negativ, if so add knot
            # idx_min = np.argmin(data_spl)
            # print(t[indx_knots][1:-1])
            #
            # if data_spl[idx_min] < 0 :
            #     indx[idx_min] = True
            #     indx[idx_min+1] = True
            #     indx[idx_min-1] = True
            #     new_t = np.append(t[indx_knots][1:-1], [idx_min+fit_range[0]])
            #     new_t.sort()
            #     print(new_t)
            #
            #     spl = LSQUnivariateSpline(
            #         self.x[fit_range[0]: fit_range[1]][indx],
            #         data[fit_range[0]: fit_range[1]][indx],
            #         t=new_t,
            #         k=3,
            #     )

            # data[fit_range[0]: fit_range[1]] -= spl(
            #     self.x[fit_range[0]: fit_range[1]]
            # )

            # rb groups hyper fine lines
            for g in self.selected_groups:
                # self.pars.add(f"{g}_iratio", value=self.references[g]["globIntensity"],vary=False)
                # self.pars.add(f"{g}_gaussian_sigma", value=self.references[g]["gaussian_sigma"])
                x_center = self.references[g]["linecenter"]

                for hfl in self.references[g]["freq"]:
                    x_hyperfine = (
                            x_center
                            + 1.0
                            / self.px_to_mhz(x_center)
                            * self.references[g]["freq"][hfl]
                            - 1.0
                            / self.px_to_mhz(x_center)
                            * list(self.references[g]["reference_line"].values())[0]
                    )
                    self.pars[f"{g}_{hfl}_amplitude"].set(
                        value=data[int(x_hyperfine)] - spl(x_hyperfine)
                        # value=self.references[g]["Iratio"][hfl], vary=True
                    )

            res = self.model.fit(
                data[fit_range[0]: fit_range[1]] -
                spl(
                    self.x[fit_range[0]: fit_range[1]]
                )
                ,
                params=self.pars,
                x=self.x[fit_range[0]: fit_range[1]],
                fit_kws={"maxfev": 500, },
            )

            result_dict = {
                "data": (
                    self.x[fit_range[0]: fit_range[1]],
                    data[fit_range[0]: fit_range[1]],
                ),
                "fit": (self.x[fit_range[0]: fit_range[1]], res.best_fit+spl(self.x[fit_range[0]: fit_range[1]])),
                "init": (self.x[fit_range[0]: fit_range[1]], res.init_fit+spl(self.x[fit_range[0]: fit_range[1]])),
                "spline": (self.x[fit_range[0]: fit_range[1]], spl(self.x[fit_range[0]: fit_range[1]])),
                "residuals": (self.x[fit_range[0]: fit_range[1]], res.residual),
            }

            if res.success:
                print("SUCCESS")
                self.sig_new_fit_result.emit(
                    res.fit_report(show_correl=False, sort_pars=True)
                )
                self.pars = res.params
                self.sig_new_residuals.emit("", result_dict)
                self.new_model = False
            else:
                print("NO SUCCESS")
                self.sig_new_fit_result.emit(
                    res.fit_report(show_correl=False, sort_pars=True)
                )
                self.pars = res.params
                self.sig_new_residuals.emit("", result_dict)
            self.busy = False
            self.save_pars_to_json()
            self.sig_is_busy.emit(False)


class RbFitterWorker(FitterWorker):
    def __init__(self, path=None):
        super().__init__()
        if path is None:
            path = (
                    str(pathlib.Path(__file__).resolve().parent)
                    + "/rubidium_references.json"
            )
        with open(path, "r") as fp:
            self.references = json.load(fp)

        self.selected_groups = []
        for g in self.references:
            if isinstance(self.references[g], dict):
                if self.references[g]["selected"]:
                    self.selected_groups.append(g)

        print(self.selected_groups)

        self.model = None
        self.background_model = None
        self.background_pars = None
        self.pars = None
        self.px_to_mhz = None
        self.new_model = True
        self.x = None
        self.create_models()

    @Slot(np.ndarray)
    def set_px_to_mhz(self, coefficients):
        self.px_to_mhz = np.poly1d(coefficients)

    def create_models(self):
        self.background_model = LinearModel(prefix=f"linear_")
        for g in self.selected_groups:
            self.background_model += GaussianModelnn(prefix=f"{g}_")
        self.background_pars = self.background_model.make_params()
        self.pars = self.background_pars
        self.model = self.background_model
        print(self.background_pars)

    def save_pars_to_json(self, pars=None, path="test.json"):
        if pars is None:
            pars = self.pars

        references = self.references.copy()
        for g in self.selected_groups:
            # write global intensity to file
            # references[g]["gaussian_amplitude"] = pars[f"{g}_amplitude"].value
            references["hfl_sigma"] = self.pars[f"hfl_sigma"].value
            references["hfl_sigma_g"] = self.pars[f"hfl_sigma_g"].value
            references["hfl_fraction"] = self.pars["hfl_fraction"].value
            references["doppler_sigma"] = self.pars[f"doppler_sigma_{g}"].value

            # write gaussian background parameters
            references[g]["gaussian_sigma"] = pars[f"doppler_sigma_{g}"].value
            for hfl in references[g]["gaussian_amplitudes"]:
                references[g]["gaussian_amplitudes"][hfl] = pars[
                    f"doppler_{g}_{hfl}_amplitude"
                ].value
            # write amplitudes of hyperfine lines
            for hfl in references[g]["Iratio"]:
                references[g]["Iratio"][hfl] = self.pars[f"{g}_{hfl}_amplitude"].value

        with open(path, "w") as json_file:
            json.dump(references, json_file, indent=4)

    def fit(self, data):
        if not (self.busy or self.px_to_mhz is None):
            self.sig_is_busy.emit(True)

            self.busy = True
            if self.x is None or len(data) != len(self.x):
                self.x = np.arange(len(data))
            if self.new_model:
                # create models:
                self.model = LinearModel(prefix="linear_")
                for g in self.selected_groups:
                    # self.model += GaussianModelnn(prefix=f"{g}_")
                    for hfl in self.references[g]["freq"]:
                        self.model += PseudoVoigtnn2(prefix=f"{g}_{hfl}_")
                        # self.model += MoffatModel(prefix=f"{g}_{hfl}_")
                        if len(hfl) == 4:
                            self.model += GaussianModelnn(prefix=f"doppler_{g}_{hfl}_")
                        # self.model += LorentzianModelnn(prefix=f"{g}_{hfl}_")

                self.pars = self.model.make_params()
                expression = ""
                for i, c in enumerate(self.px_to_mhz.coefficients):
                    self.pars.add(f"poly_{i}", value=c, vary=False)
                    expression += f"poly_{len(self.px_to_mhz.coefficients) - 1 - i}*x**{i}+"

                expression = expression[:-1]
                poly_expression = lambda x: expression.replace("x", x)

                # set initial values
                # background
                self.pars["linear_intercept"].set(value=np.max(data))
                self.pars["linear_slope"].set(value=0.0, vary=False)
                # rb groups gaussian background
                fit_range_min_per_group = []
                fit_range_max_per_group = []
                for g in self.selected_groups:
                    x_center = (
                            self.references[g]["linecenter"]
                            + 1.0
                            / self.px_to_mhz(self.references[g]["linecenter"])
                            * self.references[g]["gaussian_offset"]
                    )

                    fit_range_min_per_group.append(
                        int(
                            x_center
                            - 3.5
                            * 1.0
                            / self.px_to_mhz(x_center)
                            * self.references[g]["gaussian_sigma"]
                        )
                    )
                    fit_range_max_per_group.append(
                        int(
                            x_center
                            + 3.5
                            * 1.0
                            / self.px_to_mhz(x_center)
                            * self.references[g]["gaussian_sigma"]
                        )
                    )

                self.pars.add("hfl_sigma", self.references["hfl_sigma"], vary=False)
                self.pars.add("hfl_sigma_g", self.references["hfl_sigma_g"], vary=False)
                self.pars.add(
                    "hfl_fraction", self.references["hfl_fraction"], vary=False
                )

                # rb groups hyper fine lines
                for g in self.selected_groups:
                    # self.pars.add(f"{g}_iratio", value=self.references[g]["globIntensity"],vary=False)
                    # self.pars.add(f"{g}_gaussian_sigma", value=self.references[g]["gaussian_sigma"])
                    x_center = self.references[g]["linecenter"]
                    self.pars.add(f"doppler_{g}_pressureshift", 0.0)
                    self.pars.add(
                        f"doppler_sigma_{g}", self.references["doppler_sigma"]
                    )
                    first_hfl = None

                    for hfl in self.references[g]["freq"]:
                        x_hyperfine = (
                                x_center
                                + 1.0
                                / self.px_to_mhz(x_center)
                                * self.references[g]["freq"][hfl]
                                - 1.0
                                / self.px_to_mhz(x_center)
                                * list(self.references[g]["reference_line"].values())[0]
                        )

                        self.pars[f"{g}_{hfl}_sigma"].set(
                            expr="1./("
                                 + poly_expression(f"{g}_{hfl}_center")
                                 + ")*hfl_sigma"
                        )
                        self.pars[f"{g}_{hfl}_sigma_g"].set(
                            expr="1./("
                                 + poly_expression(f"{g}_{hfl}_center")
                                 + ")*hfl_sigma_g"
                        )

                        self.pars[f"{g}_{hfl}_fraction"].set(
                            value=self.references["hfl_fraction"], expr=f"hfl_fraction"
                        )

                        self.pars[f"{g}_{hfl}_amplitude"].set(
                            value=self.references[g]["Iratio"][hfl], vary=False
                        )
                        # if no crossover line
                        if len(hfl) == 4:
                            self.pars[f"{g}_{hfl}_center"].set(
                                value=x_hyperfine,
                                vary=False,
                                # vary=self.references[g]["fitted"][hfl]
                            )

                            self.pars[f"doppler_{g}_{hfl}_sigma"].set(
                                expr="1./("
                                     + poly_expression(f"{g}_{hfl}_center")
                                     + f")*doppler_sigma_{g}"
                            )
                            self.pars[f"doppler_{g}_{hfl}_amplitude"].set(
                                value=self.references[g]["gaussian_amplitudes"][hfl],
                                max=0.0,
                            )
                            self.pars[f"doppler_{g}_{hfl}_center"].set(
                                value=x_hyperfine,
                                expr=f"{g}_{hfl}_center+doppler_{g}_pressureshift",
                            )
                            if first_hfl is None:
                                first_hfl = hfl
                        # crossover line
                        else:
                            lines = hfl.split("c")
                            l1 = lines[1][:2]
                            l2 = lines[1][2:]
                            self.pars[f"{g}_{hfl}_center"].set(
                                value=x_hyperfine,
                                expr=f"({g}_{lines[0]}{l1}_center + {g}_{lines[0]}{l2}_center) / 2",
                            )

                fit_range = (
                    np.max([0] + [np.min(fit_range_min_per_group)]),
                    np.min([len(data)] + [np.max(fit_range_max_per_group)]),
                )
                # for i in range(10):
                #     for g in self.selected_groups:
                #         for hfl in self.references[g]["freq"]:
                #             self.pars[f"{g}_{hfl}_amplitude"].set(
                #                 vary=((i+1) % 2) == 0
                #             )
                #             if len(hfl) == 4:
                #                 self.pars[f"doppler_{g}_{hfl}_amplitude"].set(
                #                     vary=(i%2) == 0
                #                     )
                #     print(self.pars[f"{g}_{hfl}_amplitude"].value, self.pars[f"{g}_{hfl}_amplitude"].vary)
                indx = np.ones_like(self.x[fit_range[0]: fit_range[1]], dtype=bool)
                for g in self.selected_groups:
                    for hfl in self.references[g]["freq"]:
                        xmin = int(self.pars[f"{g}_{hfl}_center"].value - 30)
                        xmax = int(self.pars[f"{g}_{hfl}_center"].value + 30)
                        indx[
                            np.logical_and(
                                self.x[fit_range[0]: fit_range[1]] > xmin,
                                self.x[fit_range[0]: fit_range[1]] < xmax,
                            )
                        ] = False

                spl = UnivariateSpline(
                    self.x[fit_range[0]: fit_range[1]][indx],
                    data[fit_range[0]: fit_range[1]][indx],
                    k=3,
                    s=0.0005,
                )
                # spl2 = UnivariateSpline(self.x[fit_range[0]: fit_range[1]][indx], data[fit_range[0]: fit_range[1]][indx],
                #                        k=3, s=0.0001)
                # spl3 = UnivariateSpline(self.x[fit_range[0]: fit_range[1]][indx], data[fit_range[0]: fit_range[1]][indx],
                #                        k=3, s=0.001)

                # plt.figure()
                # plt.plot(self.x[fit_range[0] : fit_range[1]][indx], spl(self.x[fit_range[0] : fit_range[1]][indx])-data[fit_range[0] : fit_range[1]][indx])
                # plt.show()
                res = self.model.fit(
                    data[fit_range[0]: fit_range[1]][indx],
                    params=self.pars,
                    x=self.x[fit_range[0]: fit_range[1]][indx],
                    # method="l",
                    fit_kws={"maxfev": 5, },
                )
                # self.pars = res.params

            else:
                fit_range = 0, len(data)
                res = self.model.fit(
                    data[fit_range[0]: fit_range[1]],
                    self.pars,
                    x=self.x[fit_range[0]: fit_range[1]],
                )

            comps = res.eval_components(x=self.x)
            result_dict = {
                "data": data,
                "fit": (self.x[fit_range[0]: fit_range[1]][indx], res.best_fit),
                "init": (self.x[fit_range[0]: fit_range[1]][indx], res.init_fit),
                "spline": (
                    self.x[fit_range[0]: fit_range[1]],
                    spl(self.x[fit_range[0]: fit_range[1]]),
                ),
                "spline_res": (
                    self.x[fit_range[0]: fit_range[1]],
                    data[fit_range[0]: fit_range[1]]
                    - spl(self.x[fit_range[0]: fit_range[1]]),
                ),
                # "spline_res2": (self.x[fit_range[0]: fit_range[1]],
                #                data[fit_range[0]:fit_range[1]] - spl2(self.x[fit_range[0]: fit_range[1]])),
                # "spline_res3": (self.x[fit_range[0]: fit_range[1]],
                #                data[fit_range[0]:fit_range[1]] - spl3(self.x[fit_range[0]: fit_range[1]])),
                # "residuals": (self.x[fit_range[0]: fit_range[1]][indx], res.residual),
            }

            if res.success:
                print("SUCCESS")
                self.sig_new_fit_result.emit(
                    res.fit_report(show_correl=False, sort_pars=True)
                )
                self.pars = res.params
                self.sig_new_residuals.emit("", result_dict)
                self.new_model = False
            else:
                print("NO SUCCESS")
                self.sig_new_fit_result.emit(
                    res.fit_report(show_correl=False, sort_pars=True)
                )

                self.pars = res.params
                self.sig_new_residuals.emit("", result_dict)
            # self.busy = False
            self.save_pars_to_json()
        # self.sig_is_busy.emit(False)


class EtalonFitterWorker(FitterWorker):
    def __init__(self):
        super().__init__()
        self.peak_model = GaussianModel(prefix="peak_")
        self.linear_model = LinearModel(prefix="background_")
        self.model = self.peak_model + self.linear_model
        self.pars = None
        self.peak_pars = None
        self.linear_pars = self.linear_model.make_params(0.0, slope=0.0)

        self.new_model = True
        self.x = None

    def set_new_model(self, modelname):
        print(f"New model: {modelname}")
        self.peak_model = getattr(lmfit.models, modelname)(prefix="peak_")
        self.new_model = True
        self.model = self.peak_model + self.linear_model
        self.x = None

    def fit(self, data):
        self.sig_is_busy.emit(True)
        if self.x is None or len(data) != len(self.x):
            self.x = np.arange(len(data))
        if self.new_model:
            self.peak_pars = self.peak_model.guess(data, x=self.x)
            self.pars = self.peak_pars + self.linear_pars
            res = self.model.fit(data, params=self.pars, x=self.x)
        else:
            res = self.model.fit(data, self.pars, x=self.x)

        if res.success:
            comps = res.eval_components(x=self.x)

            result_dict = {
                "data": data,
                "fit": res.best_fit,
                "peakmodel": comps["peak_"],
                "background": comps["background_"],
                "residuals": res.residual,
            }
            self.sig_new_fit_result.emit(res.fit_report(show_correl=False))
            self.pars = res.params
            self.sig_new_residuals.emit("", result_dict)
            self.new_model = False
        self.sig_is_busy.emit(False)


class CFPitterWorker(FitterWorker):
    def __init__(self):
        super().__init__()
        self.pars = {}
        self.peaks = {}
        self.fit_ranges = {}
        self.results = {}
        self.fit_range_px = 10
        self.success = True
        self.peak_pars = None
        self.peak_modelname = "MoffatModel"
        self.peak_threshold = 0.7
        self.fitrange = 3  # this number times fwhm in px is the fitrange for each peak in each direction
        self.new_model = True
        self.x = None

    def set_new_model(self, modelname):
        print(f"New model: {modelname}")
        self.peak_modelname = modelname
        self.new_model = True

    def analyse_cfp(self, data):
        xmax_all = argrelextrema(data, np.greater_equal)[0]
        xmax = xmax_all[data[xmax_all] > self.peak_threshold * np.max(data)]
        peakmax = np.max(data)
        fwhm_px = float(data[data > 0.4 * peakmax].size) / float(xmax.size)
        return xmax, fwhm_px

    def fit(self, data):
        self.sig_is_busy.emit(True)
        if self.x is None or len(data) != len(self.x):
            self.x = np.arange(len(data))
        best_fit = np.zeros_like(data)
        residuals = np.zeros_like(data)
        x_values = []
        if self.new_model:
            x_center, fwhm_px = self.analyse_cfp(data)
            assert len(x_center > 0), "No CFP peaks found, check signal !"
            self.fit_range_px = int(fwhm_px * self.fitrange)

            for i, x in enumerate(x_center):
                xmin = x - self.fit_range_px
                xmax = x + self.fit_range_px
                if xmin > 0 and xmax < self.x[-1]:
                    peak = getattr(lmfit.models, self.peak_modelname)(
                        prefix=f"peak_{i}_"
                    )
                    self.peaks[i] = peak

                    pars = peak.guess(data[xmin:xmax], x=self.x[xmin:xmax])
                    self.pars[i] = pars
                    res = peak.fit(data[xmin:xmax], x=self.x[xmin:xmax], params=pars)
                    if res.success:
                        self.results[i] = res
                    self.success = self.success and res.success
                    best_fit[xmin:xmax] += res.best_fit
                    residuals[xmin:xmax] += res.residual
                    x_values.append(res.params[f"peak_{i}_center"].value)
                    # print(res.fit_report())

            # self.peak_pars = self.peak_model.guess(data, x=self.x)
            # self.pars = self.peak_pars + self.linear_pars
            # res = self.model.fit(data, params=self.pars, x=self.x)
        else:

            for p in self.pars.keys():
                xmin = int(self.pars[p][f"peak_{p}_center"].value - self.fit_range_px)
                xmax = int(self.pars[p][f"peak_{p}_center"].value + self.fit_range_px)
                res = self.peaks[p].fit(
                    data[xmin:xmax], x=self.x[xmin:xmax], params=self.results[p].params
                )
                self.success = self.success and res.success
                if res.success:
                    self.results[p] = res
                best_fit[xmin:xmax] += res.best_fit
                residuals[xmin:xmax] += res.residual
                x_values.append(res.params[f"peak_{p}_center"].value)

            # res = self.model.fit(data, self.pars, x=self.x)

        if self.success:
            x = np.arange(len(x_values)) * 187.0
            lf = np.polyfit(x_values, x, 5)
            result_dict = {
                "data": data,
                "fit": best_fit,
                "residuals": residuals,
                "ramp": (x, x - np.polyval(lf, x_values)),
            }
            self.sig_new_ramp_polynomial.emit(np.polyder(np.poly1d(lf)).coefficients)
            self.sig_new_residuals.emit("", result_dict)
            self.new_model = False
        self.sig_is_busy.emit(False)


class RbFitter(QWidget):
    sig_receive_data = Signal(str, dict)
    sig_new_rb_data = Signal(np.ndarray)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fitter_worker = RbFitterWorker2()
        self._thread = QThread(self)
        self._thread.start()
        self.fitter_worker.moveToThread(self._thread)

        self.busy = False
        self.d_mean = []

        layout = QVBoxLayout()

        self.fit_results = QTextEdit()
        self.plot = DataViewerGui(
            number_of_signals_on_left_axis=4, windowTitle="Rb Fit"
        )
        layout.addWidget(self.plot)
        layout.addWidget(self.fit_results)

        self.setLayout(layout)

        self.sig_receive_data.connect(self.select_data)
        self.sig_new_rb_data.connect(self.fitter_worker.fit)
        self.fitter_worker.sig_is_busy.connect(self.is_busy)
        self.fitter_worker.sig_new_residuals.connect(self.plot.plot)
        self.fitter_worker.sig_new_fit_result.connect(self.fit_results.setText)

    def is_busy(self, flag):
        self.busy = flag

    def select_data(self, timestamp, data):
        print(timestamp)
        if not self.busy:
            self.d_mean.append(data["Rb"])
            if len(self.d_mean) > 4:
                self.sig_new_rb_data.emit(np.mean(np.array(self.d_mean), axis=0))
                self.d_mean = []
        else:
            print("Still busy fitting. Skipping Etalon Spectrum")


class EtalonFitter(QWidget):
    sig_receive_data = Signal(str, dict)
    sig_new_etalon_data = Signal(np.ndarray)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fitter_worker = EtalonFitterWorker()
        self._thread = QThread(self)
        self._thread.start()
        self.fitter_worker.moveToThread(self._thread)

        self.busy = False

        layout = QVBoxLayout()
        self.model_select = QComboBox()
        self.model_select.addItems(
            [
                "GaussianModel",
                "LorentzianModel",
                "VoigtModel",
                "SkewedGaussianModel",
                "SkewedVoigtModel",
                "DonaichModel",
            ]
        )
        layout.addWidget(self.model_select)
        self.fit_results = QTextEdit()
        self.plot = DataViewerGui(
            number_of_signals_on_left_axis=4, windowTitle="Etalon Fit"
        )
        layout.addWidget(self.plot)
        layout.addWidget(self.fit_results)

        self.setLayout(layout)
        self.model_select.currentTextChanged.connect(self.fitter_worker.set_new_model)
        self.sig_receive_data.connect(self.select_data)
        self.sig_new_etalon_data.connect(self.fitter_worker.fit)
        self.fitter_worker.sig_is_busy.connect(self.is_busy)
        self.fitter_worker.sig_new_residuals.connect(self.plot.plot)
        self.fitter_worker.sig_new_fit_result.connect(self.fit_results.setText)

    def is_busy(self, flag):
        self.busy = flag

    def select_data(self, timestamp, data):
        if not self.busy:
            self.sig_new_etalon_data.emit(data["Etalon"])
        else:
            print("Still busy fitting. Skipping Etalon Spectrum")


class CFPFitter(QWidget):
    sig_receive_data = Signal(str, dict)
    sig_new_etalon_data = Signal(np.ndarray, str)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fitter_worker = CFP()
        self._thread = QThread(self)
        self._thread.start()
        self.fitter_worker.moveToThread(self._thread)

        self.busy = False

        layout = QVBoxLayout()
        # self.model_select = QComboBox()
        # self.model_select.addItems(
        #     [
        #         "GaussianModel",
        #         "LorentzianModel",
        #         "VoigtModel",
        #         "SkewedGaussianModel",
        #         "SkewedVoigtModel",
        #         "DonaichModel",
        #     ]
        # )
        # layout.addWidget(self.model_select)
        self.log = QTextEdit()
        handler = QTextEditLoggingHandler(self.log)
        handler.setFormatter(
            logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s", '%H:%M:%S'))
        handler.setLevel(logging.DEBUG)
        logger = logging.getLogger("etalon")
        logger.addHandler(handler)

        self.plot = DataViewerGui(
            number_of_signals_on_left_axis=2, windowTitle="Etalon Fit"
        )
        layout.addWidget(self.plot)
        layout.addWidget(self.log)

        self.setLayout(layout)
        # self.model_select.currentTextChanged.connect(self.fitter_worker.set_new_model)
        self.sig_receive_data.connect(self.select_data)
        self.sig_new_etalon_data.connect(self.fitter_worker.fit)
        self.fitter_worker.sig_is_busy.connect(self.is_busy)
        self.fitter_worker.sig_new_residuals.connect(self.plot.plot)
        self.fitter_worker.sig_new_fit_result.connect(self.log.setText)

    def is_busy(self, flag):
        self.busy = flag

    def select_data(self, timestamp, data):
        if not self.busy:
            self.sig_new_etalon_data.emit(data["CFP"], datetime.datetime.utcnow().isoformat())
        else:
            print("Still busy fitting. Skipping Etalon Spectrum")


class MCCDAQ(QWidget):
    sig_start_stop_requested = Signal(bool)
    sig_connect_requested = Signal()
    sig_disconnect_requested = Signal()
    sig_ramp_running = Signal(bool)
    sig_reload_settings_requested = Signal()

    sig_change_filename = Signal(str)
    sig_start_stop_save_requested = Signal(bool)
    sig_save_raw_requested = Signal(int, int)

    sig_ready = Signal()
    sig_busy = Signal()
    sig_error = Signal()

    def __init__(self, *args, **kwargs):
        """

        Args:
            client (GuiderCameraClient): Guide Camera Client
        """

        super().__init__(*args, **kwargs)
        self.client = MCCDAQClient()
        self._thread = QThread(self)
        self._thread.start()
        self.client.moveToThread(self._thread)

        self.redis_worker = Redisworker()
        self._thread = QThread(self)
        self._thread.start()
        self.redis_worker.moveToThread(self._thread)

        self.dl = HDFLogger(f"{datetime.datetime.utcnow().isoformat()}.hdf")

        layout = QVBoxLayout()

        # buttons
        self.pb_connect = QPushButton("Connect")
        self.pb_disconnect = QPushButton("Disconnect")
        self.pb_reload = QPushButton("Reload settings")
        self.pb_start_ramp = QPushButton("Start ramp...")
        self.pb_stop_ramp = QPushButton("Stop ramp...")

        self.pb_start_ramp.setEnabled(False)
        self.pb_stop_ramp.setEnabled(False)
        self.pb_reload.setEnabled(False)

        control_layout = QHBoxLayout()

        button_layout = QVBoxLayout()
        button_layout.addWidget(self.pb_connect)
        button_layout.addWidget(self.pb_disconnect)
        button_layout.addWidget(self.pb_start_ramp)
        button_layout.addWidget(self.pb_stop_ramp)
        button_layout.addWidget(self.pb_reload)
        self.filename_label = QLabel(f"{self.dl.filename}")

        control_layout.addLayout(button_layout)

        # Menu

        self.menu = QMenuBar(self)
        self.fileMenu = self.menu.addMenu("File")
        self.dataLoggerMenu = self.menu.addMenu("DataLogger")

        self.menufreq_Snapthot = self.dataLoggerMenu.addMenu("Frequency of Snapshots")
        self.menuN_Snapshot = self.dataLoggerMenu.addMenu("Number of Snapshots")
        exitAction = QAction(QtGui.QIcon("exit24.png"), "Exit", self)
        self.fileMenu.addAction(exitAction)
        exitAction.triggered.connect(self.close)
        exitAction.setShortcut("Ctrl+Q")

        self.freqMenu = QActionGroup(self.menufreq_Snapthot)
        self.freqMenu.setExclusive(True)

        frequencies = [-1, 1, 3, 5, 10, 60, 300, 600]
        actions = []
        for f in frequencies:
            name = "Sample every " + str(f) + " s"
            actions.append(QAction(name, self.freqMenu, checkable=True))
        self.menufreq_Snapthot.addActions(actions)
        actions[0].setChecked(True)
        # map(lambda action: action.triggered.connect(self.updateSnapshotTimer), actions)

        # add Menubar for number of snapshots
        self.NsnapshotMenu = QActionGroup(self.menuN_Snapshot)
        self.NsnapshotMenu.setExclusive(True)
        n = [-1, 1, 10, 100, 1000, 10000]
        actions = []
        for f in n:
            name = "Number of snapshots to save: " + str(f)
            actions.append(QAction(name, self.NsnapshotMenu, checkable=True))
        self.menuN_Snapshot.addActions(actions)
        actions[0].setChecked(True)
        self.change_name_action = QAction("Set filename")
        self.dataLoggerMenu.addAction(self.change_name_action)
        self.change_name_action.triggered.connect(self.request_name_change)
        self.save_action = QAction("save snapshot", self, checkable=True)
        self.save_action.triggered.connect(self.request_save_raw)
        self.dataLoggerMenu.addAction(self.save_action)

        self.menu.show()
        layout.addLayout(control_layout)
        # plot widgets
        self.raw_plotter = DataViewerGui(windowTitle="Raw data")
        layout.addWidget(self.raw_plotter)

        # ramp status - put all into labels
        self.labels = {}
        for field in mccdaq_pb2.state.DESCRIPTOR.fields:
            self.labels[field.name] = QLabel(f"{field.name}",)
            self.labels[field.name].setFont(QFont("Monospace"))

        self.labels["Save to: "] = QLabel(f"Save to: {self.dl.filename}")

        label_layout = QVBoxLayout()

        for label in self.labels.values():
            label_layout.addWidget(label)

        control_layout.addLayout(label_layout)

        ## idle state
        self.unknown_state = QStateNamed(name="Unknown")
        self.waiting_state = QStateNamed(name="Waiting")
        self.streaming_state = QStateNamed(name="Streaming")

        # Transitions
        self.unknown_state.addTransition(self.client.sig_ready, self.waiting_state)
        self.waiting_state.addTransition(self.sig_ramp_running, self.streaming_state)
        self.waiting_state.addTransition(self.client.shutdown, self.unknown_state)
        self.streaming_state.addTransition(self.client.shutdown, self.unknown_state)

        # StateMachine
        self.machine = QStateMachine()
        self.machine.addState(self.unknown_state)
        self.machine.addState(self.waiting_state)
        self.machine.addState(self.streaming_state)
        self.machine.setInitialState(self.unknown_state)

        # connect signals
        self.redis_worker.sig_new_data.connect(self.raw_plotter.plot)
        self.sig_start_stop_requested.connect(self.redis_worker.sig_start_stop)
        self.sig_connect_requested.connect(self.client.initialize)
        self.sig_disconnect_requested.connect(self.client.__del__)
        self.sig_reload_settings_requested.connect(self.client.reload_settings)

        self.sig_start_stop_save_requested.connect(self.dl.set_save_raw_active)
        self.redis_worker.sig_new_data.connect(self.dl.write_raw)

        # client-> ui
        self.client.sig_new_state.connect(self.update_gui)

        # ui
        QTimer.singleShot(10, lambda: self.sig_start_stop_requested.emit(True))

        self.pb_connect.clicked.connect(self.sig_connect_requested.emit)
        self.pb_disconnect.clicked.connect(self.sig_disconnect_requested.emit)

        # ui -> client
        self.pb_start_ramp.clicked.connect(lambda: self.client.start_stop_scan(True))
        self.pb_stop_ramp.clicked.connect(lambda: self.client.start_stop_scan(False))
        self.pb_reload.clicked.connect(self.sig_reload_settings_requested.emit)
        self.sig_change_filename.connect(self.dl.change_file)
        self.sig_change_filename.connect(
            lambda fn: self.labels["Save to: "].setText(f"Save to: {fn}")
        )
        self.dl.sig_save_raw_active.connect(self.save_action.setChecked)
        self.sig_save_raw_requested.connect(self.dl.save_n_raw_samples)

        # states -> ui
        self.unknown_state.entered.connect(self.on_enter_unknown_state)
        self.waiting_state.entered.connect(self.on_enter_waiting_state)
        self.streaming_state.entered.connect(self.on_enter_streaming_state)

        self.setLayout(layout)
        self.machine.start()

    def request_name_change(self):
        fileName = QFileDialog.getSaveFileName(
            self,
            None,
            f"{datetime.datetime.utcnow().isoformat()}.hdf",
            filter="HDF files (*.hdf *.h5)",
        )
        print(fileName)
        if fileName[0]:
            self.sig_change_filename.emit(fileName[0])

    def request_save_raw(self):
        if self.save_action.isChecked():
            text = str(self.NsnapshotMenu.checkedAction().text())
            N = [int(s) for s in text.split() if s.lstrip("-").isdigit()][0]
            text = str(self.freqMenu.checkedAction().text())
            freq = [int(s) for s in text.split() if s.lstrip("-").isdigit()][0]
            self.sig_save_raw_requested.emit(N, freq)
            self.sig_start_stop_save_requested.emit(True)
        else:
            self.sig_start_stop_save_requested.emit(False)

    @Slot()
    def on_enter_unknown_state(self):
        print("UNKNOWN STATE")
        self.pb_connect.setEnabled(True)
        self.pb_start_ramp.setEnabled(False)
        self.pb_stop_ramp.setEnabled(False)
        self.pb_stop_ramp.hide()
        self.pb_disconnect.hide()
        self.pb_connect.show()
        self.pb_reload.setEnabled(False)

    @Slot()
    def on_enter_waiting_state(self):
        print("WAITING STATE")
        self.pb_start_ramp.setEnabled(True)
        self.pb_stop_ramp.setEnabled(True)
        self.pb_connect.hide()
        self.pb_disconnect.show()
        self.pb_reload.setEnabled(True)

    @Slot()
    def on_enter_streaming_state(self):
        pass

    @Slot()
    def on_pb_clicked(self):
        self.sig_start_stop_requested.emit(True)

    @Slot(mccdaq_pb2.state)
    def update_gui(self, state):
        print("NEW STATE")
        if state.ramp_running:
            self.pb_stop_ramp.setEnabled(True)
            self.pb_stop_ramp.show()
            self.pb_start_ramp.hide()
        else:
            self.pb_stop_ramp.hide()
            self.pb_start_ramp.setEnabled(True)
            self.pb_start_ramp.show()

        for field in mccdaq_pb2.state.DESCRIPTOR.fields:
            if not field.name == "shutters":
                self.labels[field.name].setText(
                    f"{field.name:16}: {getattr(state, field.name):8}"
                )


if __name__ == "__main__":
    app = QApplication([])
    # data = dict(zip(["Rb", "etalon", "cfp", "intensity"], np.random.random((4, 50))))
    # widget = DataViewerGui()
    # widget.plot("", data)
    widget = MCCDAQ()
    widget.show()
    # cfp = CFPFitter()
    # widget.redis_worker.sig_new_data.connect(cfp.sig_receive_data)
    # cfp.show()

    # etalon = EtalonFitter()
    # widget.redis_worker.sig_new_data.connect(cfp.sig_receive_data)
    # cfp.show()

    # ew = RbFitter()
    # widget.redis_worker.sig_new_data.connect(ew.sig_receive_data)
    # cfp.fitter_worker.sig_new_ramp_polynomial.connect(ew.fitter_worker.set_px_to_mhz)
    # ew.show()
    sys.exit(app.exec_())
