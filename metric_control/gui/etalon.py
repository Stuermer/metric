"""
.. module:: etalon
   :platform: Unix
   :synopsis: A module for fitting the etalon signals.

.. moduleauthor:: Julian Stuermer <stuermer@uchicago.edu>


"""
import cProfile
import logging
import os
from datetime import datetime
from multiprocessing import Pool

import PyQt5
import numpy as np
import parmap
from scipy.optimize import least_squares
from scipy.signal import argrelextrema, filtfilt, butter

from metric_control.gui import parameter
from metric_control.gui.fitter import FitterWorker
from metric_control.servers import redis_helper

os.environ['MKL_NUM_THREADS'] = '1'

logger = logging.getLogger("etalon")


def dfunc_moffat(p, *ys, **xs):
    xx = ys[0]
    A = p[0]
    da = (1. / (1. + (xx - p[1]) ** 2 / p[2] ** 2)) ** p[3]
    dmu = (2. * A * p[3] * (xx - p[1]) * (1. / (1. + (xx - p[1]) ** 2 / p[2] ** 2)) ** (1. + p[3])) / p[2] ** 2
    db = A * (1. / (1. + (xx - p[1]) ** 2 / p[2] ** 2)) ** p[3] * np.log(1. / (1 + (xx - p[1]) ** 2 / p[2] ** 2))
    ds = (2. * A * p[3] * (xx - p[1]) ** 2 * (1 / (1 + (xx - p[1]) ** 2 / p[2] ** 2)) ** (1. + p[3])) / p[2] ** 3
    return np.array([da, dmu, ds, db]).T


def moffatfunc(p, x):
    return p[0] / (((x - p[1]) / p[2]) ** 2 + 1) ** p[3]


def errorfunc_moffat(p, x, z):
    return moffatfunc(p, x) - z


def lorentzfunc(p, x):
    return p[0] / (((x - p[1]) / p[2]) ** 2 + 1)


def errorfunc_lorentz(p, x, z):
    return lorentzfunc(p, x) - z


def do_fitting(peak_instance, x, y):
    return peak_instance.fit(x, y)


class Peak:
    def __init__(self, model, fitrange=3, fwhm_px=10, name='', key='', errorfunc=errorfunc_lorentz):

        self.name = name
        self.key = key

        self.modelname = model
        self.parameter_names = []
        self.paramHist = None

        self.last_fit_successful = False

        self.fitrange = fitrange
        self.fwhm_px = fwhm_px
        self.validFit = False

        self.errorfunc = errorfunc
        self.dfunc = None

        self.active = True
        self.assigned_peak = None
        self.residuals = None

        self.timestamp = None
        # for convenience
        self.center = None
        self.last_center = None
        self.chi_sqr = None

    def apply_shift(self, shift):
        self.center += shift

    def fwhm(self, params):
        return 2. * params[2] * np.sqrt(2. ** (1. / params[3]) - 1.)

    def initial_guess(self, xx, yy):
        return [np.max(yy), np.average(xx, weights=yy), self.fwhm_px / 2., 1.]

    def set_center(self, newcenter):
        self.center = newcenter
        self.last_center = newcenter

    def fit(self, x, y):
        if self.active:
            peak = self.center
            self.idx = np.logical_and(x > (peak - self.fwhm_px * self.fitrange),
                                      x < (peak + self.fwhm_px * self.fitrange))
            xx = x[self.idx]
            yy = y[self.idx]

            if len(xx) > 0 and len(yy) > 0:
                if not self.validFit:
                    p0 = self.initial_guess(xx, yy)
                    self.paramHist = parameter.ParameterHistoryArray(p0, N=25)

                p0 = self.paramHist.getParameter()
                p0[1] = np.average(xx, weights=yy)

                if self.dfunc:
                    ls_result = least_squares(self.errorfunc, p0, args=(xx, yy), verbose=0, jac=self.dfunc, max_nfev=50)
                else:
                    ls_result = least_squares(self.errorfunc, p0, args=(xx, yy), verbose=0, max_nfev=50)

                p1 = ls_result.x
                success = ls_result.success
                self.residuals = ls_result.fun
            else:
                success = False

            if success:
                self.validFit = True
                self.paramHist.addParameter(p1)
                self.last_center = self.center
                self.center = p1[1]
                res = dict(zip(self.parameter_names, p1))
                res['fwhm'] = self.fwhm(p1)
                res['chisqr'] = sum(ls_result.fun ** 2)
                res['nfev'] = float(ls_result.nfev)
                return success, res, self.center, self.last_center, self.name, self.residuals, self.idx, self.active, self
            else:
                logger.debug(f"{self.name} at {self.center}")
                return False, {}, self.center, self.last_center, self.name, self.residuals, self.idx, self.active, self
        else:
            logger.debug(f"Peak not active: {self.name} at {self.center}")
            return False, {}, self.center, self.last_center, self.name, self.residuals, self.idx, self.active, self


class CFPPeak(Peak):
    def __init__(self, **kws):
        super(CFPPeak, self).__init__(**kws)
        self.parameter_names = ['amplitude', 'center', 'sigma', 'beta']
        self.errorfunc = errorfunc_moffat
        self.dfunc = dfunc_moffat


class LorenzPeak(Peak):
    def __init__(self, **kws):
        super(LorenzPeak, self).__init__(**kws)
        self.parameter_names = ['amplitude', 'center', 'sigma']
        self.errorfunc = errorfunc_lorentz

    def fwhm(self, params):
        return 2. * params[2]

    def initial_guess(self, xx, yy):
        return [np.max(yy), np.average(xx, weights=yy), self.fwhm_px / 2.]


class Etalon(FitterWorker):
    """
    Etalon class.

    Handels fitting of an etalon spectrum with arbitrary number of peaks.
    """

    def __init__(self, fit_range=10, peak_threshold=0.5, filter_raw_data=True, debug=0, profile=False, ui=False,
                 cutoff_n_px_start=0, cutoff_n_peaks_start=0, cutoff_n_peaks_end=0, prefix='CFP', model='Lorentziannn',
                 etalontype='planar', d=0.4, refindex=1.0002752, num_cpu=1):
        super().__init__()
        self.model = model
        self.peaks = {}
        self.pool = Pool(num_cpu)

        self.cutoff_n_peaks_start = cutoff_n_peaks_start
        self.cutoff_n_peaks_end = cutoff_n_peaks_end
        self.cutoff_n_px_start = cutoff_n_px_start

        self.x = None
        self.fwhm_px = None
        self.fitrange = fit_range
        self.peak_threshold = peak_threshold
        self.filter_raw_data = filter_raw_data
        self.signal_filter = butter(3, 0.05)
        self.last_spectrum = None

        self.queue_N = 500
        self.number_of_valid_peaks = 0

        self.valid_fit = False
        self.prefix = prefix

        self.debug = debug
        if profile:
            logger.info('enable profile')
            self.profile = cProfile.Profile()
            self.profile.enable()

        self.d = d
        self.refindex = refindex
        self.etalontype = etalontype
        # theoretical FSR in MHz
        self.theoretical_FSR = 2.99792458E2 / (self.d * self.refindex * 2.)

    def _add_peak(self, key):
        if self.model == 'Moffat':
            self.peaks[key] = CFPPeak(model=self.model, fitrange=self.fitrange, fwhm_px=self.fwhm_px,
                                      name=self.etalontype + str(key), key=key)
        elif self.model == 'Lorentziannn':
            self.peaks[key] = LorenzPeak(model=self.model, fitrange=self.fitrange, fwhm_px=self.fwhm_px,
                                         name=self.etalontype + str(key), key=key)
        else:
            raise NotImplementedError(f"Model {self.model} not implemented!")

    def _get_valid_peaks(self, xmax, x):
        valid_peaks = np.ones(len(xmax), dtype=bool)
        valid_peaks[:self.cutoff_n_peaks_start] = False
        if self.cutoff_n_peaks_end > 0:
            valid_peaks[-self.cutoff_n_peaks_end:] = False

        valid_peaks[xmax - self.fitrange * self.fwhm_px < 0] = 0
        valid_peaks[xmax + self.fitrange * self.fwhm_px > len(x)] = 0
        valid_peaks[xmax < self.cutoff_n_px_start + self.fitrange * self.fwhm_px] = 0

        peaks = xmax[valid_peaks]
        return peaks, len(peaks)

    @PyQt5.QtCore.pyqtSlot(np.ndarray, str)
    def fit(self, y, timestamp=datetime.utcnow().isoformat()):
        """
        Fits all etalon peaks in the spectrum.

        :param x: x-data. If None x=np.arange(len(y))
        :type x: np.ndarray
        :param y: y-data
        :type y: np.ndarray
        :param timestamp: ISO-8601 timestamp
        :type timestamp: str
        :return: (centers of all peaks, fit successful)
        :rtype: tuple
        """
        self.sig_is_busy.emit(True)
        if not self.busy:
            self.busy = True
            if self.x is None or len(y) != len(self.x):
                self.x = np.arange(len(y))

            if not self.valid_fit:
                result, names, self.valid_fit = self.createInitialModel(self.x, y)
                self.busy = False
                self.sig_is_busy.emit(False)
                return result, names, self.valid_fit
            else:
                ymin = np.min(y)
                y -= ymin
                # filter
                y_tmp = y.copy()
                if self.filter_raw_data:
                    if self.filter_raw_data:
                        y_tmp = filtfilt(self.signal_filter[0], self.signal_filter[1], y)

                # calc average shift:
                # shift_x = np.arange(1 - len(x), len(x))
                # correlation = np.correlate(y, self.last_spectrum, 'full')
                # average_peak_shift = shift_x[correlation.argmax()] * 1.0 * x[-1] / len(x)
                # print 'Etalon shift: ', average_peak_shift
                # if self.multiple_peaks:
                #     if abs(average_peak_shift) > self.FSR_px / 2.:
                #         print 'Etalon shift corrected -----------------------------------'
                #         if average_peak_shift < 0:
                #             average_peak_shift += self.FSR_px
                #         else:
                #             average_peak_shift -= self.FSR_px

                # delete peaks which are already fitted or within the fitrange of peaks that are fitted
                left_most_peak_center = self.x[-1]
                left_most_peak_key = 0
                right_most_peak_center = self.x[0]
                right_most_peak_key = 0

                fit_range_x = self.fitrange * self.fwhm_px
                for key, p in self.peaks.items():
                    if p.center < left_most_peak_center:
                        left_most_peak_center = p.center
                        left_most_peak_key = key
                    if p.center > right_most_peak_center:
                        right_most_peak_center = p.center
                        right_most_peak_key = key

                if self.multiple_peaks:
                    x_blocked = np.logical_and(self.x > left_most_peak_center - fit_range_x,
                                               self.x < right_most_peak_center + fit_range_x)
                    x_blocked = np.logical_or(x_blocked, self.x < self.x[self.cutoff_n_px_start] + fit_range_x)
                    x_blocked = np.logical_or(x_blocked, self.x > self.x[-1] - fit_range_x)

                    y_tmp[x_blocked] = 0.

                # local maxima
                xmax_all = argrelextrema(y_tmp, np.greater)[0]
                xmax = xmax_all[y[xmax_all] > self.peak_threshold * np.max(y)]

                # check if new peak
                if self.multiple_peaks:
                    if len(xmax) == 1:
                        # new peak is on the left
                        if self.x[xmax[0]] < np.mean(self.x):
                            i = left_most_peak_key - 1
                        else:
                            i = right_most_peak_key + 1

                        self._add_peak(i)
                        self.peaks[i].set_center(self.x[xmax[0]])
                        self.peaks[i].fit(self.x, y)
                        logger.debug('Added ', self.peaks[i].name, self.peaks[i].key, ' peak with center ',
                                     self.peaks[i].center)
                        redis_helper.write_annotation_to_influx(timestamp, 'FIT event',
                                                                'Peak ' + p.name + ' is added at' + str(p.center),
                                                                'cfp')

                    for p in self.peaks.values():
                        if p.center > self.x[-1] - fit_range_x or p.center < self.x[
                            self.cutoff_n_px_start] + fit_range_x:
                            if p.active:
                                logger.debug(f'Peak {p.name} is falling off at {p.center}')
                                redis_helper.write_annotation_to_influx(timestamp, 'FIT event',
                                                                        'Peak ' + p.name + ' is falling off at ' + str(
                                                                            p.center), 'cfp')
                                p.active = False

                        else:
                            if not p.active:
                                logger.debug(f'Peak {p.name} is now fitted at {p.center}')
                                redis_helper.write_annotation_to_influx(timestamp, 'FIT event',
                                                                        'Peak ' + p.name + ' is now fitted at' + str(
                                                                            p.center), 'cfp')
                                p.active = True

                z = np.zeros(len(self.x))
                zz = np.zeros(len(self.x))

                idx_background_signal = np.ones(len(self.x), dtype=np.bool)
                if self.multiple_peaks:
                    idx_background_signal[:int(self.cutoff_n_px_start + 2 * fit_range_x)] = 0
                    idx_background_signal[-int(fit_range_x * 2):] = 0
                else:
                    idx_background_signal = np.zeros(len(self.x), dtype=np.bool)
                    idx_background_signal[self.x < self.x[0] + 100] = 1

                listz = parmap.map(do_fitting, self.peaks.values(), self.x, y, pool=self.pool)
                success, resultlist, centers, last_centers, center_names, residuals, idx, active, etalonpeakobjects = zip(
                    *listz)
                active_index = np.array(active, dtype=bool)
                success = np.array(success)[active_index]
                resultlist = np.array(resultlist)[active_index]
                centers = np.array(centers)[active_index]
                last_centers = np.array(last_centers)[active_index]
                center_names = np.array(center_names)[active_index]
                residuals = np.array(residuals)[active_index]
                idx = np.array(idx)[active_index]

                results = dict(zip(center_names, resultlist))

                for ii, index in enumerate(idx):
                    z[index] = residuals[ii]
                    idx_background_signal[index] = 0

                for pobject in etalonpeakobjects:
                    self.peaks[pobject.key] = pobject

                average_peak_shift = np.mean(centers - last_centers)
                for p in self.peaks.values():
                    if not p.active:
                        p.apply_shift(average_peak_shift)

                zz[idx_background_signal] = y[idx_background_signal]

                result_dict = {
                    "data": y,
                    # "fit": y,
                    "residuals": z,
                }
                # self.sig_data_plot.emit(timestamp,
                #                         {'data': {'x': self.x, 'y': y + ymin}, 'residuals': {'x': self.x, 'y': z}})
                self.sig_new_residuals.emit("", result_dict)

                fit_error_occured = len(success) - np.sum(np.array(success))
                if fit_error_occured == 0:
                    success = True
                    for dk, dv in results.items():
                        if isinstance(dv, dict):
                            if not self.multiple_peaks:
                                redis_helper.write_to_influx(timestamp, dv, self.etalontype, {'peak_name': dk})
                    xx = np.arange(len(centers)) * self.theoretical_FSR
                    lf = np.polyfit(centers, xx, 5)
                    self.sig_new_ramp_polynomial.emit(np.polyder(np.poly1d(lf)).coefficients)
                else:
                    success = False
                background_values = {'rms_background': np.std(y[idx_background_signal]),
                                     'mean_background': np.mean(y[idx_background_signal] + ymin)}

                redis_helper.write_to_influx(timestamp, background_values, self.etalontype, {})

                self.last_spectrum = y
                self.busy = False
                self.sig_is_busy.emit(False)
                return centers, center_names, success

    def createInitialModel(self, x, y):
        """
        Create an initial Model for the Etalon - no assumption other than it is an Etalon spectrum are needed.
        It first does an autocorrelation on the Etalon spectrum to retrieve an average Line profile. This is then used
        as an initial guess for all lines

        :param x: x-values
        :param y: y-values (intensity), usually in [V]
        :param model: Type of the model of an individual peak, can be 'Gaussian', 'Lorentzian', 'Voigt', or 'PseudoVoigt'
        :return:
        """
        # clear old peaks
        self.peaks.clear()

        logger.info(f'Create initial Model for {self.etalontype} Etalon ********************************** ')
        # ymin = np.mean(np.sort(y)[100])
        y -= np.min(y)

        # filter
        if self.filter_raw_data:
            ytmp = filtfilt(self.signal_filter[0], self.signal_filter[1], y)
        else:
            ytmp = y

        # local maxima
        xmax_all = argrelextrema(ytmp, np.greater_equal)[0]
        xmax = xmax_all[y[xmax_all] > self.peak_threshold * np.max(y)]
        peakmax = np.max(y)

        self.fwhm_px = float(y[y > 0.4 * peakmax].size) / float(xmax.size)

        # delete peaks where the fitrange is outside of the recorded range
        if self.multiple_peaks:
            peaks, self.number_of_valid_peaks = self._get_valid_peaks(xmax, x)
        else:
            peaks = xmax

        # print basic information
        logger.info(f"Number of peaks found: {self.number_of_valid_peaks}")

        success = True
        centers = []
        center_names = []
        for i, peak in enumerate(peaks):
            self._add_peak(i)
            self.peaks[i].center = x[peak]
            # valid fit only stays true if no errors
            success_fit = self.peaks[i].fit(x, y)
            success = success and success_fit
            centers.append(self.peaks[i].center)
            center_names.append(self.peaks[i].name)
            logger.debug(f"Added {self.peaks[i].name}, {self.peaks[i].key} peak with center {self.peaks[i].center}")

        self.last_spectrum = y
        if self.multiple_peaks:
            self.FSR_px = np.mean(np.ediff1d(centers))
            xx = np.arange(len(centers)) * self.theoretical_FSR
            lf = np.polyfit(centers, xx, 5)
            self.sig_new_ramp_polynomial.emit(np.polyder(np.poly1d(lf)).coefficients)

            logger.debug(f"FSR in px: {self.FSR_px}")

        return np.array(centers), center_names, success


class CFP(Etalon):
    def __init__(self, **kws):
        super(CFP, self).__init__(**kws, etalontype="confocal")

        self.theoretical_FSR = 2.99792458E2 / (self.d * self.refindex * 4.)
        self.signal_filter = butter(3, 0.5)
        self.multiple_peaks = True


class Planar(Etalon):
    def __init__(self, **kws):
        super(Planar, self).__init__(**kws)
        self.theoretical_FSR = 2.99792458E2 / (self.d * self.refindex * 2.)
        self.signal_filter = butter(3, 0.05)
        self.multiple_peaks = False


if __name__ == '__main__':
    pass
