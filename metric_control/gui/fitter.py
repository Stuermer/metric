import logging

import numpy as np
from qtpy.QtCore import QObject, Signal


class QTextEditLoggingHandler(logging.Handler):
    def __init__(self, text_edit):
        logging.Handler.__init__(self)
        self.text_edit = text_edit

    def emit(self, record):
        self.text_edit.append(self.format(record))


def main():
    import sys
    app = QApplication(sys.argv)
    window = QTextEdit()
    window.show()
    logger = logging.getLogger("GUI")
    handler = QTextEditLoggingHandler(window)
    handler.setFormatter(
        logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s", '%H:%M:%S'))
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    logger.debug("DEBUG")
    logger.warning('WARNING')
    logger.info("INFO")

    sys.exit(app.exec_())


class FitterWorker(QObject):
    sig_is_busy = Signal(bool)
    sig_new_residuals = Signal(str, dict)
    sig_new_fit_result = Signal(str)
    sig_new_ramp_polynomial = Signal(np.ndarray)

    def __init__(self):
        super().__init__()
        self.busy = False
