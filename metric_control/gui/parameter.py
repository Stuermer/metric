import numpy as np


class ParameterHistory(object):
    def __init__(self, parameter, N=10, N_meta=10):
        super(ParameterHistory, self).__init__()

        self.hist = {}
        self.hist_meta = {}
        if parameter:
            for key, value in parameter.valuesdict().items():
                self.hist[key] = [value]

        self.N = N
        self.N_meta = N_meta

    def addParameter(self, parameter):
        for key, value in parameter.valuesdict().items():
            self.hist[key].append(value)
            if len(self.hist[key]) > self.N:
                del self.hist[key][0]

    def getParameter(self, parameter):
        for key in parameter.valuesdict().iterkeys():
            parameter[key].set(np.median(np.array(self.hist[key])))

    def addMeta(self, meta_data):
        for key, value in meta_data.items():
            if key in self.hist_meta:
                self.hist_meta[key].append(value)
                if len(self.hist_meta[key]) > self.N_meta:
                    del self.hist_meta[key][0]
            else:
                self.hist_meta[key] = [value]

    def getMeta(self, key, allowed_relative_deviation=20., allowed_sigma_deviation=None, initial_limits=None):
        if key in self.hist_meta:
            median = np.median(np.array(self.hist_meta[key]))
            if len(self.hist_meta[key]) == self.N_meta:
                sigma = np.std(np.array(self.hist_meta[key]))
                if allowed_sigma_deviation is not None:
                    return median + allowed_sigma_deviation * sigma, \
                           median - allowed_sigma_deviation * sigma, median
                else:
                    return (1. + allowed_relative_deviation / 100.) * median, \
                           (1. - allowed_relative_deviation / 100.) * median, median
            else:
                if initial_limits is None:
                    return median + 2. * median, median - 2. * median, median
                else:
                    return initial_limits[0], initial_limits[1], median
        else:
            return None


class ParameterHistoryArray(object):
    def __init__(self, parameter, N=10, N_meta=10):
        super(ParameterHistoryArray, self).__init__()

        self.hist = []
        self.hist_meta = {}
        self.hist.append(parameter)

        self.N = N
        self.N_meta = N_meta

    def addParameter(self, parameter):
        self.hist.append(parameter)
        if len(self.hist) > self.N:
            del self.hist[0]

    def getParameter(self):
        return np.median(np.array(self.hist), axis=0)

    def addMeta(self, meta_data):
        for key, value in meta_data.items():
            if key in self.hist_meta:
                self.hist_meta[key].append(value)
                if len(self.hist_meta[key]) > self.N_meta:
                    del self.hist_meta[key][0]
            else:
                self.hist_meta[key] = [value]

    def getMeta(self, key, allowed_relative_deviation=20., allowed_sigma_deviation=None, initial_limits=None):
        if key in self.hist_meta:
            median = np.median(np.array(self.hist_meta[key]))
            if len(self.hist_meta[key]) == self.N_meta:
                sigma = np.std(np.array(self.hist_meta[key]))
                if allowed_sigma_deviation is not None:
                    return median + allowed_sigma_deviation * sigma, \
                           median - allowed_sigma_deviation * sigma, median
                else:
                    return (1. + allowed_relative_deviation / 100.) * median, \
                           (1. - allowed_relative_deviation / 100.) * median, median
            else:
                if initial_limits is None:
                    return median + 2. * median, median - 2. * median, median
                else:
                    return initial_limits[0], initial_limits[1], median
        else:
            return None


if __name__ == "__main__":
    pass
