"""
.. module:: rubidium
   :platform: Unix
   :synopsis: A module for fitting the rubidium signals.

.. moduleauthor:: Julian Stuermer <stuermer@uchicago.edu>

This is a test description of the rubidium module.

"""
import logging
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('PyQt5').setLevel(logging.WARNING)
import PyQt5
import matplotlib

# matplotlib.use("Qt5Agg")
from scipy.interpolate import LSQUnivariateSpline

matplotlib.use("Qt4Agg")

import itertools
import json
from datetime import datetime
from multiprocessing import Pool
from metric_control.servers.parameter import ParameterHistoryArray

import matplotlib.pyplot as plt
import numpy as np
import parmap
from scipy.optimize import least_squares
from scipy.stats import sigmaclip

import redis_helper

# from Signal import SignalGui

from metric_control.servers.Signal import SignalGui
plt.ion()

logger = logging.getLogger("Rb")


def lorentzfunc(p, x):
    return (
            p[0] / (((x - p[1]) / p[2]) ** 2 + 1)
            + p[3] / (((x - p[4]) / p[5]) ** 2 + 1)
            + p[6] / (((x - p[7]) / p[8]) ** 2 + 1)
            + p[9] / (((x - p[10]) / p[11]) ** 2 + 1)
            + p[12] / (((x - p[13]) / p[14]) ** 2 + 1)
            + p[15] / (((x - p[16]) / p[17]) ** 2 + 1)
    )


def errorfunc(p, x, z):
    residuals = lorentzfunc(p, x) - z
    return residuals


def do_rb_fitting(fit_arguments):
    try:
        res = least_squares(
            errorfunc,
            fit_arguments["p0"],
            bounds=(fit_arguments["min"], fit_arguments["max"]),
            args=(fit_arguments["x"], fit_arguments["y"]),
            verbose=0,
        )
        res.__setattr__('group', fit_arguments['group'])
        return res
    except ValueError as e:
        return False


class Rubidium(PyQt5.QtCore.QObject):
    # signals
    sig_rb_raw = PyQt5.QtCore.pyqtSignal(np.ndarray, np.ndarray, str)
    sig_rb_res = PyQt5.QtCore.pyqtSignal(np.ndarray, np.ndarray, str)

    sig_data_plot = PyQt5.QtCore.pyqtSignal(str, dict)
    sig_current_result = PyQt5.QtCore.pyqtSignal(dict)

    # signals
    sig_init_current_result = PyQt5.QtCore.pyqtSignal(str, dict)
    sig_save_current_result = PyQt5.QtCore.pyqtSignal(str, dict)

    sig_initial_parameter_list = PyQt5.QtCore.pyqtSignal(dict)
    sig_fit_parameter_list = PyQt5.QtCore.pyqtSignal(dict)

    sig_save_to_hdf = PyQt5.QtCore.pyqtSignal(str, dict, str, dict)

    def __init__(
            self,
            debug=0,
            ui=False,
            datalogger=None,
            profile=False,
    ):
        """
        Rb Fitting
        data from Daniel Adam Steck
        http://steck.us/alkalidata/rubidium87numbers.pdf
        http://steck.us/alkalidata/rubidium85numbers.pdf

        :param debug:
        :param ui:
        :param datalogger:
        :param profile:
        """
        super(Rubidium, self).__init__()
        self.guess_MHz_per_px = 1.78
        self.valid_fit = False
        self.debug = debug
        self.references = {}

        # self.available_groups = ["rb85f3", "rb85f2", "rb87f1", "rb87f2"]
        self.available_groups = ["rb87f1"]
        self.pool = Pool(len(self.available_groups))
        self.rb_models = {}
        self.rb_fitted_models = {}

        self.scaling = None
        self.rb_models_scaling = {}
        self.rb_models_shifts = {}
        self.rb_models_fitidx = {}
        self.rb_models_centers = {}
        self.average_scaling = 1.0
        self.prefix = "Rb"

        self.parameterHist = {}

        with open("metric_control/servers/rubidium_references.json", "r") as fp:
            self.references = json.load(fp)

        self.N_params = 18
        self.initial_guesses_MHz = {}
        self.fit_range_MHz = {}
        self.parameter_names = {}
        self.ref_centers = {}
        self.ref_weights = {}
        self.bounds_min = {}
        self.bounds_max = {}

        self.spline_knots_MHz = {}
        self.spline_knots = {}

        for group in self.available_groups:
            self.initial_guesses_MHz[group] = np.zeros(self.N_params)
            self.bounds_min[group] = np.ones(self.N_params) * -1.0 * np.inf
            self.bounds_max[group] = np.ones(self.N_params) * np.inf
            self.spline_knots_MHz[group] = np.array(list(self.references[group]['splineknots'].values()))

            self.parameter_names[group] = [""] * self.N_params

            self.fit_range_MHz[group] = np.array(
                [
                    self.references[group]["limits"]["min"],
                    self.references[group]["limits"]["max"],
                ]
            )

            self.ref_centers[group] = [0.0] * 6
            self.ref_weights[group] = [0.0] * 6
            for i, line in enumerate(self.references[group]["freq"]):
                self.initial_guesses_MHz[group][i * 3] = self.references[group]['globIntensity'] * \
                                                         self.references[group][
                                                             "Iratio"
                                                         ][
                                                             line
                                                         ]  # amplitude
                self.initial_guesses_MHz[group][i * 3 + 1] = self.references[group][
                    "freq"
                ][
                    line
                ]  # centers
                self.initial_guesses_MHz[group][i * 3 + 2] = 8.0
                self.parameter_names[group][i * 3] = line + "_amplitude"
                self.parameter_names[group][i * 3 + 1] = line + "_center"
                self.parameter_names[group][i * 3 + 2] = line + "_sigma"
                self.bounds_min[group][i * 3 + 2] = 0.0
                self.ref_centers[group][i] = (
                        self.references[group]["freq"][line] + self.references[group]["f0"]
                )
                self.ref_weights[group][i] = self.references[group]["weights"][line]

        self.initial_guesses = {}
        self.fit_range = {}

        self.resultqueue = {}
        self.current_result = {}
        self.queue_N = 500

        self.fit_kwords = {
            "xtol": 1.0e-9,
            "ftol": 1.0e-9,
            "epsfcn": 1.0e-10,
            "factor": 1,
        }

        self.dl = datalogger
        if profile:
            print("enable profile")
            self.profile = cProfile.Profile()
            self.profile.enable()

        self.ui = ui
        if ui:
            self.gui = SignalGui(name="Rb", color="r")
            self.sig_data_plot.connect(self.gui.plot)
            self.sig_current_result.connect(self.gui.showStatistics)

            self.sig_initial_parameter_list.connect(self.gui.setupTimeSeriesPplot)
            self.sig_fit_parameter_list.connect(self.gui.showTimeSeries)

            self.gui.show()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("disable profile")
        self.profile.disable()
        self.profile.dump_stats("rb.profile")

    def __del__(self):
        print("disable profile")
        self.profile.disable()
        self.profile.dump_stats("rb.profile")

    def plot_rb_lines(self):
        plt.figure()
        p = 0
        plt.annotate("Rb85_F2", xy=(self.f2rb85, 0.8))
        for l, x in self.reference85D2_f2.items():
            plt.vlines(x + self.references["rb85f2"]["f0"], -1, 1, "b", label=l)
            plt.annotate(l, xy=(x + self.references["rb85f2"]["f0"], p))
            p += 0.1

        p = 0
        plt.annotate("Rb85_F3", xy=(self.f3rb85, 0.8))
        for l, x in self.reference85D2_f3.items():
            plt.vlines(x + self.references["rb85f3"]["f0"], -1, 1, "g", label=l)
            plt.annotate(l, xy=(x + self.references["rb85f3"]["f0"], p))
            p += 0.1

        p = 0
        plt.annotate("Rb87_F2", xy=(self.f2rb87, 0.8))
        for l, x in self.reference87D2_f2.items():
            plt.vlines(x + self.references["rb87f2"]["f0"], -1, 1, "r", label=l)
            plt.annotate(l, xy=(x + self.references["rb87f2"]["f0"], p))
            p += 0.1

        p = 0
        plt.annotate("Rb87_F1", xy=(self.f1rb87, 0.8))
        for l, x in self.reference87D2_f1.items():
            plt.vlines(x + self.references["rb87f1"]["f0"], -1, 1, "y", label=l)
            plt.annotate(l, xy=(x + self.references["rb87f1"]["f0"], p))
            p += 0.1

        for l, x in self.limits85D2_f3.items():
            plt.vlines(x + self.references["rb85f3"]["f0"], -1, 1, "g", "dashed")

        for l, x in self.limits85D2_f2.items():
            plt.vlines(x + self.references["rb85f2"]["f0"], -1, 1, "b", "dashed")

        for l, x in self.limits87D2_f2.items():
            plt.vlines(x + self.references["rb87f2"]["f0"], -1, 1, "r", "dashed")

        for l, x in self.limits87D2_f1.items():
            plt.vlines(x + self.references["rb87f1"]["f0"], -1, 1, "y", "dashed")
        plt.show()

    def apply_shift_to_linepositions(self, params, shift):
        shifts = np.array([shift] * self.N_params)
        idx = np.array([0, 1, 0] * 6)
        shifts *= idx
        return params + shifts

    def setBounds(
            self, group, parameter, mhz_center=3.0, mhz_sigma=3.0, factor_amplitude=50.0
    ):
        # set +- 3Mhz bounds for center
        idx_center_lines = np.array([0, 1, 0] * 6, dtype=bool)
        self.bounds_min[group][idx_center_lines] = (
                parameter[idx_center_lines] - mhz_center / self.rb_models_scaling[group]
        )
        self.bounds_max[group][idx_center_lines] = (
                parameter[idx_center_lines] + mhz_center / self.rb_models_scaling[group]
        )

        # set +- 3 MHz bounds for sigma
        idx_sigma = np.array([0, 0, 1] * 6, dtype=bool)
        self.bounds_min[group][idx_sigma] = (
                parameter[idx_sigma] - mhz_sigma / self.rb_models_scaling[group]
        )
        self.bounds_max[group][idx_sigma] = (
                parameter[idx_sigma] + mhz_sigma / self.rb_models_scaling[group]
        )

        # set 0.8* - 1.2*amplitude bound
        idx_amplitude = np.array([1, 0, 0] * 6, dtype=bool)
        idx_negative = parameter < 0
        min_factor = 100.0 / (100.0 + factor_amplitude)
        max_factor = 100.0 / (100.0 - factor_amplitude)
        idx_negative_amplitude = np.logical_and(idx_negative, idx_amplitude)
        idx_positive_amplitude = np.logical_and(
            np.logical_not(idx_negative), idx_amplitude
        )
        self.bounds_min[group][idx_positive_amplitude] = (
                min_factor * parameter[idx_positive_amplitude]
        )
        self.bounds_max[group][idx_positive_amplitude] = (
                max_factor * parameter[idx_positive_amplitude]
        )

        self.bounds_min[group][idx_negative_amplitude] = (
                max_factor * parameter[idx_negative_amplitude]
        )
        self.bounds_max[group][idx_negative_amplitude] = (
                min_factor * parameter[idx_negative_amplitude]
        )
        return True

    @PyQt5.QtCore.pyqtSlot(np.ndarray, np.ndarray, str)
    def fitRb(self, x, y, timestamp=datetime.utcnow().isoformat()):
        if x is None:
            x = np.arange(len(y))
        z = np.zeros(len(x))
        # y_offset_subtracted = sigmaclip(y, 2.0, 2.0)[0]
        # y -= np.median(y_offset_subtracted)
        ymin = np.min(y)
        ymax = np.max(y)
        logger.debug("Fitting Rb...")
        if not self.valid_fit:
            self.parameterHist.clear()
            for g in self.available_groups:
                logger.debug(f"Initial model for group {g}")
                self.rb_models_scaling[g] = self.average_scaling * self.scaling(self.references[g]['linecenter'])
                self.initial_guesses[g] = self.initial_guesses_MHz[g] * self.rb_models_scaling[g]
                idx = np.array([0, 1, 0] * 6, dtype=bool)
                offset_reference_line = list(self.references[g]['reference_line'].values())[0]
                self.initial_guesses[g][idx] = self.initial_guesses[g][idx] + (
                        self.references[g]['linecenter'] - offset_reference_line *
                        self.rb_models_scaling[g])
                self.fit_range[g] = self.fit_range_MHz[g].copy()
                self.fit_range[g][0] = max(0, self.references[g]['linecenter'] - self.fit_range[g][0] *
                                           self.rb_models_scaling[g])
                self.fit_range[g][1] = min(len(y), self.references[g]['linecenter'] + self.fit_range[g][1] *
                                           self.rb_models_scaling[g])
                logger.debug(f"Set fitrange to: {self.fit_range[g][0]} - { self.fit_range[g][1]}")
                self.spline_knots[g] = self.spline_knots_MHz[g] * self.rb_models_scaling[g] + (
                        self.references[g]['linecenter'] - offset_reference_line *
                        self.rb_models_scaling[g])

            # self.find_correlation_max(x, y)
            for group in self.available_groups:
                self.parameterHist[group] = ParameterHistoryArray(
                    self.initial_guesses[group], N=25
                )

        res = []
        success_total = True

        fit_args = {}
        for group in self.available_groups:
            logger.debug(f"Fitting group {group}")
            fit_args[group] = {}
            minx = int(self.fit_range[group][0])
            maxx = int(self.fit_range[group][1])
            logger.debug(f"Fit range {minx} - {maxx}")
            offset_reference_line = list(self.references[group]['reference_line'].values())[0]
            xx = x[minx:maxx]
            yy = y[minx:maxx]
            # index to exclude hyperfine lines
            idx = np.logical_not(np.logical_and(
                self.spline_knots[group][np.argmax(self.spline_knots_MHz[group] - offset_reference_line > 0) - 1] < xx,
                xx < self.spline_knots[group][np.argmax(self.spline_knots_MHz[group] - offset_reference_line > 0)]))
            spl = LSQUnivariateSpline(xx[idx], yy[idx], t=self.spline_knots[group], k=3)
            yy -= spl(xx)

            fit_args[group]["x"] = xx
            fit_args[group]["y"] = yy
            fit_args[group]["minx"] = minx
            fit_args[group]["maxx"] = maxx
            p0 = self.parameterHist[group].getParameter()
            median_model = lorentzfunc(p0, xx)
            shift_x = np.arange(1 - len(xx), len(xx))
            correlation = np.correlate(median_model, yy, "full")
            shift_calculated = (
                    shift_x[correlation.argmax()] * 1.0 * (xx[-1] - xx[0]) / len(xx)
            )

            logger.debug(f'Rubidium shift: {shift_calculated}')
            shift_calculated = 0

            p0 = self.apply_shift_to_linepositions(p0, -shift_calculated)
            self.setBounds(group, p0, 7.0, 3.0, 50.0)
            self.bounds_min[group] = self.apply_shift_to_linepositions(
                self.bounds_min[group], -shift_calculated
            )
            self.bounds_max[group] = self.apply_shift_to_linepositions(
                self.bounds_max[group], -shift_calculated
            )
            fit_args[group]["p0"] = p0
            fit_args[group]["min"] = self.bounds_min[group]
            fit_args[group]["max"] = self.bounds_max[group]
            fit_args[group]['group'] = group

            if self.debug > 2:
                plt.figure()
                plt.plot(xx, yy + spl(xx), label='data')
                plt.plot(xx, spl(xx), label='spline')
                for k in self.spline_knots[group]:
                    plt.vlines(k, ymin, ymax)

                plt.plot(xx[~idx], np.mean((ymax + ymin) / 2) * np.ones_like(xx[~idx]), label='excluded for spline')
                plt.legend()

                plt.figure()
                plt.title(group)
                plt.plot(xx, yy)
                plt.plot(xx, lorentzfunc(p0, xx))
                plt.show()

        fit_results = parmap.map(do_rb_fitting, list(fit_args.values()), pool=self.pool)
        if False not in fit_results:
            for fr in fit_results:
                group = fr.group
                p1 = fr.x
                chi_sqr = sum(fr.fun ** 2)
                bound_active = np.sum(np.abs(fr.active_mask)) > 0
                # print 'Parameter within bounds?', not (bound_active)
                if bound_active:
                    print(
                        "Out of bounds:",
                        np.array(self.parameter_names[group])[
                            np.array(np.abs(fr.active_mask), dtype=bool)
                        ],
                    )
                z[fit_args[group]["minx"]: fit_args[group]["maxx"]] = fr.fun

                center_idx = np.array([0, 1, 0] * 6, dtype=bool)
                res.append((np.array(p1)[center_idx]).tolist())

                success = fr.success
                if self.debug > 2:
                    plt.figure()
                    fig, ax = plt.subplots(2, 1, 'all')
                    plt.title(group)
                    ax[0].plot(fit_args[group]['x'], fit_args[group]['y'])
                    ax[0].plot(fit_args[group]['x'], lorentzfunc(fit_args[group]["p0"], fit_args[group]['x']), 'y--')
                    ax[0].plot(fit_args[group]['x'], lorentzfunc(p1, fit_args[group]['x']))
                    # plot line centers
                    for lc in np.array(p1)[center_idx]:
                        ax[0].vlines(lc, np.min(fit_args[group]['y']), np.max(fit_args[group]['y']))
                    ax[1].plot(fit_args[group]['x'], lorentzfunc(p1, fit_args[group]['x']) - fit_args[group]['y'])
                    plt.show()

                if success:
                    self.parameterHist[group].addParameter(p1)
                    self.setBounds(group, self.parameterHist[group].getParameter())

                success_total = success_total and success

                if success_total:
                    result = dict(zip(self.parameter_names[group], p1))
                    logger.info(f"{group} , {result}")
                    self.sig_save_to_hdf.emit(timestamp, result, group, {})
                    redis_helper.write_to_influx(
                        timestamp, result, "rubidium", {"rb_group": group}
                    )

            if success_total:
                self.valid_fit = True
                res = list(itertools.chain.from_iterable(res))
                res_names = list(
                    itertools.chain.from_iterable(
                        [
                            (np.array([g + "__" + gr for gr in self.parameter_names[g]])[center_idx]
                            ).tolist()
                            for g in self.available_groups
                        ]
                    )
                )
                res_reference = list(
                    itertools.chain.from_iterable(
                        [self.ref_centers[g] for g in self.available_groups]
                    )
                )
                res_weights = list(
                    itertools.chain.from_iterable(
                        [self.ref_weights[g] for g in self.available_groups]
                    )
                )
                self.sig_data_plot.emit(
                    timestamp, {"data": {"x": x, "y": y}, "residuals": {"x": x, "y": z}}
                )
                return (
                    res,
                    res_names,
                    np.array(res_reference),
                    np.array(res_weights),
                    success_total,
                )
            self.sig_data_plot.emit(
                timestamp, {"data": {"x": x, "y": y}, "residuals": {"x": x, "y": z}}
            )
        else:
            self.sig_data_plot.emit(
                timestamp, {"data": {"x": x, "y": y}, "residuals": {"x": x, "y": z}}
            )
            if self.debug > 1:
                print("Fit not successfull")
            return res, res, np.array(res), res, False


if __name__ == "__main__":
    import qdarkstyle
    import sys
    from .QtStabilizeIt import DataViewer, parameter
    import cProfile

    app = PyQt5.QtGui.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet(pyside=False))

    dv = DataViewer.DataViewer("ni.h5", gui=False)
    dv.play_in_loop = False

    # dl = DataLogger.HDFLogger('data/out_test2.h5', 'w')
    rb = Rubidium(debug=4, ui=False, profile=True)

    # dv.sig_rb_raw.connect(rb.fitRb)

    import time

    # dv.on_tB_play_clicked()
    #    dv.openFile(read_completely=True)
    time1 = time.time()
    for i in range(7, 8):
        #     PyQt5.QtGui.QApplication.processEvents()
        tt, spec = dv.read_raw_data(i)
        y = spec["Rb"]
        rb.fitRb(np.arange(len(y)), y, tt.isoformat())
    time2 = time.time()
    print(time2 - time1)
    # dv.on_tB_play_clicked()

    # rb.gui.show()
    # dv.gui.show()

    # dv.current_index = 0
    # dv._send_next_spectrum()

    sys.exit(app.exec_())
