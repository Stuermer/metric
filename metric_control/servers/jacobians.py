__author__ = "stuermer"
import matplotlib

matplotlib.use("Qt4Agg")
import matplotlib.pyplot as plt
from lmfit.models import *
from lmfit.lineshapes import *


def moffat(x, amplitude=1.0, center=0.0, sigma=1.0, beta=1.0):
    return amplitude / (((x - center) / sigma) ** 2 + 1) ** beta


def lorentziannn(x, amplitude=1.0, center=0.0, sigma=1.0):
    """1 dimensional lorentzian
    lorentzian(x, amplitude, center, sigma)
    """
    return amplitude / (1.0 + ((1.0 * x - center) / sigma) ** 2)


def gaussiannn(x, amplitude=1.0, center=0.0, sigma=1.0):
    """1 dimensional gaussian:
    gaussian(x, amplitude, center, sigma)
    """
    return amplitude * exp(-((1.0 * x - center) ** 2) / (2 * sigma ** 2))


def guess_from_peakNN(model, y, x, negative, ampscale=1.0, sigscale=1.0):
    "estimate amp, cen, sigma for a peak, create params"
    if x is None:
        return 1.0, 0.0, 1.0
    maxy, miny = max(y), min(y)
    maxx, minx = max(x), min(x)
    imaxy = index_of(y, maxy)
    cen = x[imaxy]
    amp = maxy - miny
    sig = (maxx - minx) / 6.0

    halfmax_vals = np.where(y > (maxy + miny) / 2.0)[0]
    if negative:
        imaxy = index_of(y, miny)
        amp = -(maxy - miny) * 2.0
        halfmax_vals = np.where(y < (maxy + miny) / 2.0)[0]
    if len(halfmax_vals) > 2:
        sig = (x[halfmax_vals[-1]] - x[halfmax_vals[0]]) / 2.0
        cen = x[halfmax_vals].mean()
    amp = amp
    sig = sig * sigscale

    pars = model.make_params(amplitude=amp, center=cen, sigma=sig)
    pars["%ssigma" % model.prefix].set(min=0.0)
    return pars


# class MoffatModel(Model):
#     __doc__ = gaussian.__doc__ + COMMON_DOC if gaussian.__doc__ else ""
#     def __init__(self, *args, **kwargs):
#         super(MoffatModel, self).__init__(moffat, *args, **kwargs)
#
#     def guess(self, data, x=None, negative=False, **kwargs):
#         pars = guess_from_peak(self, data, x, negative, ampscale = 0.25)
#         return update_param_vals(pars, self.prefix, **kwargs)

def pvoigt_nn(x, amplitude=1.0, center=0.0, sigma=1.0, fraction=0.5):
    """Return a 1-dimensional pseudo-Voigt function.

    pvoigt(x, amplitude, center, sigma, fraction) =
       amplitude*(1-fraction)*gaussian(x, center, sigma_g) +
       amplitude*fraction*lorentzian(x, center, sigma)

    where sigma_g (the sigma for the Gaussian component) is

        sigma_g = sigma / sqrt(2*log(2)) ~= sigma / 1.17741

    so that the Gaussian and Lorentzian components have the
    same FWHM of 2*sigma.

    """
    sigma_g = sigma / sqrt(2 * log2)
    return ((1 - fraction) * gaussiannn(x, amplitude, center, sigma_g) +
            fraction * lorentziannn(x, amplitude, center, sigma))


def pvoigt_nn2(x, amplitude=1.0, center=0.0, sigma=1.0, sigma_g=1.0, fraction=0.5):
    """Return a 1-dimensional pseudo-Voigt function.

    pvoigt(x, amplitude, center, sigma, fraction) =
       amplitude*(1-fraction)*gaussian(x, center, sigma_g) +
       amplitude*fraction*lorentzian(x, center, sigma)

    where sigma_g (the sigma for the Gaussian component) is

        sigma_g = sigma / sqrt(2*log(2)) ~= sigma / 1.17741

    so that the Gaussian and Lorentzian components have the
    same FWHM of 2*sigma.

    """
    return ((1 - fraction) * gaussiannn(x, amplitude, center, sigma_g) +
            fraction * lorentziannn(x, amplitude, center, sigma))


class PseudoVoigtnn(Model):
    fwhm_factor = 2.0

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(pvoigt_nn, **kwargs)
        self._set_paramhints_prefix()

    def _set_paramhints_prefix(self):
        self.set_param_hint('sigma', min=0)
        self.set_param_hint('fraction', value=0.5, min=0.0, max=1.0)
        self.set_param_hint('fwhm', expr=fwhm_expr(self))
        fmt = ("(((1-{prefix:s}fraction)*{prefix:s}amplitude)/"
               "max({0}, ({prefix:s}sigma*sqrt(pi/log(2))))+"
               "({prefix:s}fraction*{prefix:s}amplitude)/"
               "max({0}, (pi*{prefix:s}sigma)))")
        self.set_param_hint('height', expr=fmt.format(tiny, prefix=self.prefix))

    def guess(self, data, x=None, negative=False, **kwargs):
        """Estimate initial model parameter values from data."""
        pars = guess_from_peakNN(self, data, x, negative, ampscale=1.25)
        pars['%sfraction' % self.prefix].set(value=0.5, min=0.0, max=1.0)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC


class PseudoVoigtnn2(Model):
    fwhm_factor = 2.0

    def __init__(self, independent_vars=['x'], prefix='', nan_policy='raise',
                 **kwargs):
        kwargs.update({'prefix': prefix, 'nan_policy': nan_policy,
                       'independent_vars': independent_vars})
        super().__init__(pvoigt_nn2, **kwargs)
        self._set_paramhints_prefix()

    def _set_paramhints_prefix(self):
        self.set_param_hint('sigma', min=0)
        self.set_param_hint('sigma_g', min=0)
        self.set_param_hint('fraction', value=0.5, min=0.0, max=1.0)
        self.set_param_hint('fwhm', expr=fwhm_expr(self))
        fmt = ("(((1-{prefix:s}fraction)*{prefix:s}amplitude)/"
               "max({0}, ({prefix:s}sigma*sqrt(pi/log(2))))+"
               "({prefix:s}fraction*{prefix:s}amplitude)/"
               "max({0}, (pi*{prefix:s}sigma)))")
        self.set_param_hint('height', expr=fmt.format(tiny, prefix=self.prefix))

    def guess(self, data, x=None, negative=False, **kwargs):
        """Estimate initial model parameter values from data."""
        pars = guess_from_peakNN(self, data, x, negative, ampscale=1.25)
        pars['%sfraction' % self.prefix].set(value=0.5, min=0.0, max=1.0)
        return update_param_vals(pars, self.prefix, **kwargs)

    __init__.__doc__ = COMMON_INIT_DOC
    guess.__doc__ = COMMON_GUESS_DOC


class LorentzianModelnn(Model):
    fwhm_factor = 2.0

    def __init__(self, *args, **kwargs):
        super(LorentzianModelnn, self).__init__(lorentziannn, *args, **kwargs)
        self.set_param_hint("sigma", min=0)
        self.set_param_hint("fwhm", expr=fwhm_expr(self))

    def guess(self, data, x=None, negative=False, **kwargs):
        pars = guess_from_peakNN(self, data, x, negative, ampscale=1.25)
        return update_param_vals(pars, self.prefix, **kwargs)


class GaussianModelnn(Model):
    __doc__ = gaussian.__doc__ + COMMON_DOC if gaussian.__doc__ else ""
    fwhm_factor = 2.354820

    def __init__(self, *args, **kwargs):
        super(GaussianModelnn, self).__init__(gaussiannn, *args, **kwargs)
        self.set_param_hint("sigma", min=0)
        self.set_param_hint("fwhm", expr=fwhm_expr(self))

    def guess(self, data, x=None, negative=False, **kwargs):
        pars = guess_from_peakNN(self, data, x, negative)
        return update_param_vals(pars, self.prefix, **kwargs)


def dfunc_moffat(params, *ys, **xs):
    xx = xs["x"]
    var = params.valuesdict()
    A = var["amplitude"]
    da = (1.0 / (1.0 + (xx - var["center"]) ** 2 / var["sigma"] ** 2)) ** var["beta"]
    dmu = (
        2.0
        * A
        * var["beta"]
        * (xx - var["center"])
        * (1.0 / (1.0 + (xx - var["center"]) ** 2 / var["sigma"] ** 2))
        ** (1.0 + var["beta"])
    ) / var["sigma"] ** 2
    db = (
        A
        * (1.0 / (1.0 + (xx - var["center"]) ** 2 / var["sigma"] ** 2)) ** var["beta"]
        * np.log(1.0 / (1 + (xx - var["center"]) ** 2 / var["sigma"] ** 2))
    )
    ds = (
        2.0
        * A
        * var["beta"]
        * (xx - var["center"]) ** 2
        * (1 / (1 + (xx - var["center"]) ** 2 / var["sigma"] ** 2))
        ** (1.0 + var["beta"])
    ) / var["sigma"] ** 3
    return np.array([db, ds, dmu, da])


def dfunc_gaussian(params, *ys, **xs):
    xx = xs["x"]
    var = params.valuesdict()
    fac = np.exp(-((xx - var["center"]) ** 2) / (2.0 * var["sigma"] ** 2))
    da = fac / (s2pi * var["sigma"])
    ds = (
        var["amplitude"]
        * fac
        * ((xx - var["center"]) ** 2 - var["sigma"] ** 2)
        / (s2pi * var["sigma"] ** 4)
    )
    dmu = var["amplitude"] * fac * (xx - var["center"]) / (s2pi * var["sigma"] ** 3)
    return np.array([ds, dmu, da])


def dfunc_gaussiannn(params, *ys, **xs):
    xx = xs["x"]
    var = params.valuesdict()
    fac = np.exp(-((xx - var["center"]) ** 2) / (2.0 * var["sigma"] ** 2))
    da = fac
    # (A*(x - \[Mu])**2)/(E**((x - \[Mu])**2/(2.*\[Sigma]**2))*\[Sigma]**3)
    ds = var["amplitude"] * (xx - var["center"]) ** 2 * fac / var["sigma"] ** 3
    dmu = var["amplitude"] * fac * (xx - var["center"]) / var["sigma"] ** 2
    return np.array([ds, dmu, da])


def dfunc_lorentzian(params, *ys, **xs):
    xx = xs["x"]
    var = params.valuesdict()
    fac = (xx - var["center"]) ** 2 + var["sigma"] ** 2
    ds = (var["amplitude"] * ((xx - var["center"]) ** 2 - var["sigma"] ** 2)) / (
        pi * fac ** 2
    )
    da = var["sigma"] / (pi * fac)
    dmu = (2.0 * var["amplitude"] * (xx - var["center"]) * var["sigma"]) / (
        pi * fac ** 2
    )
    return np.array([ds, dmu, da])


def dfunc_lorentziannn(params, *ys, **xs):
    xx = xs["x"]
    var = params.valuesdict()
    fac = (xx - var["center"]) ** 2 + var["sigma"] ** 2
    ds = (
        -2.0
        * var["amplitude"]
        * var["sigma"]
        / ((xx - var["center"]) ** 2 + var["sigma"] ** 2) ** 2
    )
    da = 1.0 / fac
    dmu = (
        2.0
        * var["amplitude"]
        * (xx - var["center"])
        / ((xx - var["center"]) ** 2 + var["sigma"] ** 2) ** 2
    )
    return np.array([ds, dmu, da])


def dfunc_voigt(params, *ys, **xs):
    xx = xs["x"]
    var = params.valuesdict()
    z = (xx - var["center"] + 1j * var["gamma"]) / (var["sigma"] * s2)

    da = wofz(z).real / (s2pi * var["sigma"])
    ds = (
        1.0
        / (s2pi * var["sigma"] ** 2)
        * var["amplitude"]
        * (
            np.sqrt(2.0 / pi)
            * var["sigma"]
            * ((xx - var["center"] + 1j * var["gamma"]) / var["sigma"] ** 2).imag
            + var["sigma"]
            * (
                wofz(z)
                * (xx - var["center"] + 1j * var["gamma"]) ** 2
                / var["sigma"] ** 3
            ).real
            - wofz(z).real
        )
    )
    dg = 0.0
    dmu = var["amplitude"] / (s2pi * var["sigma"]) * (
        wofz(z) * (xx - var["center"] + 1j * var["gamma"]) / var["sigma"] ** 2
    ).real + var["amplitude"] * (1.0 / var["sigma"]).imag / (pi * var["sigma"])

    return np.array([ds, da, dmu])


def dfunc_pseudovoigt(params, *ys, **xs):
    xx = xs["x"]
    var = params.valuesdict()
    A = var["amplitude"]
    a = var["fraction"]
    s = var["sigma"]
    mu = var["center"]
    dA = -(
        (-1 + a) / (np.exp((mu - xx) ** 2 / (2.0 * s ** 2)) * np.sqrt(2 * pi) * s)
    ) + (a * s) / (pi * (s ** 2 + (mu - xx) ** 2))
    # dA = (a*s)/(pi*((xx - mu)**2 + s**2)) - ((-1. + a)*np.sqrt(log2/pi))/(2**((xx - mu)**2/s**2)*s)
    ds = (
        ((-1 + a) * A)
        / (np.exp((mu - xx) ** 2 / (2.0 * s ** 2)) * np.sqrt(2 * pi) * s ** 2)
        - (2 * a * A * s ** 2) / (pi * (s ** 2 + (mu - xx) ** 2) ** 2)
        + (a * A) / (pi * (s ** 2 + (mu - xx) ** 2))
        - ((-1 + a) * A * (mu - xx) ** 2)
        / (np.exp((mu - xx) ** 2 / (2.0 * s ** 2)) * np.sqrt(2 * pi) * s ** 4)
    )

    dmu = ((-1 + a) * A * (mu - xx)) / (
        np.exp((mu - xx) ** 2 / (2.0 * s ** 2)) * np.sqrt(2 * pi) * s ** 3
    ) + (2 * a * A * s * (-mu + xx)) / (pi * (s ** 2 + (mu - xx) ** 2) ** 2)
    da = A * (
        -(1 / (np.exp((mu - xx) ** 2 / (2.0 * s ** 2)) * np.sqrt(2 * pi) * s))
        + s / (pi * (s ** 2 + (mu - xx) ** 2))
    )

    return np.array([dA, ds, dmu, da])


if __name__ == "__main__":
    # print '**********************************'
    # print '***** Test Moffat ***************'
    # print '**********************************'
    xs = np.linspace(-4, 4, 100)
    ys = moffat(xs, 2.5, 0, 0.5, 1.0)
    yn = ys + 0.1 * np.random.normal(size=len(xs))

    mod = MoffatModel()
    pars = mod.guess(yn, xs)
    out = mod.fit(yn, pars, x=xs)

    out2 = mod.fit(
        yn, pars, x=xs, fit_kws={"Dfun": dfunc_moffat, "col_deriv": 1, "xtol": 1e-6}
    )
    # out2  = mod.fit(yn, pars, x=xs )
    # print 'lmfit without dfunc **************'
    # print 'number of function calls: ', out.nfev
    # print 'params', out.best_values
    # print 'lmfit with dfunc *****************'
    # print 'number of function calls: ', out2.nfev
    # print 'params', out2.best_values
    # print '\n \n'
    out.plot(datafmt=".")

    # print '**********************************'
    # print '***** Test Gaussian **************'
    # print '**********************************'
    xs = np.linspace(-4, 4, 100)
    ys = gaussian(xs, 2.5, 0, 0.5)
    yn = ys + 0.1 * np.random.normal(size=len(xs))

    mod = GaussianModel()
    pars = mod.guess(yn, xs)
    out = mod.fit(yn, pars, x=xs)
    out2 = mod.fit(yn, pars, x=xs, fit_kws={"Dfun": dfunc_gaussian, "col_deriv": 1})
    # print 'lmfit without dfunc **************'
    # print 'number of function calls: ', out.nfev
    # print 'params', out.best_values
    # print 'lmfit with dfunc *****************'
    # print 'number of function calls: ', out2.nfev
    # print 'params', out2.best_values
    # print '\n \n'
    out2.plot(datafmt=".")

    # print '**********************************'
    # print '***** Test Lorentzian ************'
    # print '**********************************'
    xs = np.linspace(-4, 4, 100)
    ys = lorentzian(xs, 2.5, 0, 0.5)
    yn = ys + 0.1 * np.random.normal(size=len(xs))

    mod = LorentzianModel()
    pars = mod.guess(yn, xs)
    out = mod.fit(yn, pars, x=xs)
    out2 = mod.fit(yn, pars, x=xs, fit_kws={"Dfun": dfunc_lorentzian, "col_deriv": 1})
    # print 'lmfit without dfunc **************'
    # print 'number of function calls: ', out.nfev
    # print 'params', out.best_values
    # print 'lmfit with dfunc *****************'
    # print 'number of function calls: ', out2.nfev
    # print 'params', out2.best_values
    # print '\n \n'
    out2.plot(datafmt=".")

    # print '**********************************'
    # print '***** Test Voigt *****************'
    # print '**********************************'
    xs = np.linspace(-4, 4, 100)
    ys = voigt(xs, 2.5, 0, 0.5)
    yn = ys + 0.1 * np.random.normal(size=len(xs))

    mod = VoigtModel()
    pars = mod.guess(yn, xs)
    out = mod.fit(yn, pars, x=xs)
    out2 = mod.fit(yn, pars, x=xs, fit_kws={"Dfun": dfunc_voigt, "col_deriv": 1})
    # print 'lmfit without dfunc **************'
    # print 'number of function calls: ', out.nfev
    # print 'params', out.best_values
    # print 'lmfit with dfunc *****************'
    # print 'number of function calls: ', out2.nfev
    # print 'params', out2.best_values
    # print '\n \n'
    out2.plot(datafmt=".")

    # print '**********************************'
    # print '***** Test PseudoVoigt ***********'
    # print '**********************************'
    xs = np.linspace(-4, 4, 100)
    ys = pvoigt(xs, 2.5, 0, 0.5, 0.3)
    yn = ys + 0.1 * np.random.normal(size=len(xs))

    mod = PseudoVoigtModel()
    pars = mod.guess(yn, xs)
    out = mod.fit(yn, pars, x=xs)
    out2 = mod.fit(yn, pars, x=xs, fit_kws={"Dfun": dfunc_pseudovoigt, "col_deriv": 1})
    # print 'lmfit without dfunc **************'
    # print 'number of function calls: ', out.nfev
    # print 'params', out.best_values
    # print 'lmfit with dfunc *****************'
    # print 'number of function calls: ', out2.nfev
    # print 'params', out2.best_values
    out2.plot(datafmt=".")

    plt.show()
