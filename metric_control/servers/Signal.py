__author__ = 'julian'

import PyQt5
import numpy as np
import pyqtgraph
from PyQt5 import uic


class SignalGui(PyQt5.QtGui.QWidget):
    def __init__(self, name='CFP', color='b', remote=False, plot_residuals=True, scatter=False):
        super(SignalGui, self).__init__()
        uic.loadUi('metric_control/servers/SignalGUI.ui', self)

        self.plot_residuals = plot_residuals

        # self.setupPlots()
        # set up plots
        self.raw_plot = self.plotCFP.addPlot(name=name, row=0, col=0)
        self.residuals_plot = self.plotCFP.addPlot(name='residuals', row=1, col=0)
        self.residuals_plot.setYRange(-0.025, 0.025)
        self.plotCFP.ci.layout.setRowMaximumHeight(1, 200)
        self.residuals_plot.setXLink(name)
        if scatter:
            self.curve_raw = self.raw_plot.plot(pen=None, symbol='o')
            self.curve_residuals = self.residuals_plot.plot(pen=None, symbol='o')
        else:
            self.curve_raw = self.raw_plot.plot(pen=color)
            self.curve_residuals = self.residuals_plot.plot(pen=color)

        # list of time series plots
        self.tsplots = []
        self.tsdata = []

    @PyQt5.QtCore.pyqtSlot(str, dict)
    def plot(self, timestamp, data):
        if self.tabWidget.currentIndex() == 0:
            self.curve_raw.setData(data['data']['x'], data['data']['y'])
            if self.plot_residuals:
                self.curve_residuals.setData(data['residuals']['x'], data['residuals']['y'])

    @PyQt5.QtCore.pyqtSlot(dict)
    def showTimeSeries(self, data):
        dataset = str(self.cB_cfp_param.currentText())
        if self.tabWidget.currentIndex() == 2:
            n = len(data)
            for i in range(n):
                d = np.array(list(data[i][dataset]))
                if dataset == 'center':
                    self.tsdata[i].setData(d - data[i][dataset][0])
                else:
                    self.tsdata[i].setData(d)

    @PyQt5.QtCore.pyqtSlot(dict)
    def setupTimeSeriesPplot(self, data):
        """
        Adds a plot for each fittet Peak to show a time series of all available parameters

        """
        n = len(data)
        if isinstance(data, dict):
            for d in data.itervalues():
                for key, value in d[0].iteritems():
                    if not key == 'timestamp':
                        self.cB_cfp_param.addItem(key)
        else:
            for key, value in data[0].iteritems():
                if not key == 'timestamp':
                    self.cB_cfp_param.addItem(key)

        self.tsplots.append(self.plot_timeseries.addPlot(row=0, col=0))
        self.tsplots[0].setDownsampling(mode='peak')

        for i in range(n):
            # self.wlsplots.append(self.plotWLS.addPlot(row=i, col=0))
            self.tsdata.append(self.tsplots[0].plot(pen=pyqtgraph.mkPen(color=pyqtgraph.hsvColor(float(i) / n))))

    @PyQt5.QtCore.pyqtSlot(dict)
    def showStatistics(self, stats):
        # setup gui
        for key, value in stats.iteritems():
            for col, value2 in enumerate(value.iteritems()):
                newitem = PyQt5.QtGui.QTableWidgetItem(str(value2[1][-1]))
                self.tableWidget.setItem(key, col, newitem)


# class Signal(PyQt5.QtCore.QObject):
#     # signals
#     sig_data_plot = PyQt5.QtCore.pyqtSignal(datetime, dict)
#
#     # signals
#     sig_init_current_result = PyQt5.QtCore.pyqtSignal(str, dict)
#     sig_save_current_result = PyQt5.QtCore.pyqtSignal(str, dict)
#
#     sig_initial_parameter_list = PyQt5.QtCore.pyqtSignal(dict)
#     sig_fit_parameter_list = PyQt5.QtCore.pyqtSignal(dict)
#
#     def __init__(self, fitrange = 10, debug=0, profile=False, ui=False, cutoff_n_peaks_start=2, cutoff_n_peaks_end=2):
if __name__ == '__main__':
    pass
