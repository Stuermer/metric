"""
.. module:: intensity
   :platform: Unix
   :synopsis: A module for analysing intensity data.

.. moduleauthor:: Julian Stuermer <stuermer@uchicago.edu>


"""
from __future__ import print_function
from datetime import datetime

import numpy as np


def fit(x, y, timestamp=datetime.utcnow().isoformat()):
    res = {
        "timestamp": timestamp,
        "laser_intensity_mean": float(np.mean(y)),
        "laser_intensity_min": float(np.min(y)),
        "laser_intensity_max": float(np.max(y)),
        "laser_intensity_argmin": float(np.argmin(y)),
        "laser_intensity_argmax": float(np.argmax(y)),
    }
    return res


class Intensity(object):
    def __init__(self):
        self.x = None
        self.valid_fit = False


if __name__ == "__main__":
    import redis
    import cPickle
    import matplotlib.pyplot as plt

    r = redis.StrictRedis()

    read_dict = r.lrange("spectrum_snapshot", -1, -1)
    spectrum = cPickle.loads(read_dict[0])

    intensity = spectrum["Intensity"][100:]
    model = np.poly1d(np.polyfit(np.arange(len(intensity)), intensity, 3))
    model_data = model(np.arange(len(intensity)))

    total_dev = sum(abs(model_data - intensity))
    print(total_dev)

    plt.figure()
    plt.plot(intensity - model_data)
    plt.show()
