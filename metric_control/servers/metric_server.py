"""
.. module:: mccdaq_server
  :synopsis: Module to control the MCCDAQ USB 1808-X. This device is used to generate a smoothed sawtooth ramp for the
  lasersweep

.. moduleauthor:: Julian Stürmer <julian@stuermer.science>
"""

import datetime
import logging.handlers
import pickle
import threading
import time
from collections import namedtuple
from pathlib import Path

import numpy as np
import redis
from autologging import logged, traced

from metric_control import server
from metric_control.generated.common_pb2 import DeviceStatus
from metric_control.generated.metric_pb2 import DESCRIPTOR, state, InputPositions, OutputPositions
from metric_control.generated.metric_pb2_grpc import METRICServicer
from metric_control.clients import input_switcher_client, output_switcher_client, mccdaq_client

log_file_path = str(Path(__file__).resolve().parents[1]) + "/etc/loggers.ini"
logger = logging.getLogger("metricserver")


@logged(logger)
@traced("callback", exclude=True)
class METRIC(METRICServicer, server.ServerBase):
    """
        METRIC server
    """

    def __init__(self, config, simulation=False, section=None):
        super().__init__(simulation=simulation, section=section)

        self.config = config
        self.lock = threading.Lock()

        self.iswitcher = None
        self.oswitcher = None
        self.mccdaq = None

        self.input = 0
        self.output_1 = 0
        self.output_2 = 0
        self.time_output_1 = 0
        self.time_output_2 = 0

        self.timeout = 0
        self.timeleft = 0.
        self.start_time = None

        self.redis_server = None
        self.redis_base_string = str(DESCRIPTOR.package)
        self.logged_values = [f.name for f in state.DESCRIPTOR.fields]

        self.aborted = False
        self.blinking = False
        self.exposing = None
        self.status = DeviceStatus.Value("READY")

        self.connected = self.connect_clients()

        if not self.connected:
            raise RuntimeError(
                "Could not connect all clients. Please check log. Make sure are RPC servers are running."
            )
        else:
            print('ready')

    def initialize(self, request, context):
        return state(**self.make_log_dict_local())

    def connect_clients(self):
        self.iswitcher = input_switcher_client.InputSwitcherClient(self.config)
        self.oswitcher = output_switcher_client.OutputSwitcherClient(self.config)
        self.mccdaq = mccdaq_client.MCCDAQClient(self.config)

        self.iswitcher.initialize()
        self.oswitcher.initialize()
        self.mccdaq.initialize()

        return True

    def _abort_if_exposing(self):
        start = time.time()
        if self.blinking:
            self.aborted = True
            # abort after 5 sec
            while self.aborted and time.time()-start < 5:
                time.sleep(0.1)
                print(".", end=" ")
            print(".")
        else:
            self.aborted = True
            while self.aborted and time.time() - start < 5:
                time.sleep(0.1)
                print(".", end=" ")
            print(".")

        return self.exposing

    def abort(self, request, context):
        self._abort_if_exposing()
        if not self.blinking:
            self._to_default_positions()
        else:
            self.status = DeviceStatus.Value("ERROR")
            self.write_to_databases()
            raise RuntimeError("Could not stop blinking...")
        self.write_to_databases()
        return state(**self.make_log_dict_local())

    def _to_default_positions(self):
        output = self.oswitcher.set_position(OutputPositions.Value("PD"))
        self.output_1 = output.port
        self.output_2 = 0
        assert output.port == OutputPositions.Value("PD"), "Could not switch to Photodiode"
        input = self.iswitcher.set_position(InputPositions.Value("ETALON"))
        self.input = input.port
        assert input.port == (InputPositions.Value("ETALON")), "Could not switch to Etalon"
        # open laser shutter - DISABLED FOR NOW
        self.mccdaq.set_laser_shutter(1)
        self.exposing = False
        self.status = DeviceStatus.Value("READY")
        self.timeleft = 0.
        self.timeout = 0
        self.time_output_1 = 0
        self.time_output_2 = 0
        self.write_to_databases()

    def _blink(self,p1,p2,t1,t2):
        self.start_time = time.time()
        self.timeleft = max(0., self.timeout - (time.time() - self.start_time))
        self.blinking = True
        self.exposing = True
        self.aborted = False
        self.time_output_1 = t1
        self.time_output_2 = t2
        while (self.timeleft>0) and not self.aborted:
            logger.debug(f"Switch to {OutputPositions.Name(p1)}.")
            logger.debug(f"total time left: {self.timeleft} s")
            rep = self.oswitcher.set_position(p1)
            self.output_1 = rep.port

            self._wait_for_timeout_or_abort(t1/1000.)
            self.timeleft = max(0., self.timeout - (time.time() - self.start_time))
            self.write_to_databases()
            if not self.aborted:
                logger.debug(f"Switch to {OutputPositions.Name(p2)}")
                logger.debug(f"total time left: {self.timeleft} s")
                rep = self.oswitcher.set_position(p2)
                self.output_2 = rep.port
                self._wait_for_timeout_or_abort(t2/1000.)
            self.timeleft = max(0., self.timeout - (time.time() - self.start_time))
            self.write_to_databases()
        self.blinking = False
        self._to_default_positions()

    def _wait_for_timeout_or_abort(self, t, change_timeleft=False):
        start = time.time()
        while (time.time() - start < t) and not self.aborted:
            time.sleep(min(0.1, t))
            print(".", end=" ")
            if change_timeleft:
                self.timeleft = max(0., t - (time.time() - start))
        print(".")

    def close(self, request, context):
        self.iswitcher.close()
        self.oswitcher.close()
        self.mccdaq.close()

    def get_position(self, request, context):
        return state(**self.make_log_dict_local())

    def _set_position_and_wait(self, input, output):
        self.blinking = False
        self.aborted = False
        self.exposing = True

        rep = self.iswitcher.set_position(input)
        self.input = rep.port
        rep = self.oswitcher.set_position(output)
        self.output_1 = rep.port

        self.status = DeviceStatus.Value("BUSY")
        self.write_to_databases()
        self._wait_for_timeout_or_abort(self.timeout, change_timeleft=True)
        self.aborted = False
        self._to_default_positions()

    def set_position(self, request, context):
        input = request.input
        output_1 = request.output_1
        output_2 = request.output_2
        t_output1 = request.time_output_1
        t_output2 = request.time_output_2

        # close laser shutter
        ls = self.mccdaq.set_laser_shutter(0)
        if not ls.laser_shutter:

            # stop blinking / previous position
            self._abort_if_exposing()

            if request.timeout > 0:
                self.timeout = request.timeout
            else:
                self.timeout = 1800

            if not self.blinking:
                # no blinking:
                if t_output2 <= 0:
                    thread = threading.Thread(target=self._set_position_and_wait,args=(input, output_1))
                    thread.start()
                    time.sleep(0.2)
                # blinking
                else:
                    rep = self.iswitcher.set_position(input)
                    self.input = rep.port
                    thread = threading.Thread(target=self._blink,args=(output_1, output_2, t_output1, t_output2))
                    thread.start()
                    self.status = DeviceStatus.Value("BUSY")
                    self.write_to_databases()
                return state(**self.make_log_dict_local())
            else:
                self.status = DeviceStatus.Value("ERROR")
                self.write_to_databases()
                raise RuntimeError("Could not stop blinking...")
        else:
            self.status = DeviceStatus.Value("ERROR")
            self.write_to_databases()
            raise RuntimeError("Could not close lasershutter...")

    def cleanup(self):
        self.iswitcher.close()
        self.oswitcher.close()
        self.mccdaq.close()


if __name__ == "__main__":
    server.make_server(METRIC, "METRIC")
