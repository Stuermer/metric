import json

import redis

r = redis.StrictRedis()
p = r.pubsub()


def write_to_influx(timestamp, data, seriesname, tags={}):
    d = {
        "timestamp": str(timestamp),
        "influx_seriesname": str(seriesname),
        "influx_tags": tags,
        "data": data,
    }
    r.publish("influx_log", json.dumps(d))


def write_annotation_to_influx(timestamp, title, text, tags):
    data = {
        "timestamp": str(timestamp),
        "title": str(title),
        "text": str(text),
        "influx_tags": str(tags),
    }

    r.publish("influx_annotations", json.dumps(data))


if __name__ == "__main__":
    pass
    # write_annotation_to_influx(datetime.utcnow().isoformat(), 'testtile', 'blabla', 'I am a tag !')
