"""
.. module:: Fitter_server
  :synopsis: Module to control the Fitter.

.. moduleauthor:: Julian Stürmer <julian@stuermer.science>
"""

import logging.handlers
import pickle
import sys
import threading
from collections import deque
from pathlib import Path

import PyQt5
import numpy as np
from autologging import logged, traced
from pandas import Timestamp

from metric_control import server
from metric_control.generated.common_pb2 import DeviceStatus
from metric_control.generated.fitter_pb2 import DESCRIPTOR, state
from metric_control.generated.fitter_pb2_grpc import FitterServicer
from metric_control.servers import intensity, rubidium, etalon, Ramp, redis_helper

import matplotlib.pyplot as plt

log_file_path = str(Path(__file__).resolve().parents[1]) + "/etc/loggers.ini"
logger = logging.getLogger("server")


@logged(logger)
@traced("fitter", exclude=True)
class Fitter(FitterServicer, server.ServerBase):
    """
        Fitter server
    """

    def __init__(self, config, simulation=False, section=None):
        super().__init__(simulation=simulation, section=section)

        self.config = config
        self.lock = threading.Lock()
        self.device = None
        self.status = DeviceStatus.Value("READY")

        self.queue = deque(maxlen=4)
        self.average_N_spectra = 4

        self.redis_base_string = str(DESCRIPTOR.package)
        self.logged_values = [f.name for f in state.DESCRIPTOR.fields]
        self.is_fitting = True

        self.cfp = etalon.CFP(
            ui=True,
            debug=1,
            cutoff_n_peaks_start=1,
            cutoff_n_peaks_end=1,
            peak_threshold=0.3,
            d=0.4,
            fit_range=10,
            etalontype="confocal",
            model="Moffat",
        )

        self.etalon = etalon.Planar(
            ui=True,
            cutoff_n_peaks_end=0,
            cutoff_n_peaks_start=0,
            fit_range=10,
            prefix="AstroEtalon",
            model="Lorentziannn",
            etalontype="planar",
            d=0.005,
        )
        self.ramp = Ramp.Ramp(ui=True)
        self.rb = rubidium.Rubidium(debug=1, ui=True)

        self.N_rb_failed = 0
        self.N_cfp_failed = 0
        self.N_etalon_failed = 0
        self.N_until_rb_reset = 5
        self.N_until_etalon_reset = 5
        self.N_until_cfp_reset = 5

        self._wait_for_data()

        # thread = threading.Thread(target=self._wait_for_data, args=())
        # thread.start()

    def initialize(self, request, context):

        return state(**self.make_log_dict_local())

    def _wait_for_data(self):
        print("waiting")
        while self.is_fitting:
            spectrum_read = self.redis.brpop("spectra", timeout=1)
            if spectrum_read is not None:
                spectrum = pickle.loads(spectrum_read[1])
                self.queue.append(spectrum)
                print(len(self.queue))
                if len(self.queue) > self.average_N_spectra - 1:
                    cfp = np.zeros(len(spectrum["CFP"]))
                    rb = np.zeros(len(spectrum["Rb"]))
                    etalon = np.zeros(len(spectrum["Etalon"]))
                    intens = np.zeros(len(spectrum["Intensity"]))
                    times = []
                    for b in self.queue:
                        cfp += b["CFP"]
                        rb += b["Rb"]
                        etalon += b["Etalon"]
                        intens += b["Intensity"]
                        times.append(Timestamp(b["time"]))
                    cfp /= self.average_N_spectra
                    etalon /= self.average_N_spectra
                    rb /= self.average_N_spectra
                    intens /= self.average_N_spectra
                    # timestamp = times[0] + reduce((lambda x, y: y - x), times)
                    timestamp = times[0]
                    self.fitter(timestamp.isoformat(), cfp, rb, intens, -etalon)

    def fitter(self, timestamp, cfp, rb, intens, etalon):
        intensity_res = intensity.fit(x=None, y=intens, timestamp=timestamp)
        redis_helper.write_to_influx(timestamp, intensity_res, 'intensity', {})
        # plt.figure()
        # plt.plot(cfp, label="cfp")
        # plt.plot(rb, label="rb")
        # plt.plot(intens, label="intens")
        # plt.plot(etalon, label="etalon")
        # plt.legend()
        # plt.show()
        if self.is_fitting:
            logger.debug("Fitting CFP...")
            # t1 = time.time()
            e_centers, e_center_names, success = self.cfp.fit(
                x=None, y=cfp, timestamp=timestamp
            )
            diffe = np.ediff1d(e_centers)
            self.rb.average_scaling = np.median(diffe) / self.cfp.theoretical_FSR * 1e6
            normalized_diffe = diffe / np.median(diffe)
            self.rb.scaling = np.poly1d(np.polyfit(e_centers[:-1], normalized_diffe, 1))
            PyQt5.QtGui.QApplication.processEvents()
            if success:
                logger.debug("Fitting CFP successful. Now fitting Rb...")
                try:
                    (
                        rb_centers,
                        rb_center_names,
                        rb_references,
                        rb_weights,
                        success,
                    ) = self.rb.fitRb(x=None, y=rb, timestamp=timestamp)
                    PyQt5.QtGui.QApplication.processEvents()
                    # self.is_fitting = False
                except Exception as e:
                    print(e)
                    success = False
                if success:
                    logger.debug("Fitting Rb successful. Now fitting Ramp...")
                    rb_references = rb_references - np.min(rb_references)

                    solp, ramp_errors_rb, ramp_errors_cfp = self.ramp.simultaneous_fit(
                        timestamp,
                        rb_centers,
                        e_centers,
                        rb_references,
                        rb_center_names,
                        rb_weights,
                        e_center_names,
                        expected_cfp_FSR=self.cfp.theoretical_FSR,
                    )
                    if (abs(ramp_errors_rb["Rb-ramp-residuals"]) > 10.0).any() or (
                        abs(ramp_errors_rb["Rb-ramp-residuals"][rb_weights > 0.1]) > 2.0
                    ).any():
                        logger.warning("Rb fit seems wrong --------------------------------")
                        self.N_rb_failed += 1
                        logger.warning(f"Rb failed/until reset fits {self.N_rb_failed}/{self.N_until_rb_reset}.  ")
                        if self.N_rb_failed > self.N_until_rb_reset:
                            logger.warning(f"Rb fit will be reset.")
                            self.rb.valid_fit = False
                            redis_helper.write_annotation_to_influx(
                                timestamp,
                                "Fit error",
                                "new initial model for Rubidium",
                                "fit error",
                            )
                    else:
                        logger.debug("Rb fit seems correct... ")
                        self.N_rb_failed = 0

                    if (abs(ramp_errors_cfp["CFP-ramp-residuals"]) > 2.0).any():
                        logger.warning("CFP FIT SEEMS WRONG ----------------------------------------------------------")
                        self.N_cfp_failed += 1
                        logger.warning(f"CFP failed fits until reset: {self.N_cfp_failed} / {self.N_until_cfp_reset}")
                        if self.N_cfp_failed > self.N_until_cfp_reset:
                            self.cfp.valid_fit = False
                            logger.warning("CFP initial model will be reset")
                            redis_helper.write_annotation_to_influx(
                                timestamp,
                                "Fit error",
                                "new initial model for CFP",
                                "fit error",
                            )
                    else:
                        logger.debug("CFP fit seems correct")
                        self.N_cfp_failed = 0

                    redis_helper.write_to_influx(timestamp, solp, "rampfit", {})
                    # self.sig_save_to_hdf.emit(timestamp, solp, "rampfit", {})
                    x = self.ramp.us(np.arange(len(etalon)))
                    # print np.mean(rb_centers)
                    logger.debug("Fitting Astro Etalon...")
                    if (self.N_cfp_failed + self.N_rb_failed < 1) and np.max(etalon) > 0.5:

                        e, e_names, success = self.etalon.fit(
                            x=x, y=etalon, timestamp=timestamp
                        )
                        logger.info(f"Etalon position: {e}")
                        # self.rbpid.update(np.mean(rb_centers), timestamp)
                        if not success:
                            self.N_etalon_failed += 1
                            logger.warning(
                                f"Etalon failed fits until reset: {self.N_etalon_failed} / {self.N_until_etalon_reset}")
                            if self.N_etalon_failed > self.N_until_etalon_reset:
                                logger.warning("Etalon initial model will be reset...")
                                self.etalon.valid_fit = False
                                redis_helper.write_annotation_to_influx(
                                    timestamp,
                                    "Fit error",
                                    "new initial model for Etalon",
                                    "fit error",
                                )
                        else:
                            logger.info("All good. Next spectrum !")
                            self.N_etalon_failed = 0
                else:
                    self.N_rb_failed += 1
                    logger.warning(f"Rubidium failed fits until reset: {self.N_rb_failed} / {self.N_until_rb_reset}")
                    if self.N_rb_failed > self.N_until_rb_reset:
                        logger.warning(f"Rubidium initial model will be reset...")
                        self.rb.valid_fit = False
                        redis_helper.write_annotation_to_influx(
                            timestamp,
                            "Fit error",
                            "new initial model for Rubidium",
                            "fit error",
                        )
            else:
                # print 'CFP failed'
                self.N_cfp_failed += 1
                logger.warning(f"CFP failed fits until reset: {self.N_cfp_failed} / {self.N_until_cfp_reset}")
                if self.N_cfp_failed > self.N_until_cfp_reset:
                    logger.warning(f"CFP initial model will be reset")
                    self.cfp.valid_fit = False
                    redis_helper.write_annotation_to_influx(
                        timestamp, "Fit error", "new initial model for CFP", "fit error"
                    )
        else:
            pass

        self.queue.clear()

    def reload_settings(self, request, context):
        pass

    def start_stop_ramp(self, request, context):
        pass


if __name__ == "__main__":
    app = PyQt5.QtGui.QApplication(sys.argv)
    logger.setLevel(logging.DEBUG)
    server.make_server(Fitter, "Fitter")
    sys.exit(app.exec_())
