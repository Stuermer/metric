"""
.. module:: mccdaq_server
  :synopsis: Module to control the MCCDAQ USB 1808-X. This device is used to generate a smoothed sawtooth ramp for the
  lasersweep

.. moduleauthor:: Julian Stürmer <julian@stuermer.science>
"""

import datetime
import logging.handlers
import pickle
import threading
import time
from collections import namedtuple
from pathlib import Path

import numpy as np
import redis
from autologging import logged, traced

from metric_control import server
from metric_control.generated.common_pb2 import DeviceStatus
from metric_control.generated.mccdaq_pb2 import DESCRIPTOR, state
from metric_control.generated.mccdaq_pb2_grpc import MCCDAQServicer

log_file_path = str(Path(__file__).resolve().parents[1]) + "/etc/loggers.ini"
logger = logging.getLogger("server")

try:
    uldaq_loaded = True
    from uldaq import (
        get_daq_device_inventory,
        DaqDevice,
        Range,
        AInScanFlag,
        AOutScanFlag,
        DaqEventType,
        WaitType,
        ScanOption,
        InterfaceType,
        AiInputMode,
        create_float_buffer,
        ULException,
        EventCallbackArgs,
        DigitalPortType,
        DigitalPortIoType,
        DigitalDirection,
        TriggerType,
    )

except (ImportError, OSError):
    uldaq_loaded = False
    logger.warning(
        "uldaq modules have not been imported. This is OK, when using simulation mode."
    )


def generate_ramp(offset, amplitude, ramp_length, ramp_length_down):
    """
    Generates a smoothed sawtooth ramp. Smoothness can be adjusted by increasing the downramplength

    Args:
        offset (float): offset of ramp in Volts
        amplitude (float): PV amplitude of the ramp
        ramp_length (int): total number of sampling points
        ramp_length_down (int): number of sampling points used for the falling part of the ramp

    Returns:
        ndarray: smoothed sawtooth ramp

    """

    up_ramp = np.linspace(offset, offset + amplitude, ramp_length - ramp_length_down)

    # Create a second ramp to return to start of first ramp, e.g. a cosine shaped function, of length downramplength
    x = np.linspace(0, np.pi, ramp_length_down)
    down_ramp = ((np.cos(x) + 1.0) / 2.0) * amplitude + offset

    # Create final ramp, consisting of up- and down-ramp parts
    ramp = np.append(up_ramp, down_ramp)
    return ramp


@logged(logger)
@traced("callback", exclude=True)
class MCCDAQ(MCCDAQServicer, server.ServerBase):
    """
        MCCDAQ server
    """

    def __init__(self, config, simulation=False, section=None):
        super().__init__(simulation=simulation, section=section)
        if not uldaq_loaded and not simulation:
            raise ImportError(
                "uldaq module is not loaded and simulation mode is not activated. Please check installation of uldaq "
                "python modules and libraries."
            )

        self.config = config
        self.lock = threading.Lock()
        self.device_1 = None  # output pzt ramp, FF   input all signals
        self.device_2 = None  # output for VOA
        self.ai_device_1 = None
        self.ao_device_1 = None
        self.dio_device_1 = None
        self.ao_info_1 = None
        self.ao_device_2 = None
        self.ao_info_2 = None

        self.ramp_pzt = None
        self.ramp_voa = None
        self.ramp_ff = None

        self.out_data_device_1 = None
        self.out_ff_data = None
        self.out_voa_data = None

        self.ramp_offset = None
        self.ramp_amplitude = None
        self.ramp_length = None
        self.ramp_length_down = None
        self.scan_rate = None

        self.ramp_running = False

        self.out_rate_1 = None
        self.out_rate_2 = None
        self.in_rate = None
        self.data = None
        self.requested_to_run = True
        self.status = DeviceStatus.Value("READY")
        self.ff_scale = None
        self.ff_offset = None
        self.voa_scale = None
        self.devices = None
        self.laser_shutter = 1
        self.ldls_shutter = 1

        self.pd_mean = -10.

        self.redis_server = None
        self.redis_base_string = str(DESCRIPTOR.package)
        self.logged_values = [f.name for f in state.DESCRIPTOR.fields]

        self._read_config_and_set_ramps()

        self.connected = True if self.simulation else self.config_hardware()

        if not self.connected:
            raise RuntimeError(
                "There was an error configuring the hardware. Please check log."
            )
        else:
            print('ready')
            # self._start_scan()
            # time.sleep(5)
            # self._stop_scan()

    def initialize(self, request, context):
        return state(**self.make_log_dict_local())

    def reload_settings(self, request, context):
        self.cleanup()
        self.config.read_file_again()
        self._read_config_and_set_ramps()
        self.connected = True if self.simulation else self.config_hardware()
        return state(**self.make_log_dict_local())

    def _read_config_and_set_ramps(self):
        self.ramp_offset = self.config.get_float("MCCDAQ", "ramp_offset", 0.0)
        self.ramp_amplitude = self.config.get_float("MCCDAQ", "ramp_amplitude", 4.5)
        self.ramp_length = self.config.get_integer("MCCDAQ", "ramp_length", 4096)
        self.voa_scale = self.config.get_float("MCCDAQ", "voa_scale",0.)
        self.ff_scale = self.config.get_float("MCCDAQ", "ff_scale",0.)
        self.ff_offset = self.config.get_float("MCCDAQ", "ff_offset", 0.0)
        self.ramp_length_down = self.config.get_integer(
            "MCCDAQ", "ramp_length_down", 400
        )
        self.scan_rate = self.config.get_integer("MCCDAQ", "scan_rate", 40000)

        self.redis_server = redis.StrictRedis(
            self.config.get_string("RedisServer", "host", "localhost")
        )

        self.ramp_pzt = generate_ramp(
            self.ramp_offset,
            self.ramp_amplitude,
            self.ramp_length,
            self.ramp_length_down,
        )

        # self.ramp_voa = generate_ramp(
        #     self.ramp_offset / self.voa_scale,
        #     self.ramp_amplitude / self.voa_scale,
        #
        # )
        self.ramp_voa = generate_ramp(0., 0., self.ramp_length, self.ramp_length_down)
        print(self.ramp_voa, np.min(self.ramp_voa), np.max(self.ramp_voa))

        self.ramp_ff = generate_ramp(
            self.ff_offset,
            - self.ramp_amplitude / self.ff_scale,
            self.ramp_length,
            self.ramp_length_down,
        )

        logger.debug(
            f"Configure ramp with {self.ramp_offset} V offset, {self.ramp_amplitude} V amplitude, "
            f"{self.ramp_length} sample points, with {self.ramp_length_down} points for the falling part of the ramp.\n"
            f"The scan rate is {self.scan_rate} Hz."
        )

    def set_laser_shutter(self, request, context):
        if not self.simulation:
            self.dio_device_2.d_bit_out(DigitalPortType.AUXPORT, 0, not(request.on))

        time.sleep(0.5)
        real_state = abs(self.pd_mean) > 0.01
        # requested open, and it's open!
        if self.simulation:
            self.laser_shutter = request.on
        else:
            success = (request.on and real_state) or (not(request.on) and not(real_state))
            if success:
                self.laser_shutter = request.on
            else:
                self.laser_shutter = not(request.on)

        self.write_to_databases()
        return state(**self.make_log_dict_local())

    def set_ldls_shutter(self, request, context):
        self.dio_device_2.d_bit_out(DigitalPortType.AUXPORT, 1, not(request.on))
        self.ldls_shutter = not(request.on)
        self.write_to_databases()
        return state(**self.make_log_dict_local())

    def get_laser_shutter(self, request, context):
        return state(**self.make_log_dict_local())

    def get_ldls_shutter(self, request, context):
        return state(**self.make_log_dict_local())

    def config_hardware(self):
        if self.simulation:
            pass
        else:
            self.devices = get_daq_device_inventory(InterfaceType.USB)
            logger.debug(f"Devices found: {self.devices}")
            try:
                # TODO: replace hard coded id with entry in ini file
                if self.devices[0].unique_id == "01E4F604":
                    idx_dev1 = 0
                    idx_dev2 = 1
                else:
                    idx_dev1 = 1
                    idx_dev2 = 0
                self.device_1 = DaqDevice(self.devices[idx_dev1])
                logger.debug(f"Open device 1 {self.device_1}. {self.devices[idx_dev1].unique_id}")
                self.device_1.connect()

                self.ai_device_1 = self.device_1.get_ai_device()
                logger.debug(f"Got device {self.ai_device_1}")
                self.ao_device_1 = self.device_1.get_ao_device()
                logger.debug(f"Got device {self.ao_device_1}")
                self.dio_device_1 = self.device_1.get_dio_device()
                logger.debug(f"Got device {self.dio_device_1}")
                self.ao_info_1 = self.ao_device_1.get_info()
                logger.debug(f"Got device {self.ao_info_1}.")

                self.device_2 = DaqDevice(self.devices[idx_dev2])
                logger.debug(f"Open device 2 {self.device_2}. {self.devices[idx_dev2].unique_id}")

                self.device_2.connect()
                self.dio_device_2 = self.device_2.get_dio_device()
                self.ao_device_2 = self.device_2.get_ao_device()
                self.ao_info_2 = self.ao_device_2.get_info()

                # Allocate a buffer to receive the data.
                logger.debug("Create buffer...")
                self.data = create_float_buffer(4, self.ramp_length)
                ScanParams = namedtuple("ScanParams", "buffer high_chan low_chan")

                # Store the scan event parameters for use in the callback function.
                scan_event_parameters = ScanParams(self.data, 3, 0)
                logger.debug("enable events")
                # Enable the event to be notified every time 100 samples are available.
                available_sample_count = self.ramp_length
                event_types = (
                    DaqEventType.ON_DATA_AVAILABLE
                    | DaqEventType.ON_END_OF_INPUT_SCAN
                    | DaqEventType.ON_INPUT_SCAN_ERROR
                )

                self.device_1.enable_event(
                    event_types,
                    available_sample_count,
                    self.callback,
                    scan_event_parameters,
                )

                self.out_data_device_1 = create_float_buffer(2, self.ramp_length)
                self.out_voa_data = create_float_buffer(1, self.ramp_length)
                logger.debug("Fill buffer values")
                for i in range(self.ramp_length):
                    self.out_data_device_1[2 * i] = self.ramp_pzt[i]
                    self.out_data_device_1[2 * i + 1] = self.ramp_ff[i]
                    self.out_voa_data[i] = self.ramp_voa[i]
                logger.debug("Configure digital IO bits")
                self.dio_device_1.d_config_bit(
                    DigitalPortType.AUXPORT, 0, DigitalDirection.OUTPUT
                )
                self.dio_device_2.d_config_bit(
                    DigitalPortType.AUXPORT, 0, DigitalDirection.OUTPUT
                )
                self.dio_device_2.d_config_bit(
                    DigitalPortType.AUXPORT, 1, DigitalDirection.OUTPUT
                )

                self.dio_device_1.d_bit_out(DigitalPortType.AUXPORT, 0, 0)
                self.dio_device_2.d_bit_out(DigitalPortType.AUXPORT, 0, 1)  # laser shutter default: closed
                self.laser_shutter = 0
                self.dio_device_2.d_bit_out(DigitalPortType.AUXPORT, 1, 0)  # ldls shutter default: on
                self.ldls_shutter = 1
                return True

            except IndexError:
                logger.error("Couldn't find MCCDAQ device")
                self.status = DeviceStatus.Value("ERROR")
                self.cleanup()
                return False
            except ULException as e:
                logger.error(f"{e}")
                self.cleanup()
                return False
            except Exception as e:
                logger.error(f"{e}")
            finally:
                self.write_to_databases()

    def _start_scan(self):
        if self.simulation:
            self.ramp_running = True
        else:
            try:

                logger.debug("Set Triggers")
                self.ao_device_1.set_trigger(TriggerType.POS_EDGE, 0, 4, 0, 0)
                self.ai_device_1.set_trigger(TriggerType.POS_EDGE, 0, 4, 0, 0)
                self.ao_device_2.set_trigger(TriggerType.POS_EDGE, 0, 4, 0, 0)
                logger.debug("Configure output")
                self.out_rate_1 = self.ao_device_1.a_out_scan(
                    0,
                    1,
                    self.ao_info_1.get_ranges()[0],
                    self.ramp_length,
                    self.scan_rate,
                    ScanOption.EXTTRIGGER | ScanOption.CONTINUOUS,
                    AOutScanFlag.DEFAULT,
                    self.out_data_device_1,
                )
                logger.debug(f"Configured output 1 with a true rate of {self.out_rate_1}")

                self.out_rate_2 = self.ao_device_2.a_out_scan(
                    0,
                    0,
                    self.ao_info_2.get_ranges()[0],
                    self.ramp_length,
                    self.scan_rate,
                    ScanOption.EXTTRIGGER | ScanOption.CONTINUOUS | ScanOption.EXTCLOCK,
                    AOutScanFlag.DEFAULT,
                    self.out_voa_data,
                )
                logger.debug(f"Configured output 2 with a true rate of {self.out_rate_2}")

                self.in_rate = self.ai_device_1.a_in_scan(
                    0,
                    3,
                    AiInputMode.DIFFERENTIAL,
                    Range.BIP10VOLTS,
                    self.ramp_length,
                    self.scan_rate,
                    ScanOption.CONTINUOUS | ScanOption.EXTTRIGGER | ScanOption.EXTCLOCK,
                    AInScanFlag.DEFAULT,
                    self.data,
                )
                logger.debug(f"Configure Input with a rate of {self.in_rate}")
                logger.debug(f"Make Pos Edge")
                self.dio_device_1.d_bit_out(DigitalPortType.AUXPORT, 0, 0)
                self.dio_device_1.d_bit_out(DigitalPortType.AUXPORT, 0, 1)
            except KeyboardInterrupt:
                logger.warning(f"Scan stopped... Keyboard interrupted")
                self._stop_scan()

            except ULException as e:
                logger.error(f"Exception: {e}")
                self.cleanup()

    def _stop_scan(self):
        self.dio_device_1.d_bit_out(DigitalPortType.AUXPORT, 0, 0)

        if self.ai_device_1:
            self.ai_device_1.scan_stop()
        if self.ao_device_1:
            self.ao_device_1.scan_stop()
        if self.ao_device_2:
            self.ao_device_2.scan_stop()

        self.ramp_running = False

    def start_stop_ramp(self, request, context):
        if request.on and not self.ramp_running:
            self._start_scan()
            self.ramp_running = True
        if not request.on:
            self._stop_scan()
        return state(**self.make_log_dict_local())

    def callback(self, event_callback_args):
        # type: (EventCallbackArgs) -> None
        """
        The callback function called in response to an event condition.

        Args:
            event_callback_args: Named tuple :class:`EventCallbackArgs` used to pass
                parameters to the user defined event callback function :class`DaqEventCallback`.
                The named tuple contains the following members
                event_type - the condition that triggered the event
                event_data - additional data that specifies an event condition
                user_data - user specified data that will be passed to the callback function
        """
        try:
            self.ramp_running = True
            event_type = event_callback_args.event_type
            event_data = event_callback_args.event_data
            user_data = event_callback_args.user_data
            # logger.debug(f"event data: {event_data}   event type: {event_type}")
            # print(f"event data: {event_data}   event type: {event_type}")

            d = np.frombuffer(self.data, float)
            nn = 4
            d1 = d[::nn][:-self.ramp_length_down]
            d2 = d[1::nn][:-self.ramp_length_down]
            d3 = d[2::nn][:-self.ramp_length_down]
            d4 = d[3::nn][:-self.ramp_length_down]
            # d5 = d[4::nn][:-self.ramp_length_down]
            print(f"rb :    {np.min(d1):.2f},  {np.max(d1):.2f},  {np.mean(d1):.2f}")
            print(f"cfp:    {np.min(d2):.2f},  {np.max(d2):.2f},  {np.mean(d2):.2f}")
            print(f"etalon: {np.min(d3):.2f},  {np.max(d3):.2f},  {np.mean(d3):.2f}")
            print(f"inten:  {np.min(d4):.2f},  {np.max(d4):.2f},  {np.mean(d4):.2f}")
            # print(f"{np.min(d5):.2f},  {np.max(d5):.2f},  {np.mean(d5):.2f}")
            timestamp = datetime.datetime.utcnow()
            d_to_redis = {}
            d_to_redis.update({"time": timestamp.isoformat()})
            d_to_redis.update({"Rb": d1[:]})
            d_to_redis.update({"Intensity": d4[:]})
            d_to_redis.update({"CFP": d2[:]})
            d_to_redis.update({"Etalon": d3[:]})

            self.pd_mean = np.mean(d4)
            print(f"pd mean: {self.pd_mean}")

            self.redis_server.lpush("spectra", pickle.dumps(d_to_redis, protocol=0))
            self.redis_server.lpush(
                "spectrum_snapshot", pickle.dumps(d_to_redis, protocol=0)
            )
            self.redis_server.ltrim("spectra", 0, 5)
            self.redis_server.ltrim("spectrum_snapshot", 0, 1)

            if event_type == DaqEventType.ON_INPUT_SCAN_ERROR:
                exception = ULException(event_data)
                self.ramp_running = False
                logger.error(exception)
        except KeyboardInterrupt:
            self._stop_scan()

    def cleanup(self):
        self._stop_scan()
        if self.device_1.is_connected():
            self.device_1.disconnect()
        if self.device_2.is_connected():
            self.device_1.disconnect()

        self.device_1.release()
        self.device_2.release()


if __name__ == "__main__":
    server.make_server(MCCDAQ, "MCCDAQ")
