import logging
import threading
import time
from pathlib import Path

import serial
from autologging import logged, traced

from metric_control import server
from metric_control.generated.common_pb2 import DeviceStatus
from metric_control.generated.outputswitcher_pb2 import Positions, state, DESCRIPTOR
from metric_control.generated.outputswitcher_pb2_grpc import OutputSwitcherServicer

log_file_path = str(Path(__file__).resolve().parents[1]) + '/etc/loggers.ini'

logger = logging.getLogger('server')


@traced(logger)
@logged(logger)
class OutputSwitcher(OutputSwitcherServicer, server.ServerBase):
    """
        Module to control PiezoJena fiber switcher 1:4

        As of right now, the actual commands are send via a arduino.
        This is the fiber switcher that selects where the output of the etlon is routed: either to the spectrograph or
        to the laser measurement setup.
    """

    def __init__(self, config, simulation=False, section=None):
        super().__init__(simulation=simulation, section=section)
        self.status = DeviceStatus.Value("ERROR")
        self.lock = threading.Lock()

        self.serial_port = None
        self.serial_port_address = config.get_string("OutputSwitcher", "serial_port")
        self.serial_port_baudrate = config.get_string("OutputSwitcher", "serial_port_baudrate")
        self.port = None

        self.redis_base_string = str(DESCRIPTOR.package)
        self.logged_values = [f.name for f in state.DESCRIPTOR.fields]

    def initialize(self, request, context):
        if self.simulation:
            self.port = 1
            self.status = DeviceStatus.Value("READY")
            return self.OK
        else:
            if self.serial_port is None:
                try:
                    self.serial_port = serial.Serial(self.serial_port_address, self.serial_port_baudrate)
                    self.status = DeviceStatus.Value("READY")

                except serial.serialutil.SerialException as e:
                    self.status = DeviceStatus.Value("ERROR")
                    return self.error("Error connecting to " + str(self.serial_port))
            try:
                self.get_position(request, context)
            except Exception as e:
                logger.error(e)

            time.sleep(1.5)
            self.write_to_databases()
            return state(**self.make_log_dict_local())

    def set_position(self, request, context):
        port = request.port
        if self.simulation:
            if request.port in Positions.values():
                self.status = DeviceStatus.Value("READY")
                self.port = request.port
                return state(port=self.port, status=self.status)
            else:
                self.port = 0
                self.status = DeviceStatus.Value("ERROR")
                raise ValueError("Requested position not available")
        else:
            with self.lock:
                self.serial_port.write(f"ch{request.port}\r\n".encode("ascii"))

            self._get_position()
            if self.port == port:
                self.status = DeviceStatus.Value("READY")
                self.write_to_databases()
                return state(port=self.port, status=self.status)
            else:
                self.status = DeviceStatus.Value("ERROR")
                self.write_to_databases()
                raise RuntimeError(f"Could not switch to port f{port}")

    def _get_position(self):
        with self.lock:
            self.serial_port.write(b'ch?\r\n')
            answer = self.serial_port.readline()
            logger.debug("Answer: %s", answer)
        try:
            answerInt = int(answer)
            if 0 < answerInt < 5:
                self.port = answerInt
            else:
                raise ValueError(f"Reading invalid port number ({answer})")
        except Exception as e:
            logger.error(e)

    def get_position(self, request, context):
        self._get_position()
        return state(**self.make_log_dict_local())

    def close(self, reques, context):
        self.serial_port.close()


if __name__ == '__main__':
    server.make_server(OutputSwitcher, "OutputSwitcher")
