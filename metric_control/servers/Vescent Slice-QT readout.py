# *****************************************************************************#
#                        Code for the labjack T6                            #
# *****************************************************************************#

import sys
import time

import serial

# import labjack.ljm


# ----------------- SET VARIABLES ----------------------------------------------
loop_count = 0
pcm_time = 1.0

# Thorlabs TH10K coefficients
a = 3.3540170E-3
b = 2.5617244E-4
c = 2.1400943E-6
d = -7.2405219E-8

# ----------------- INFINTE LOOP -----------------------------------------------
i = 0

while True:
    try:

        f = open('test.log', 'a')
        loop_count = loop_count + 1

        time.sleep(1.0)

        # ----------------- PRINT VALUES ---------------------------------------

        if (loop_count == 5):
            with serial.Serial('/dev/ttyACM0', 9600, timeout=1) as s:
                # s.write(b"TempSet? 1\r")
                s.write(b"Temp? 1\r")
                # s.write(b"Temp? 2\r")
                chan_1 = s.readline().decode("ascii")
                channel_1 = float(chan_1)

                s.write(b"Temp? 2\r")
                chan_2 = s.readline().decode("ascii")
                channel_2 = float(chan_2)

                s.write(b"Temp? 3\r")
                chan_3 = s.readline().decode("ascii")
                channel_3 = float(chan_3)

                s.write(b"Temp? 4\r")
                chan_4 = s.readline().decode("ascii")
                channel_4 = float(chan_4)

            time_sec = time.time()
            lineOut = " %.4f %.4f %.4f %.4f %.2f" % (channel_1, channel_2, channel_3, channel_4, time_sec)

            f.write(lineOut + '\n')  # +' '+localtime+'\n')
            f.close()

            print(lineOut)
            loop_count = 0
            # P_old = P


    except KeyboardInterrupt:
        break
    except Exception:
        print(sys.exc_info()[1])
        break
