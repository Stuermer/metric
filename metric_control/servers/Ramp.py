from datetime import datetime

import PyQt5
import numpy as np
import parameter
from scipy.interpolate import LSQUnivariateSpline
from scipy.optimize import leastsq
from statsmodels.stats.diagnostic import acorr_ljungbox
from statsmodels.stats.stattools import jarque_bera

from metric_control.servers import Signal


class Ramp(PyQt5.QtCore.QObject):
    sig_data_plot = PyQt5.QtCore.pyqtSignal(str, dict)

    def __init__(self, ui=True):
        super(Ramp, self).__init__()
        self.n_rb = 0
        self.n_etalon = 0
        self.rb_weights = None
        self.us = None
        self.x = None
        self.valid_fit = False

        if ui:
            self.gui = Signal.SignalGui("Ramp", "y", scatter=True)
            self.sig_data_plot.connect(self.gui.plot)
            self.gui.show()

        self.residuals_rb = None
        self.residuals_etalon = None
        self.param_hist = parameter.ParameterHistory(parameter=None)

    def make_y(self, var, xs, ys):
        y = np.zeros(self.n_rb + self.n_etalon)  # create new y vector
        y[: self.n_rb] = ys[: self.n_rb]  # copy Rb frequencies
        y[self.n_rb :] = (
            var[0] + var[1] * ys[self.n_rb :]
        )  # populate y vector with CFP comb frequencies
        return y

    def f(self, var, xs):
        return var[2] + var[3] * xs

    def errorfunc(self, var, xs, ys):
        y = self.make_y(var, xs, ys)
        x_sorted = np.sort(xs[self.n_rb :])
        ind_sorted = np.argsort(xs[self.n_rb :])
        y_sorted = y[self.n_rb :][ind_sorted]
        # self.us = UnivariateSpline(x_sorted, y_sorted, s=5, k=4)
        n = 15
        self.us = LSQUnivariateSpline(
            x_sorted,
            y_sorted,
            np.linspace(x_sorted[0], x_sorted[-1], n)[1 : n - 1],
            k=3,
        )
        # print self.us.get_residual(), np.sum(y[:n_rb] - self.us(x[:n_rb]))
        return (y[: self.n_rb] - self.us(self.x[: self.n_rb])) * self.rb_weights
        # return f(var, xs) - y

    def errorfuncLinear(self, var, xs, ys):
        y = self.make_y(var, xs, ys)
        return self.f(var, xs) - y

    def simultaneous_fit(
        self,
        timestamp,
        rb_centers,
        etalon_centers,
        rb_references,
        rb_names,
        rb_weights,
        etalon_names,
        expected_cfp_FSR=187.0,
    ):
        """
            Simultaneous fit of zeropoint and FSR of CFP and polynomial fit of piezo axis.
            5th order polynomial is fitted to piezo axis

            :param rb_centers:  fitted positions of Rubidium lines (in sampling units)
            :type rb_centers: ndarray
            :param etalon_centers: fitted positions of confocal etalon lines (in sampling units)
            :type etalon_centers: ndarray
            :param rb_references: known frequency of Rubidium reference lines (in Mhz and arbitrary zeropoint subtracted)
            :type rb_references: ndarray
            :param expected_cfp_FSR: expacted FSR of confocal etalon in MHz
            :type expected_cfp_FSR: float
            :return:
            :rtype:
        """
        if not self.valid_fit:
            self.param_hist.addMeta(
                {"offset": -(etalon_centers[0] + np.min(rb_centers))}
            )
            self.param_hist.addMeta({"FSR": expected_cfp_FSR})
            self.valid_fit = True

        P0 = [self.param_hist.getMeta("offset")[2], self.param_hist.getMeta("FSR")[2]]

        self.n_rb = len(rb_centers)
        self.n_etalon = len(etalon_centers)

        self.rb_weights = rb_weights

        x = np.hstack((rb_centers, np.sort(etalon_centers)))
        self.x = x
        y = np.hstack((rb_references, np.arange(self.n_etalon)))

        solp, ier = leastsq(
            self.errorfunc,
            P0,
            args=(x, y),
            full_output=False,
            maxfev=2000,
            epsfcn=1e-10,
            factor=1,
        )

        y_plot = self.make_y(solp, x, y)
        # plt.figure()
        # plt.scatter(x, y_plot)
        # plt.plot(np.sort(x), self.us(np.sort(x)))
        # plt.plot(self.us.get_knots(), self.us(self.us.get_knots()), 'ro')
        # plt.figure()
        # # plt.plot(x[:n_rb], (f(solp,x)-y_plot)[:n_rb], 'ro')
        # # plt.plot(x[n_rb:], (f(solp,x)-y_plot)[n_rb:], 'bo')
        # plt.plot(x[:self.n_rb], (self.us(x) - y_plot)[:self.n_rb], 'ro')
        # plt.plot(x[self.n_rb:], (self.us(x) - y_plot)[self.n_rb:], 'bo')
        #
        # plt.show()
        # print self.us.get_coeffs(), self.us.get_knots()
        #        print ier, solp, P0
        residuals = self.us(x) - y_plot
        deriv = self.us.derivative()
        derivatives = deriv(x)

        red_chi_sqr = np.sum(residuals ** 2) / len(residuals)

        self.sig_data_plot.emit(
            timestamp,
            {"data": {"x": x, "y": y_plot}, "residuals": {"x": x, "y": residuals}},
        )
        self.residuals_etalon = residuals[self.n_rb :]
        self.residuals_rb = residuals[: self.n_rb]
        res = {
            "offset": solp[0],
            "FSR": solp[1],
            "redchi": red_chi_sqr,
            "rms_rb": np.std(self.residuals_rb) / len(self.residuals_rb),
            "rms_cfp": np.std(self.residuals_etalon) / len(self.residuals_etalon),
            "residuals_offset": np.mean(self.residuals_etalon)
            - np.mean(self.residuals_rb),
            "stringlength": np.sum(np.abs(np.ediff1d(residuals))),
            "ljung_box": np.min(acorr_ljungbox(residuals)[1]),
            "jarque_bera": jarque_bera(residuals)[1],
            "derivative_min": np.min(derivatives),
            "derivative_mean": np.mean(derivatives),
            "derivative_max": np.max(derivatives),
            "derivative_argmin": x[np.argmin(derivatives)],
            "derivative_argmax": x[np.argmax(derivatives)],
        }

        for i, en in enumerate(etalon_names):
            res.update({en: self.residuals_etalon[i]})

        for i, rbn in enumerate(rb_names):
            res.update({rbn: self.residuals_rb[i]})

        return (
            res,
            {"X": x[: self.n_rb], "Rb-ramp-residuals": self.residuals_rb},
            {"X": x[self.n_rb :], "CFP-ramp-residuals": self.residuals_etalon},
        )


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import sys

    app = PyQt5.QtGui.QApplication(sys.argv)
    import qdarkstyle

    app.setStyleSheet(qdarkstyle.load_stylesheet(pyside=False))

    rb_center = [
        724.23120447,
        669.64557338,
        688.42391342,
        707.21330498,
        742.95284212,
        2334.2924886,
        2366.1711942,
        2358.44912083,
        2341.94680822,
        2383.26282254,
        2349.79986634,
        3606.68982218,
        3646.46202943,
        3628.41134543,
        3569.80660993,
        3588.5699693,
        3686.2086609,
    ]
    rb_refs = [
        3.84229150e14,
        3.84229058e14,
        3.84229089e14,
        3.84229121e14,
        3.84229181e14,
        3.84232064e14,
        3.84232125e14,
        3.84232110e14,
        3.84232079e14,
        3.84232157e14,
        3.84232093e14,
        3.84234526e14,
        3.84234605e14,
        3.84234569e14,
        3.84234454e14,
        3.84234490e14,
        3.84234683e14,
    ]
    etalon_centers = [
        229.23631421748365,
        359.09062838501916,
        479.48033910032927,
        594.66040011473081,
        706.59231091422589,
        816.3366308635575,
        924.03973356183269,
        1030.4724810469124,
        1135.9678548395377,
        1240.2841107337997,
        1343.5887320326069,
        1446.392542160344,
        1548.5802923439282,
        1650.8215238000087,
        1752.7782811866227,
        1854.642749418349,
        1956.6322845239051,
        2058.4266014056193,
        2160.0586900005424,
        2261.2115883259739,
        2361.3584697988631,
        2460.9239673350057,
        2559.5352815744227,
        2657.9430693850841,
        2755.287289591744,
        2852.5512841466921,
        2949.5256865930701,
        3045.6730931173902,
        3141.9153055428983,
        3237.6877147988876,
        3333.2630421446338,
        3428.4987826988013,
        3523.3323606814815,
        3618.2971693233785,
        3712.5250031121464,
        3806.7907509227966,
        3900.9936010809838,
        3994.6572606388809,
    ]

    rb_refs = (rb_refs - np.min(rb_refs)) / 1e6
    rb_weigts = np.ones(len(rb_center))
    r = Ramp()
    r.simultaneous_fit(
        datetime.now().isoformat(),
        rb_center,
        etalon_centers,
        rb_refs,
        rb_weights=rb_weigts,
        rb_names="",
        etalon_names="",
    )

    r.gui.show()

    plt.figure(figsize=(15, 3))
    plt.plot(rb_center, r.residuals_rb, "ro")
    plt.plot(etalon_centers, r.residuals_etalon, "bo")
    # import lmfit
    # from lmfit.models import LinearModel
    #
    # plt.figure()
    # x = np.arange(4000)
    # lm = LinearModel()
    # pars = lm.guess(r.us(x), x=x)
    # fr = lm.fit(r.us(x), pars, x=x)
    # fr.plot()
    # plt.show()

    sys.exit(app.exec_())
