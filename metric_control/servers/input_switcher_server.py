import logging
import threading
import time
from pathlib import Path

import serial
from autologging import logged, traced

from metric_control import server
from metric_control.generated.common_pb2 import DeviceStatus
from metric_control.generated.inputswitcher_pb2 import Positions, state, DESCRIPTOR
from metric_control.generated.inputswitcher_pb2_grpc import InputSwitcherServicer

log_file_path = str(Path(__file__).resolve().parents[1]) + '/etc/loggers.ini'

logger = logging.getLogger('server')


@traced(logger)
@logged(logger)
class InputSwitcher(InputSwitcherServicer, server.ServerBase):
    """
        Module to control PiezoJena fiber switcher 1:4

        As of right now, the actual commands are send via a arduino.
        This is the fiber switcher that selects where the output of the etlon is routed: either to the spectrograph or
        to the laser measurement setup.
    """

    def __init__(self, config, simulation=False, section=None):
        super().__init__(simulation=simulation, section=section)
        self.status = DeviceStatus.Value("ERROR")
        self.lock = threading.Lock()

        self.serial_port = None
        self.serial_port_address = config.get_string("InputSwitcher", "serial_port")
        self.serial_port_baudrate = config.get_string("InputSwitcher", "serial_port_baudrate")
        self.port = 0

        self.redis_base_string = str(DESCRIPTOR.package)
        self.logged_values = [f.name for f in state.DESCRIPTOR.fields]

    def initialize(self, request, context):
        if self.simulation:
            self.position = 1
            self.status = DeviceStatus.Value("READY")
            return self.OK
        else:
            if self.serial_port is None:
                try:
                    self.serial_port = serial.Serial(self.serial_port_address, self.serial_port_baudrate)
                    self.status = DeviceStatus.Value("READY")

                except serial.serialutil.SerialException as e:
                    self.status = DeviceStatus.Value("ERROR")
                    return self.error("Error connecting to " + str(self.serial_port))
            try:
                self.get_position(request, context)
            except Exception as e:
                logger.error(e)

            time.sleep(1.5)
            self.write_to_databases()
            return state(**self.make_log_dict_local())

    # def _blink(self, port1, port2, t1, t2, timeout):
    #     while (time.time() - self.start_time) < timeout:
    #         if not self.simulation:
    #             self.serial_port.write(f"ch{port1}\r\n".encode("ascii"))
    #         logger.debug(f"Switch to ch{port1}")
    #         time.sleep(t1/1000.)
    #         if not self.simulation:
    #             self.serial_port.write(f"ch{port2}\r\n".encode("ascii"))
    #         logger.debug(f"Switch to ch{port2}")
    #         time.sleep(t2/1000.)

    def set_position(self, request, context):
        port = request.port
        if self.simulation:
            if request.port in Positions.values():
                self.status = DeviceStatus.Value("READY")
                self.port = request.port
                return state(port=self.port, status=self.status)
            else:
                self.port = 0
                self.status = DeviceStatus.Value("ERROR")
                raise ValueError("Requested position not available")
        else:
            with self.lock:
                self.serial_port.write(f"ch{request.port}\r\n".encode("ascii"))

            self._get_position()
            if self.port == port:
                    self.status = DeviceStatus.Value("READY")
                    self.write_to_databases()
                    return state(port=self.port, status=self.status)
            else:
                self.status = DeviceStatus.Value("ERROR")
                self.write_to_databases()
                raise RuntimeError(f"Could not switch to port f{port}")

        #
        # first_port = request.first_port
        # second_port = request.second_port
        # first_pulse_length = request.first_pulse_length
        # second_pulse_length = request.second_pulse_length
        # timeout = request.timeout
        #
        # # no blinking:
        # if (second_port <= 0) or (second_pulse_length <= 0) or(timeout <= 0):
        #     if request.first_port in Positions.values():
        #         if self.simulation:
        #             self.status = DeviceStatus.Value("READY")
        #             self.first_port = first_port
        #             self.second_port = 0
        #             self.first_pulse_length = 0
        #             self.second_pulse_length = 0
        #             self.timeout = 0
        #         else:
        #             with self.lock:
        #                 self.serial_port.write(f"ch{first_port}\r\n".encode("ascii"))
        #                 answer = self.serial_port.readline()
        #                 logger.debug("Answer: %s", answer)
        #                 try:
        #                     int_answer = int(answer)
        #                 except Exception as e:
        #                     logger.error(e)
        #
        #                 if int_answer == first_port:
        #                     self.first_port = first_port
        #                     self.status = DeviceStatus.Value("READY")
        #                 else:
        #                     self.first_port = int_answer
        #                     self.status = DeviceStatus.Value("ERROR")
        #
        #                 self.second_port = 0
        #                 self.first_pulse_length = 0
        #                 self.second_pulse_length = 0
        #                 self.timeout = 0
        #
        #                 self.write_to_databases()
        #
        #     else:
        #         self.status = DeviceStatus.Value("ERROR")
        #         raise ValueError("Requested position not available")
        #     return state(**self.make_log_dict_local())
        # # blinking:
        # else:
        #     self.start_time = time.time()
        #     self.blink_Thread = threading.Thread(target=self._blink, args=(first_port,second_port,first_pulse_length, second_pulse_length,timeout))
        #     self.blink_Thread.start()
        #     return state(**self.make_log_dict_local())

    def _get_position(self):
        with self.lock:
            self.serial_port.write(b'ch?\r\n')
            answer = self.serial_port.readline()
            logger.debug("Answer: %s", answer)
        try:
            answerInt = int(answer)
            if 0 < answerInt < 5:
                self.port = answerInt
            else:
                raise ValueError(f"Reading invalid port number ({answer})")
        except Exception as e:
            logger.error(e)

    def get_position(self, request, context):
        self._get_position()
        return state(**self.make_log_dict_local())

    def close(self, reques, context):
        self.serial_port.close()


if __name__ == '__main__':
    server.make_server(InputSwitcher, "InputSwitcher")
