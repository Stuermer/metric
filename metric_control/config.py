import configparser
import os


class Config(object):
    """Configuration"""

    def __init__(self, filename):
        self.config = configparser.ConfigParser(comment_prefixes=(";",))
        self.config.read_file(filename)
        self.filename = filename.name

    def get_section(self, section):
        return dict(self.config.items(section, raw=True))

    def get_string(self, section, key, fallback=None):
        return self.config.get(section, key, raw=True)

    def get_integer(self, section, key, fallback=None):
        return self.config.getint(section, key, raw=True)

    def get_float(self, section, key, fallback=None):
        return self.config.getfloat(section, key, raw=True, fallback=fallback)

    def get_file_content(self, section, key, mode):
        filename = self.config.get(section, key, raw=True)
        with open(filename, mode=mode) as f:
            return f.read()

    def read_file_again(self):
        with open(self.filename) as f:
            self.config.read_file(f)


def ConfigType(filename):
    "Helper for to be used as type in add_argument"
    if not os.path.isfile(filename):
        import argparse

        raise argparse.ArgumentTypeError("'{}' is not a file".format(filename))
    with open(filename) as f:
        return Config(f)
