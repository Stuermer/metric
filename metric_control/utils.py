"Utils for maroonx_controll"
import errno
import logging
import os
import subprocess

import OpenSSL

try:
    GIT_HASH = subprocess.check_output(
        ["git", "describe", "--always", "--dirty", "--tags"]
    ).strip()
except subprocess.CalledProcessError:
    GIT_HASH = None


def get_software_version():
    return GIT_HASH


def generate_ssl_certs(
    key_file, cert_file, domains, ips, bits=4096, validity_in_s=3600
):
    """
    Generate a pair of self-signed certificates to use.

    Arguments:
    key_file: Name of the generated key file
    cert_file: Name of the generated certificate file
    domains: domains to add to SAN list
    ips: IPs to add to SAN list
    """
    key = OpenSSL.crypto.PKey()
    key.generate_key(OpenSSL.crypto.TYPE_RSA, bits)

    # create a self-signed cert
    cert = OpenSSL.crypto.X509()
    cert.get_subject().C = "US"
    cert.get_subject().ST = "Illinois"
    cert.get_subject().L = "A"
    cert.get_subject().O = "B"
    cert.get_subject().OU = "C"
    cert.get_subject().CN = "test"
    cert.set_serial_number(42)
    # Valid for just an hour, and some grace time
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(validity_in_s)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(key)
    cert.sign(key, "sha256")

    # This is guess work, see wiki
    san_list = ["DNS.{}:{}".format(ii, x) for ii, x in enumerate(domains + ips, 1)]
    san_list += ["IP.{}:{}".format(ii, x) for ii, x in enumerate(ips, 1)]
    cert.add_extensions(
        [
            OpenSSL.crypto.X509Extension(
                b"subjectAltName", False, bytes(", ".join(san_list), "utf8")
            )
        ]
    )

    cert = OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)
    key = OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, key)

    with open(cert_file, "wb") as f:
        f.write(cert)
    with open(key_file, "wb") as f:
        f.write(key)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def safe_open_wb(path):
    """
     Open "path" for writing (bytes), creating any parent directories as needed.
    """
    mkdir_p(os.path.dirname(path))
    return open(path, "wb")
