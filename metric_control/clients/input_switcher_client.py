from google.protobuf.empty_pb2 import Empty
from qtpy.QtCore import Signal, Slot

from metric_control.client import ClientBase, make_interactive_client
from metric_control.generated import inputswitcher_pb2
from metric_control.generated import inputswitcher_pb2_grpc


class InputSwitcherClient(ClientBase):
    """
    FiberSwitcher1 is the fiber switcher that defines where the output of the etalon is directed.
    It's either directed to the spectrograph (1) or to the laser measurement setup (2).
    """
    sig_new_pos = Signal(str)

    def __init__(self, config=None, enforce=False, section=None):
        super().__init__()
        self.fiber_switcher = self.create_stub(
            inputswitcher_pb2_grpc.InputSwitcherStub,
            config=config,
            enforce=enforce,
            section=section)

    @Slot()
    def initialize(self):
        return self.queue_call(self.fiber_switcher.initialize, Empty())

    @Slot()
    def get_position(self):
        return self.queue_call(self.fiber_switcher.get_position, Empty())

    @Slot(int)
    @Slot(str)
    def set_position(self, port):
        """
        Sets output position to given value.
        Allowed values are 1 and 2.
        Args:
            port (int): 1->spectrograph;  2->lasermeasurement

        Returns:
            New fiber switcher position.
        """
        if isinstance(port, int):
            if port in inputswitcher_pb2.Positions.values():
                req = inputswitcher_pb2.Position(port=port)
            else:
                raise ValueError(f"Requested port {port} not element of allowed values: {inputswitcher_pb2.Positions.values()}")
        elif isinstance(port, str):
            if port in inputswitcher_pb2.Positions.keys():
                req = inputswitcher_pb2.Position(port=inputswitcher_pb2.Positions.Value(port))
            else:
                raise ValueError(f"Requested port {port} not element of allowed values: {inputswitcher_pb2.Positions.keys()}")
        else:
            raise TypeError(f"Passed argument port is neither int nor str.")

        res = self.queue_call(self.fiber_switcher.set_position, req)
        self.sig_new_pos.emit(inputswitcher_pb2.Positions.Name(res.port))
        return res


def main():
    from IPython import embed
    fs1 = make_interactive_client(InputSwitcherClient, 'InputSwitcher')
    embed()


if __name__ == "__main__":
    main()
