from google.protobuf.empty_pb2 import Empty
from qtpy.QtCore import Signal, Slot

from metric_control.client import ClientBase, make_interactive_client
from metric_control.generated import outputswitcher_pb2
from metric_control.generated import outputswitcher_pb2_grpc


class OutputSwitcherClient(ClientBase):
    """
    FiberSwitcher2 is the fiber switcher that defines where the output of the etalon is directed.
    """
    sig_new_pos = Signal(str)

    def __init__(self, config=None, enforce=False, section=None):
        super().__init__()
        self.fiber_switcher = self.create_stub(
            outputswitcher_pb2_grpc.OutputSwitcherStub,
            config=config,
            enforce=enforce,
            section=section)

    @Slot()
    def initialize(self):
        return self.queue_call(self.fiber_switcher.initialize, Empty())

    @Slot()
    def get_position(self):
        return self.queue_call(self.fiber_switcher.get_position, Empty())

    @Slot(int)
    @Slot(str)
    def set_position(self, port):
        """
        Sets output position to given value.
        Allowed values are 1,2,3 and 4 or their equivalent key
        "PD", "FRONTEND", "SPECTROGRAPH" or "OFF"
        Args:
            port (int): 1->photodiode (for laser measurement);  2->frontend; 3->spectrograph; 4->off
            port (str): "PD"->1, "FRONTEND"->2, "SPECTROGRAPH"->3, OFF->4

        Returns:
            New fiber switcher position.
        """
        if isinstance(port, int):
            if port in outputswitcher_pb2.Positions.values():
                req = outputswitcher_pb2.Position(port=port)
            else:
                raise ValueError(f"Requested port {port} not element of allowed values: {outputswitcher_pb2.Positions.values()}")
        elif isinstance(port, str):
            if port in outputswitcher_pb2.Positions.keys():
                req = outputswitcher_pb2.Position(port=outputswitcher_pb2.Positions.Value(port))
            else:
                raise ValueError(f"Requested port {port} not element of allowed values: {outputswitcher_pb2.Positions.keys()}")
        else:
            raise TypeError(f"Passed argument port is neither int nor str.")

        res = self.queue_call(self.fiber_switcher.set_position, req)
        self.sig_new_pos.emit(outputswitcher_pb2.Positions.Name(res.port))
        return res


def main():
    from IPython import embed
    OutputSwitcher = make_interactive_client(OutputSwitcherClient, 'OutputSwitcher')
    embed()


if __name__ == "__main__":
    main()
