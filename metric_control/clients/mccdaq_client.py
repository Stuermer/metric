"""
Test Client
"""

import logging

from autologging import logged, traced
from google.protobuf.empty_pb2 import Empty
from qtpy.QtCore import Slot, Signal

from metric_control.client import ClientBase, make_interactive_client
from metric_control.generated import mccdaq_pb2
from metric_control.generated import mccdaq_pb2_grpc
from metric_control.generated.common_pb2 import DefaultBoolean

logger = logging.getLogger("MCCDAQ client")


@traced
@logged(logger)
class MCCDAQClient(ClientBase):
    sig_new_state = Signal(mccdaq_pb2.state)

    def __init__(self, config=None, enforce=False):
        super().__init__()
        self.config = config
        self.enforce = enforce
        self.mccdaq = None

    @Slot()
    def initialize(self):
        """
        Initialize MCCDAQ Controller
        """
        try:
            self.mccdaq = self.create_stub(
                mccdaq_pb2_grpc.MCCDAQStub, config=self.config, enforce=self.enforce
            )
            res = self.queue_call(self.mccdaq.initialize, Empty())
            self.sig_new_state.emit(res)
            self.sig_ready.emit()
            return res
        except RuntimeError:
            pass

    def set_laser_shutter(self, open=False):
        req = DefaultBoolean(on=open)
        res = self.queue_call(self.mccdaq.set_laser_shutter, req)
        self.sig_new_state.emit(res)
        return res

    def set_ldls_shutter(self, open=False):
        req = DefaultBoolean(on=open)
        res = self.queue_call(self.mccdaq.set_ldls_shutter, req)
        self.sig_new_state.emit(res)
        return res

    def get_laser_shutter(self):
        res = self.queue_call(self.mccdaq.get_laser_shutter, Empty())
        self.sig_new_state.emit(res)
        return res

    def get_ldls_shutter(self):
        res = self.queue_call(self.mccdaq.get_ldls_shutter, Empty())
        self.sig_new_state.emit(res)
        return res

    # def set_shutter(self, ldls=True, laser=False):
    #     req = mccdaq_pb2.shutter_state(shutter_LDLS=ldls, shutter_laser=laser)
    #     res = self.queue_call(self.mccdaq.shutter, req)
    #     self.sig_new_state.emit(res)
    #     return res

    def start_stop_scan(self, on):
        """
        Starts or stops the scan
        Args:
            on (bool): if True, the scan is started (if it isn't already running).
        Returns:
            DeviceStatus status = 1; // current device status
            bool ramp_running = 2; // is ramp running?
        """
        print(on)
        msg = DefaultBoolean(on=on)
        res = self.queue_call(self.mccdaq.start_stop_ramp, msg)
        self.sig_new_state.emit(res)
        return res

    def reload_settings(self):
        res = self.queue_call(self.mccdaq.reload_settings, Empty())
        self.sig_new_state.emit(res)
        return res


def main():
    from IPython import embed

    mccdaq = make_interactive_client(MCCDAQClient, "mccdaq")
    embed()


if __name__ == "__main__":
    main()
