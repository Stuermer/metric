from google.protobuf.empty_pb2 import Empty
from qtpy.QtCore import Signal, Slot

from metric_control.client import ClientBase, make_interactive_client
from metric_control.generated import metric_pb2
from metric_control.generated import metric_pb2_grpc


class METRICClient(ClientBase):
    """
    FiberSwitcher1 is the fiber switcher that defines where the output of the etalon is directed.
    It's either directed to the spectrograph (1) or to the laser measurement setup (2).
    """
    sig_new_pos = Signal(str, str, str, int, int, )

    def __init__(self, config=None, enforce=False, section=None):
        super().__init__()
        self.metric = self.create_stub(
            metric_pb2_grpc.METRICStub,
            config=config,
            enforce=enforce,
            section=section)

    @Slot()
    def initialize(self):
        """
        Initializes connection.
        Call this the first time you connect
        Returns:

        """
        return self.queue_call(self.metric.initialize, Empty())

    @Slot()
    def get_position(self):
        """
        Returns current position / timeout of METRIC
        :return:
        """
        return self.queue_call(self.metric.get_position, Empty())

    @Slot(int, int, int,int, int, int)
    def set_blinking(self, input, output_1, output_2, time_output_1, time_output_2, timeout):
        """
        Blinks input between output_1 and output_2 port
        Allowed values are 1 and 2.
        Args:
            input (str): "DARK1"  "Etalon" "CALUNIT" "DARK2"
            output_1 (str): "PD" "FRONTEND"  "SPECTROGRAPH"  "OFF"
            output_2 (str): "PD" "FRONTEND" "SPECTROGRAPH" "OFF"
            time_output_1: time in milliseconds [ms] spend on port 1
            time_output_2: time in milliseconds [ms] spend on port 2
            timeout:  timeout in seconds [s] before going back to default position

        Returns:
            Position/state of METRIC after all commands have been processed.
            This function will return BEFORE the timeout is reached.
            You can check the state via get_position()
        """
        assert isinstance(input, str), "input should be a string"
        assert input in metric_pb2.InputPositions.keys(), f"input {input} should element of {metric_pb2.InputPositions.keys()}"
        assert isinstance(output_1, str), "output_1 should be an string"
        assert output_1 in metric_pb2.OutputPositions.keys(), f"input {output_1} should element of {metric_pb2.OutputPositions.keys()}"
        assert isinstance(output_2, str), "output_2 should be an string"
        assert output_2 in metric_pb2.OutputPositions.keys(), f"input {output_2} should element of {metric_pb2.OutputPositions.keys()}"

        assert isinstance(time_output_1, int), "time_output_1 should be an integer"
        assert isinstance(time_output_2, int), "time_output_2 should be an integer"
        assert isinstance(timeout, int), "timeout should be an integer"

        req = metric_pb2.Position(input=metric_pb2.InputPositions.Value(input),
                                  output_1=metric_pb2.OutputPositions.Value(output_1),
                                  output_2=metric_pb2.OutputPositions.Value(output_2),
                                  time_output_1=time_output_1,
                                  time_output_2=time_output_2,
                                  timeout=timeout
                                  )

        res = self.queue_call(self.metric.set_position, req)

        return res

    def set_position(self, input, output, timeout):
        """
        Sets switchers to given positions.
        After timeout, the switcher will go to default position (input: etalon, output: photodiode) and open the lasershutter

        If this command is send while another position than the default position is in place, the old position is
         aborted and the new position and timeout are valid

        Args:
            input (str): "DARK1"  "Etalon" "CALUNIT" "DARK2"
            output (str): "PD" "FRONTEND"  "SPECTROGRAPH"  "OFF"
            timeout(int): timeout in seconds before switching back to 'default' position (i.e. no light towards Spectrograph)

        Returns:
            Position/state of METRIC after all commands have been processed. This function will return BEFORE the timeout is reached.
            You can check the state via get_position()
        """
        assert isinstance(input, str), "input should be a string"
        assert input in metric_pb2.InputPositions.keys(), f"input {input} should element of {metric_pb2.InputPositions.keys()}"
        assert isinstance(output, str), "output should be an string"
        assert output in metric_pb2.OutputPositions.keys(), f"input {output} should element of {metric_pb2.OutputPositions.keys()}"
        assert isinstance(timeout, int), "timeout should be an integer"

        return self.set_blinking(input=input, output_1=output, output_2=output, time_output_1=timeout, time_output_2=0,
                                 timeout=timeout)

    def abort(self):
        """
        Aborts current position / blinking and goes back to default position (i.e. no light to spectroraph)
        Returns:
            Position/state of METRIC after aborting
        """
        res = self.queue_call(self.metric.abort, Empty())
        return res

    # TODO: implement emergency_shutdown() (i.e. laser off)


def main():
    from IPython import embed
    metric = make_interactive_client(METRICClient, 'metric')
    embed()


if __name__ == "__main__":
    main()
