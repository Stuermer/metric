#!/bin/bash

FILES="proto/metric_control/generated/*.proto"
# GRPC_CPP_PLUGIN=protoc-gen-grpc=$(which grpc_cpp_plugin)

python3 -m grpc_tools.protoc -Iproto --python_out=. --grpc_python_out=. ${FILES}

# protoc -Iproto --cpp_out=gui ${FILES}
# protoc -Iproto --grpc_out=gui --plugin=${GRPC_CPP_PLUGIN} ${FILES}
