#!/usr/bin/env bash
set -x
set -e

byobu list-sessions | grep rpc_server || byobu new-session -d -s rpc_server

# mccdaq
byobu rename-window -t rpc_server:0 'MCCDAQ'
byobu send-keys "cd /home/$USER/Repos/metric && PYTHONPATH=/home/$USER/Repos/metric python3 metric_control/servers/mccdaq_server.py --config=etc/metric_config_simulation.ini --simulation" C-m
#
## output switcher
#byobu new-window -t rpc_server:1 'OutputSwitcher'
#byobu send-keys "cd /home/$USER/metric && PYTHONPATH=/home/$USER/metric /home/$USER/anaconda3/bin/python metric_control/servers/output_switcher_server.py --config=etc/metric_config.ini" C-m
#
## input switcher
#byobu new-window -t rpc_server:2 'InputSwitcher'
#byobu send-keys "cd /home/$USER/metric && PYTHONPATH=/home/$USER/metric /home/$USER/anaconda3/bin/python metric_control/servers/input_switcher_server.py --config=etc/metric_config.ini" C-m
#
## input switcher
#byobu new-window -t rpc_server:3 'MetricServer'
#byobu send-keys "cd /home/$USER/metric && PYTHONPATH=/home/$USER/metric /home/$USER/anaconda3/bin/python metric_control/servers/metric_server.py --config=etc/metric_config.ini" C-m


#byobu new-window -t rpc_server:1 -n 'BacklightFilterWheel'
#byobu send-keys -t rpc_server:BacklightFilterWheel "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/backlight_filterwheel_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## Cryotel CCD Red
#byobu new-window -t rpc_server:2 -n 'CryotelRed';
#byobu send-keys -t rpc_server:CryotelRed "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/cryotel_server.py --config=etc/maroonx_config_simulation.ini --simulation"  " --section CryotelRed" C-m
#
## Cryotel CCD Blue
#byobu new-window -t rpc_server:3 -n 'CryotelBlue';
#byobu send-keys -t rpc_server:CryotelBlue "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/cryotel_server.py --config=etc/maroonx_config_simulation.ini --simulation"  " --section CryotelBlue" C-m
#
## FLI Focuser
#byobu new-window -t rpc_server:4 -n 'FLIFocuser';
#byobu send-keys -t rpc_server:FLIFocuser "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/fli_focuser_server.py --config=etc/maroonx_config_simulation.ini --simulation"  " --section FLIFocuser" C-m
#
## FLI Focuser 2
#byobu new-window -t rpc_server:5 -n 'FLIFocuser2';
#byobu send-keys -t rpc_server:FLIFocuser2 "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/fli_focuser_server.py --config=etc/maroonx_config_simulation.ini --simulation"  " --section FLIFocuser2" C-m
#
## Filter Wheel
#byobu new-window -t rpc_server:6 -n 'FilterWheel'
#byobu send-keys -t rpc_server:FilterWheel "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/filter_wheel_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## GuideCamera
#byobu new-window -t rpc_server:7 -n 'GuideCam'
#byobu send-keys -t rpc_server:GuideCam "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/guider_camera_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## Pupil Camera
#byobu new-window -t rpc_server:8 -n 'PupilCam'
#byobu send-keys -t rpc_server:PupilCam "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/pupil_camera_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## Shutter
#byobu new-window -t rpc_server:9 -n 'FiberShutter'
#byobu send-keys -t rpc_server:FiberShutter "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/shutter_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## Telescope Shutter
#byobu new-window -t rpc_server:10 -n 'TelescopeShutter'
#byobu send-keys -t rpc_server:TelescopeShutter "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/telescope_shutter_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## Telescope server
#byobu new-window -t rpc_server:11 -n 'Telescope'
#byobu send-keys -t rpc_server:Telescope "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/telescope_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## tiptilt EPICS
##byobu new-window -t rpc_server:12 -n 'tiptilt EPICS'
##byobu send-keys "cd /home/Apps/epics/modules/npoint/iocBoot/iocnPoint && ../../bin/linux-x86_64/asynNpointTest st.cmd" C-m
#
## tiptilt
#byobu new-window -t rpc_server:12 -n 'tiptilt_server'
#byobu send-keys -t rpc_server:tiptilt_server "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/tiptilt_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## Weather server
#byobu new-window -t rpc_server:13 -n 'Weather'
#byobu send-keys -t rpc_server:Weather "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/weather_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## Zaber Motors
#byobu new-window -t rpc_server:14 -n 'ZaberMotors'
#byobu send-keys -t rpc_server:ZaberMotors "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/zaber_motor_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## FS1
#byobu new-window -t rpc_server:15 -n 'FS1'
#byobu send-keys -t rpc_server:FS1 "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/fiber_switcher_1_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## FS2
#byobu new-window -t rpc_server:16 -n 'FS2'
#byobu send-keys -t rpc_server:FS2 "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/fiber_switcher_2_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## FS3
#byobu new-window -t rpc_server:17 -n 'FS3'
#byobu send-keys -t rpc_server:FS3 "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/fiber_switcher_3_server.py --config=etc/maroonx_config_simulation.ini --simulation"  C-m
#
## mxblue
#byobu new-window -t rpc_server:18 -n 'BlueCCD'
#byobu send-keys -t rpc_server:BlueCCD "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/sta_camera_server.py --config=etc/maroonx_config_simulation.ini --simulation --section=MaroonxBlue"  C-m
#
## mxblue
#byobu new-window -t rpc_server:19 -n 'RedCCD'
#byobu send-keys -t rpc_server:RedCCD "cd /home/$USER/Repos/python/MaroonX-Control && PYTHONPATH=/home/$USER/Repos/python/MaroonX-Control /home/$USER/anaconda3/bin/python maroonx_control/servers/sta_camera_server.py --config=etc/maroonx_config_simulation.ini --simulation --section=MaroonxRed"  C-m