This folder contains udev rules that make sure that the hardware-components
plugged into the computers/servers/pi's always get assigned to the same hardware addresses 
or setting the correct permissions for accessing the device.

Find out about the devices by calling

```
udevadm info --attribute-walk --name=/dev/DEVICEPATH
```


Copy the udev rule on the appropriate device and reload udev rules with

```
sudo cp ./88-metric.rules /etc/udev/rules.d/88-metric.rules && sudo udevadm control --reload-rules && sudo udevadm trigger
```

Find out what physical port a certain device is on:
```
echo /dev/bus/usb/`udevadm info --name=/dev/DEVICEPATH --attribute-walk | sed -n 's/\s*ATTRS{\(\(devnum\)\|\(busnum\)\)}==\"\([^\"]\+\)\"/\4/p' | head -n 2 | awk '{$1 = sprintf("%03d", $1); print}'` | tr " " "/"
```
I know. Looks funny, but it's true !