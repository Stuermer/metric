Leoni Fiber Switch Control
==========================

General Information
-------------------

The Leoni 2x4 fiber switch has four input fibers (Port numbers 1-4) and
two output fibers (Port A and B).

All fibers are 200 micron diameter multi-mode fibers.

A specsheet of (similar) fiber switches can be found here: https://www.leoni.com/fileadmin/fiber_optics/publications/catalogues/fiberswitch.pdf


.. note::  The 4 input fibers can not be switched to the two output fibers independently,
            but only as shown in the following table.

Possible input/output configurations.

+-------------------+---------------------+
+ Electrical pins   + Fiber output ports  +
+---------+---------+-----------+---------+
| D 0     |D 1      |PORT A     |PORT B   |
+---------+---------+-----------+---------+
| LOW     |LOW      |1          |3        |
+---------+---------+-----------+---------+
| HIGH    |LOW      |2          |4        |
+---------+---------+-----------+---------+
| LOW     |HIGH     |3          | --      |
+---------+---------+-----------+---------+
| HIGH    |LOW      |4          |1        |
+---------+---------+-----------+---------+


Electrical connections
----------------------
To control the fiber switch, we use an Arduino, the connections are as follows:

Arduino --> FiberSwitch

    DIO 2 --> D 0

    DIO 3 --> D 1

    GND --> GND

    5V  -> VCC


Software control
----------------
To control the fiber switch we send commands to the Arduino controller via serial port.
The syntax is:

    port_number, pulse_length, pulse_pause

white spaces are ignored by the controller.

*port_number* is the number of the input port you want to select, so valid values are **1**, **2**, **3** and **4**.

*pulse_length* is the time the switcher will stay on the port selected by port_number. Units are milliseconds.

*pulse_pause* is the time the switcher will stay on the port **2**. Units are milliseconds.

.. note::  Currently, it is assumed that Port 2 is the 'dark' port, with no light source attached.

If either *pulse_length* or *pulse_pause* is not given or 0, both parameters will be ignored and the switcher will
simply switch to the given port until other commands are received.

Examples:

    "1, 1000, 1000"

would make Port A switch between Input 1 and 2 with 1s interval. (and Port B would blink between Input 3 and 4)

    "3, 0, 0"

would simply switch Port A to Input 3 (and Port B to None, since there is not output in this configuration
for this port).

