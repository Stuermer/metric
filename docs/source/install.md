# Installation

We recommend to run the software within an LXD container.
Alternatively, you could also use a virtual machine or a dedicated computer.

## Setting up LXD container

```bash
lxc launch ubuntu:20.04 metric
```
This will install an ubuntu 20.04 LXD container named 'metric'.

After that, log into the container,
```bash
lxc exec metric bash
```
and create a user 'metric' and create the .ssh directory.
```bash
adduser metric
usermod -aG dialout metric
usermod -aG sudo metric
mkdir /home/metric/.ssh
```

Then, create a ssh key and add it to the authorized keys on the metric server. Here it is assumed the keys name is id_rsa
```bash
cat .ssh/id_rsa.pub | lxc exec metric -- sh -c "cat >> /home/metric/.ssh/authorized_keys"
```

After that you should be able to login using ssh via:
```bash
ssh metric@10.68.X.X -i .ssh/id_rsa
```
Replace **10.68.X.X** with the IP address of the metric server.

## Use ansible to install METRIC software
Add the server to the ansible hosts. When using ubuntu, this mean editing */etc/ansible/hosts*:
add
```bash
10.68.X.X ansible_ssh_user=metric ansible_ssh_private_key_file=~/.ssh/id_rsa
```

Check connection to servers:

```bash
ansible all -m ping
```

---

Before we install the software package, we need three passwords which we can choose freely:

Please replace in the following command INFLUXPASSWORD, myinfluxtoken and GRAFANAPASSWORD with something appropriate.

Go to **metric/etc/** and execute  

```bash
ansible-playbook grafana.yaml -bK  -e "influx_pw=INFLUXPASSWORD influx_token=myinfluxtoken grafana_pw=GRAFANAPASSWORD" 

```
to install and setup InfluxDB, redis and grafana.

---

Once this is done successfully, execute:
```bash
ansible-playbook metric.yaml -bK  -e "influx_pw=INFLUXPASSWORD influx_token=myinfluxtoken grafana_pw=GRAFANAPASSWORD" 

```
to install all metric software.
