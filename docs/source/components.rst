Hardware components
===================

Fiber connections
-----------------

The following graph shows all fiber connections METRIC has.

.. graphviz::

    digraph{
        style=dashed
        rack [label="Electronics rack", shape=box]
        ldls [label="LDLS"]
        fibersplitter [label="50:50 split"]
        laser [label="laser"]
        mixer [label="mixer"]
        etalon [label="etalon"]
        cfp [label="confocal etalon"]
        rb [label="rubidium"]
        thar [label="ThAr"]
        fs1 [label="fs1"]
        fs2 [label="fs2"]
        spectrograph [label="spec"]
        frontend [label="frontend"]
        scrambler [label="scrambler"]
        pd [label="PD"]
        ldls -> fibersplitter
        fibersplitter -> mixer
        mixer -> etalon
        etalon -> fs1
        laser -> mixer
        laser->rb
        laser->cfp
        fs1 -> fs2
        fs2 -> frontend
        fs2 -> scrambler
        scrambler -> spectrograph
        fs2 -> pd
        thar -> fs1
        fibersplitter -> fs1
        subgraph cluster_inside{
        label="breadboard"
        laser
        fibersplitter
        mixer
        etalon
        fs1
        fs2
        pd
        cfp
        rb
        }
        subgraph cluster_elec{
        label="outside fies room"
        rack
        thar
        ldls
        scrambler
        }
        subgraph cluster_output{
        spectrograph
        frontend
        }
    }




Hardware that requires control:
-------------------------------

.. graphviz::

    digraph{
        style=dashed
        ldd [label="Laser diode driver current (LDD)"]
        lddpower [label="LDD power switch"]
        pa [label="Piezo amplifier (PA)"]
        ups [label="UPS"]
        vescent [label="Temperature controller"]
        vacuum [label="Vacuum gauge"]
        ion [label="ion getter pump"]
        pd1 [label="photo diode CFP"]
        pd2 [label="photo diode FPE"]
        pd3 [label="photo diode Rb"]
        pd4 [label="photo diode Rb power"]
        voa [label="voltage operated attenuator"]
    }

Software design
---------------

Two level design, engineering and user interface.

Engineering interface needs to adjust:

DAQ:
    * sample rate
    * number of samples per ramp
    * ramp shape / asymmetry (see http://mathworld.wolfram.com/FourierSeriesTriangleWave.html)
    * piezo ramp shape / asymmetry
    * piezo gain factor
    * voltage ranges of input channels + SE/DIFF
    * (optional add FF/gain optimization)
    * VOA ramp zero point

Fit:
    * Start/Stop fit
    * status

FiberSwitcher:
    * port
    * brightness /

Vescent:
    * read temperatures
    * set temperatures
    * switch on/off PID loop

Environmental sensors:
    * P/T/Humidity sensor

Vacuum:
    * read ion pump, switch?
    * read gauge