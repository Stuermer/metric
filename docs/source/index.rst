.. METRIC documentation master file, created by
   sphinx-quickstart on Thu Jan 23 13:46:14 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to METRIC's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
