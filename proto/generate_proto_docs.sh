#!/usr/bin/env bash

protoc --doc_out=../maroonx_control/docs/source/proto --doc_opt=markdown,docs.md maroonx_control/generated/*.proto